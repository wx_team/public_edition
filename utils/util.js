var QQMapWX = require('qqmap-wx-jssdk.js');
var qqmapsdk;
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
//验证手机号
function checkMobile(mobile) {
  var regMobile = /^[1][3,4,5,6,7,8][0-9]{9}$/
  if (!regMobile.test(mobile)) {
    return false;
  }
  return true;
}
//验证身份证号
function checkIdNo(idNo) {
  var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
  if (!regIdNo.test(idNo)) {
    return false;
  }
  return true;
}
//纯数字
function checkInt(value) {
  var regNum = new RegExp('[0-9]', 'g');
  if (!regNum.test(value)) {
    return false;
  }
  return true;
}
//小数， 可以有 .
function checkFloat(value) {

}

//获取两地间的距离
// function getDistance(lat, lng) {
//   // 实例化API核心类
//   qqmapsdk = new QQMapWX({
//     key: 'RPTBZ-NX6LJ-ETVF5-KRBHK-GAFM6-SIFKA'
//   });
//   qqmapsdk.calculateDistance({
//     to: [{
//       latitude: lat,
//       longitude: lng
//     }],
//     success: function (res) {
//       const distance = Math.floor(res.result.elements[0].distance / 1000 * 100) / 100
//       console.info("距离");
//       return distance;
//     },
//     fail: function (res) {
//       console.log(res);
//     },
//     complete: function (res) {
//       console.log(res);
//     }
//   });

// }
function getDistance(lat, lng) {
  qqmapsdk = new QQMapWX({
    key: 'RPTBZ-NX6LJ-ETVF5-KRBHK-GAFM6-SIFKA'
  });
  return new Promise((resolve) => {
    qqmapsdk.calculateDistance({
      to: [{
        latitude: lat,
        longitude: lng
      }],
      success(res) {
        const distance = Math.floor(res.result.elements[0].distance /
          1000 * 100) /
          100
        resolve(distance)
      },
      fail() {
        resolve('')
      }
    })
  })
}

//身份证号、数字、小数 可以通过input 的type 字段控制，没必要做验证
module.exports = {
  formatTime: formatTime,
  checkMobile: checkMobile,
  checkIdNo: checkIdNo,
  checkInt: checkInt,
  getDistance: getDistance
}
