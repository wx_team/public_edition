// pages/information/information.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk = new QQMapWX({ key: 'MBUBZ-VY2CW-4B5RA-OOIFR-U7L63-4VF6G' });
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    skip_a: true,
    skip_b: false,
    flag:false,
    list:[],
    mobile:"",
    status:false,
    collect_num:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    var id = options.id;
    var that = this;
    wx.request({
      url: host + 'index/details',
      data: {
        id:id,
        uid:wx.getStorageSync("uid")
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        var location = res.data.data.location.split(',');
        if (res.data.data.is_status == 1){
          that.setData({
            status:true
          })
        } 
        console.log(location);
        that.setData({
          list:res.data.data,
          latitude: location[0],
          longitude: location[1],
          mobile: res.data.data.mobile,
          collect_num: res.data.data.collect_num
        })
        wx.hideLoading();
      },
    })
  },
  detail:function(e){
    this.setData({
      detail: e.currentTarget.dataset.target
    })
  },
  closeDetail(e) {
    this.setData({
      detail: null
    })
  },
  myCard:function(e){
    
  },
  onShow:function(){
    // var that = this;
    // // 调用接口
    // qqmapsdk.reverseGeocoder({
    //   success: function (res) {
    //     that.setData({
    //       locationLat: that.data.locationLat,
    //       locationLng: that.data.locationLng
    //     });
    //   },
    //   fail: function (res) {
    //     //console.log(res);
    //   },
    //   complete: function (res) {
    //     //console.log(res);
    //   }
    // });
  },
  onShareAppMessage: function (ops) {
    var id = this.data.list.id;
    if (ops.from === 'button') {
      // 来自页面内转发按钮
      console.log(ops.target)
    }
    return {
      title: '公司信息页',
      path: '/pages/information/information?id='+id,
      success: function (res) {
        // 转发成功
        console.log("转发成功:" + JSON.stringify(res));
      },
      fail: function (res) {
        // 转发失败
        console.log("转发失败:" + JSON.stringify(res));
      }
    }

  },
  tabFun: function (e) {
    var self = this;
    //精品课程
    //  sessionStorage.setItem('showMask',true); 
    if (e.target.dataset.id && e.target.dataset.id == 0) {
      self.setData({
        skip_a: true,
        skip_b: false
      })
    }
    if (e.target.dataset.id && e.target.dataset.id == 1) {
      self.setData({
        skip_a: false,
        skip_b: true
      })
    }
  },
  returnSy: function () {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  openMap: function () {
    var latitude = Number(this.data.latitude);
    var longitude = Number(this.data.longitude);
    // wx.openLocation({
    //   latitude,
    //   longitude
    // })
    // wx.navigateTo({
    //   url: '../position/position'
    // })
    var that = this;
      wx.openLocation({
        longitude,
        latitude,
      })
  },
  enter:function(){
    wx.navigateTo({
      url: '/pages/occupancy/occupancy?edit=' + false + "&id=0",
    })
  },
  callPhone: function () {
    var mobile = this.data.mobile;
    wx.makePhoneCall({
      phoneNumber: mobile,
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  userComment:function(e){
    this.setData({
      userComment: e.detail.value
    })
  },
   /**
   * 评论内容验证
   */
  commentCheck:function(e){
    var that = this;
    var comment = that.data.userComment;
    if(comment == ""){
      wx.showToast({
        title: '请填写评论内容',
        icon: 'none',
      })
      return false;
    }
    wx.cloud.callFunction({
      config:{ env: 'public-bb1cff'},
      name: 'checkStr',
      data: {
        txt: comment
      },
      success(res) {
        console.log('ContentCheck-res',res)
        if (res.result.errCode == 87014) {
          console.log(res.errCode)
          wx.showToast({
            icon: 'none',
            title: '评论内容有违规信息',
          });
          return false;
        }else{
          console.log('合法');
          that.comment(e);
        }
      },fail(err){
        wx.showToast({
          icon: 'none',
          title: '内评论内容有违规信息',
        });
        return false;
  
      }
    })


  },
  /**
   * 评论
   */
  comment:function(e){
    var that = this;
    var comment = that.data.userComment;
    var id = e.currentTarget.dataset.id;
    if(comment == ""){
      wx.showToast({
        title: '请填写评论内容',
        icon: 'none',
      })
      return false;
    }
    wx.request({
      url: host + 'index/comment',
      data: {
        eid:id,
        uid: wx.getStorageSync("uid"),
        comment:comment
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        that.hideModal();
        wx.showToast({
          title: '评论成功',
          icon:"success"
        })
      },
    })
  },
  /**
   * 收藏
   */
  collect:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    var status = that.data.status;
    var collect_num = that.data.collect_num;
    if(status == false){
      wx.request({
        url: host + 'index/myCollect',
        data: {
          eid: id,
          uid: wx.getStorageSync("uid")
        },
        method: 'post',
        dataType: 'json',
        success: function (res) {
          
          that.setData({
            status: true,
            collect_num: collect_num+1
          })
          wx.showToast({
            title: '收藏成功',
            icon: 'none',
            duration: 2000
          })
        },
      })
    }else{
      wx.request({
        url: host + 'index/upMyCollect',
        data: {
          eid: id,
          uid: wx.getStorageSync("uid")
        },
        method: 'post',
        dataType: 'json',
        success: function (res) {
          that.setData({
            status: false,
            collect_num: collect_num - 1
          })
          wx.showToast({
            title: '取消收藏',
            icon: 'none',
            duration: 2000
          })
        },
      })
    }
  }
})