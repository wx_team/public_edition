// pages/classifyList/classifyList.js
const app = getApp()
var host = app.globalData.host;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    recommend: ["人气", "附近", "最新"],
    currentActive: 0,
    recommendList: [],
    latitude: 0,
    longitude: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    that.setData({
      id:id
    })
    wx.request({
      url: host + 'index/classifyList',
      data: {
        id:id,
        index:0
      },
      method: 'POST',
      dataType: 'json',
      success: function(res) {
        that.setData({
          recommendList: res.data.data
        })
        wx.hideLoading();
      },
    })
  },
  NavTab: function (e) {
    var that = this;
    that.setData({
      currentActive: e.currentTarget.dataset.index
    })
    var index = e.currentTarget.dataset.index;
    var id = that.data.id;
    wx.request({
      url: host + 'index/classifyList',
      data: {
        id:id,
        index: index,
        latitude: that.data.latitude,
        longitude: that.data.longitude
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        that.setData({
          recommendList: res.data.data
        })
      }
    })
  },
  details:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/information/information?id=' + id,
    })
  },
})