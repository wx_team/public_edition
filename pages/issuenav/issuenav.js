// pages/issuenav/issuenav.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    wx.request({
      url: host + 'publish/publishList',
      method: 'post',
      dataType: 'json',
      success: function(res) {
        that.setData({
          list:res.data.data
        })
        wx.hideLoading();
      },
    })
  },
  occupancy:function(e){
    wx.navigateTo({
      url: '/pages/occupancy/occupancy',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  toIssueAdd: function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../issueadd/issueadd?id='+id,
    })
  }
})