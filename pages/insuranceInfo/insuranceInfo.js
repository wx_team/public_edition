// pages/occupancy/occupancy.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk = new QQMapWX({ key: 'MBUBZ-VY2CW-4B5RA-OOIFR-U7L63-4VF6G' });
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: null,
    imgList: [],
    imgListLogo: [],
    selectAllStatus: true,
    picker: ['喵喵喵', '汪汪汪', '哼唧哼唧'],
    classIndex: 0,
    classList: [
    ],
    recommend: ["特惠版", "特惠版2", "特惠版3","特惠版4"],
    currentActive: 0,
    recommendList: [
      {
        id: 1,
        child: [
          {
            name: "工伤死亡",
            price: "40万",
          },
          {
            name: "工伤残疾",
            price: "40万",
          },
          {
            name: "工伤医疗",
            price: "40万",
          },
          {
            name: "附加险",
            price: "40万",
          }
        ]
      },
      {
        id: 2,
        child: [
          {
            name: "员工险",
            price: "50万",
          },
          {
            name: "员工险1",
            price: "50万",
          },
          {
            name: "员工险2",
            price: "50万",
          },
          {
            name: "员工险3",
            price: "50万",
          }
        ]
      },
      {
        id: 2,
        child: [
          {
            name: "员工险",
            price: "60万",
          },
          {
            name: "员工险1",
            price: "60万",
          },
          {
            name: "员工险2",
            price: "60万",
          },
          {
            name: "员工险3",
            price: "60万",
          }
        ]
      },
      {
        id: 2,
        child: [
          {
            name: "员工险",
            price: "70万",
          },
          {
            name: "员工险1",
            price: "70万",
          },
          {
            name: "员工险2",
            price: "70万",
          },
          {
            name: "员工险3",
            price: "70万",
          }
        ]
      }
    ],
    driver:[
      {
        name:"韩鹏杰",
        card:"140524199706111037"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  NavTab: function (e) {
    var that = this;
    that.setData({
      currentActive: e.currentTarget.dataset.index
    })
  },
  selectAll(e) {
    let selectAllStatus = this.data.selectAllStatus;
    selectAllStatus = !selectAllStatus;
    this.setData({
      selectAllStatus: selectAllStatus,
    });
  },
  name:function(e){
    this.setData({
      name:e.detail.value
    })
  },
  card: function (e) {
    this.setData({
      card: e.detail.value
    })
  },
  amendName: function (e) {
    console.log(e)
  },
  amendCard: function (e) {

  },
  queding:function(){
    var that = this;
    var json = {
      name: that.data.name,
      card: that.data.card
    };
    // var json = JSON.parse(json);
    that.data.driver.push(json)
    that.setData({
      driver: that.data.driver,
      modalName: null
    })
  },
  amend:function(e){
    console.log(e)
    this.setData({
      modalName: e.currentTarget.dataset.target,
      amendName: e.currentTarget.dataset.name,
      amendCard: e.currentTarget.dataset.card
    })
  },
  baocun:function(e){
    this.setData({
      modalName: null
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  onShow() {
    var that = this;
    // 调用接口
    qqmapsdk.reverseGeocoder({
      success: function (res) {
        that.setData({
          address: res.result.address,
          locationLat: res.result.location.lat,
          locationLng: res.result.location.lng
        });
      },
      fail: function (res) {
        //console.log(res);
      },
      complete: function (res) {
        //console.log(res);
      }
    });
  },
  openMap: function () {
    wx.navigateTo({
      url: '../position/position'
    })
  },

  formSubmit: function (e) {
    var that = this;
    var address = e.detail.value.address;
    var announcement = e.detail.value.announcement;
    var city = e.detail.value.city;
    var descinfo = e.detail.value.descinfo;
    var name = e.detail.value.name;
    var phone = e.detail.value.phone;
    var position = e.detail.value.position;
    var user = e.detail.value.user;
    var pickerT = that.data.pickerT;
    var imgList = that.data.imgList;
    var imgListLogo = that.data.imgListLogo;
    if (address && announcement && city && descinfo && name && phone && position && user && pickerT && imgList.length == 3 && imgListLogo.length == 1) {
    } else {
      wx.showModal({
        title: '提示',
        content: '请填写完整的信息',
        showCancel: false
      })
    }
  },

  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  }
})