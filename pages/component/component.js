//index.js
//获取应用实例

const app = getApp()
var host = app.globalData.host;

Page({
  data: {
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    icon:[],
    swiperList: [],
    recommend:["人气","附近","最新"],
    currentActive:0,
    latitude:0,
    longitude:0,
    recommendList:[],
    cate: [{ name: '企业', value: '0', checked: 'true' },
      { name: '帖子', value: '1'},
      { name: '名片', value: '2' },
      ],
    index:0,
    search:"",
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //是否自动切换
    interval: 3000, //自动切换时间间隔,3s
    duration: 1000, //  滑动动画时长1s
  },
  //事件处理函数
  onLoad: function () {
    
  },
  // 原登录../issueLogin/issueLogin  new ../authorizedLogin/authorizedLogin
  onShow: function () {
    if (app.loginUser == null || app.loginUser.id == null || app.loginUser.id == '') {
      wx.navigateTo({
        url: '../authorizedLogin/authorizedLogin',
      })
    }
    var that = this;
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
      }
    })
    var index = that.data.currentActive;
    wx.request({
      url: host + 'index/index',
      data:{
        index: index,
        latitude: that.data.latitude,
        longitude: that.data.longitude
      },
      success: function (res) {
        console.log("index:"+res);
        that.setData({
          swiperList: res.data.data.ads1,
          icon: res.data.data.cates,
          ads: res.data.data.ads2,
          notice: res.data.data.ads3,
          recommendList: res.data.data.list
        })
      }
    })
  },
  seek:function(e){
    var that = this;
    var search = that.data.search;
    var index = that.data.index;
    if(index == 0){
      wx.navigateTo({
        url: '/pages/comList/comList?search=' + search + '&index=' + index,
      })
    }
    if(index == 1){
      wx.navigateTo({
        url: '/pages/publishList/publishList?search=' + search + '&index=' + index,
      })
    }
    if (index == 2) {
      wx.navigateTo({
        url: '/pages/cardList/cardList?search=' + search + '&index=' + index,
      })
    }
  },
  radioChange:function(e){
    this.setData({
      index: e.detail.value
    })
  },
  search:function(e){
    this.setData({
      search:e.detail.value
    })
  },
  showModal(e) {
    if (this.data.search == "") {
      wx.showToast({
        title: '请输入搜索内容',
        icon:"none"
      })
      return false
    }
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  Settled:function(){
    wx.navigateTo({
      url: '/pages/occupancy/occupancy?edit='+false+"&id=0",
    })
  },
  fabu:function(e){
    wx.navigateTo({
      url: '/pages/issue/issue',
    })
  },
  classifyList:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/classifyList/classifyList?id='+id,
    })
  },
  details:function(e){
    var id = e.currentTarget.dataset.id;
    var index = this.data.currentActive;
    wx.navigateTo({
      url: '/pages/information/information?id='+id,
    })
  },  
  NavTab: function (e) {
    var that = this;
    that.setData({
      currentActive: e.currentTarget.dataset.index
    })
    var index = e.currentTarget.dataset.index;
    wx.request({
      url: host + 'index/navTab',
      data:{
        index:index,
        latitude: that.data.latitude,
        longitude: that.data.longitude
      },
      method:'post',
      dataType:'json',
      success:function(res){
        that.setData({
          recommendList:res.data.data
        })
      }
    })
  },
})
