const app = getApp();
const config = require('../../config.js');
Page({
  data: {
    firstPerson: "全部",
    show: false,
    index: 0,
    app: false,
    select: 0,
    selectPerson: true,
    selectArea: false,
    startDate: '',
    endDate: "",
    state: "全部",
    winHeight: "",
    //窗口高度    
    currentTab: 0,
    //预设当前项的值  
    scrollLeft: 0,
    //tab标题的滚动条位置  
    showViewTotal: false,
    showView: true,
    userInfo: null,
    loginUser: null,
    isFromSearch: true,
    // 用于判断searchSongList数组是不是空数组，默认true，空的数组    searchPageNum: 1, // 设置加载的第几次，默认是第一次      
    callbackcount: 10,
    //返回数据的个数    
    searchLoading: false,
    //"上拉加载"的变量，默认false，隐藏    
    searchLoadingComplete: false,
    selectPersion: true,
    firstPersion: '开票日期',
    selectAriea: false,
    //“没有数据”的变量，默认false，隐藏   
  },

  //  点击日期组件确定事件  
  //  点击时间组件确定事件  

  //  点击日期组件确定事件  
  bindDateChange: function (e) {
    let tempdate = {};
    if (e.target.dataset.t == "start") {
      tempdate.startDate = e.detail.value;
      this.data.startDate = e.detail.value;
    }
    if (e.target.dataset.t == "end") {
      tempdate.endDate = e.detail.value;
      this.data.endDate = e.detail.value;
    }
    this.setData(tempdate)
    getList(this);
  },
  clickPers: function () {
    var selectPersion = this.data.selectPersion;
    if (selectPersion == true) {
      this.setData({
        selectAriea: true,
        selectPersion: false,
      })
    } else {
      this.setData({
        selectAriea: false,
        selectPersion: true,
      })
    }
  },
  miSlect: function (e) {
    this.setData({
      firstPersion: e.target.dataset.ne,
      selectAriea: true,
      selectPersion: false,
    })
  },
  selectTap() {
    this.setData({
      show: !this.data.show
    });
  },
  // 点击下拉列表
  optionTap(e) {
    let Index = e.currentTarget.dataset.index;//获取点击的下拉列表的下标
    this.setData({
      index: Index,
      show: !this.data.show
    });
  },
  optionTap(e) {
    let select = e.currentTarget.dataset.select;//获取点击的下拉列表的下标
    this.setData({
      select: select,
      app: !this.data.app
    });
  },
  onLoad: function (options) {
    if (app.loginUser == null || app.loginUser.id == null && app.loginUser.mobile == null) {
      wx.navigateTo({
        url: '/pages/authorizedLogin/authorizedLogin'
      })
      return;
    }
    this.data.startDate = getDate(-7);
    this.data.endDate = getDate();
    this.setData({ startDate: this.data.startDate, endDate: this.data.endDate, hoverc4: "select_h", tenantCode: options.tenantCode});
    getList(this);

  },/**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    getList(this);
    wx.stopPullDownRefresh();
  },
  //点击选择类型
  clickPerson: function () {
    var selectPerson = this.data.selectPerson;
    if (selectPerson == true) {
      this.setData({
        selectArea: true,
        selectPerson: false,
      })
    } else {
      this.setData({
        selectArea: false,
        selectPerson: true,
      })
    }
  },
  //点击切换
  mySelect: function (e) {
    // console.log(e.target.dataset.me);
    this.data.state = e.target.dataset.me
    this.setData({
      firstPerson: e.target.dataset.me,
      selectPerson: true,
      selectArea: false,
      hoverc1: "",
      hoverc2: "",
      hoverc3: "",
      hoverc4: "select_h"
    });
    getList(this);
  },
  dtx: function () {
    this.setData({
      hoverc1: "select_h",
      hoverc2: "",
      hoverc3: "",
      hoverc4: ""
    });
    this.data.state = "待提现";
    getList(this);
  },
  ytx: function () {
    this.setData({
      hoverc1: "",
      hoverc2: "select_h",
      hoverc3: "",
      hoverc4: ""
    });
    this.data.state = "已提现";
    getList(this);
  },
  ktx: function () {
    this.setData({
      hoverc1: "",
      hoverc2: "",
      hoverc3: "select_h",
      hoverc4: ""
    });
    this.data.state = "可提现";
    getList(this);
  },
  //  点击日期组件确定事件  


  // 点击标题切换当前页时改变样式  

  translation: function (e) {
    wx.redirectTo({
      url: 'nine',
    })
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
});

/**
 * 查询查件
 */
function getList(_this) {
  searchList(_this, function (msg) {
    // console.log(msg);
    let arr = new Array();
    if (msg.data.code == 100 && Object.prototype.toString.call(msg.data.content.list) === "[object Array]" && msg.data.content.list.length > 0) {
      _this.setData({ wuliu: msg.data.content.list, showView: true });
    } else {
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
    }

  });
}

/**
 * 获取查件
 * flag  0：该手机号只作为发货人手机；1：该手机号作为收货人手机；2：该手机号作为发货人或者收货人手机
 */
function searchList(_this, callback) {

  if (_this.data.startDate > _this.data.endDate){
    app.tip("开始时间不能晚于结束时间","none");
    return false;
  }
  wx.showLoading({ mask: true, title: "加载中" });
  let par = { "code": _this.data.tenantCode, "telphone": app.loginUser.mobile, "state": _this.data.state, "startDate": _this.data.startDate, "endDate": _this.data.endDate };
  // console.log(par);
  wx.request({
    url: config.requestUrl + "withdraw/searchDCInfo",
    method: "POST",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: par,
    success: function (msg) {
      callback(msg);
      wx.hideLoading();
    },
    fail: function (msg) {
      // console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}
/**
 * 获取当前日期
 */
var getDate = function (num) {
  let tempDate = new Date();
  if (num != undefined && "" != num) {
    tempDate.setDate(tempDate.getDate() + num);//获取AddDayCount天后的日期 
  }
  let nowDate = new Date(Date.parse(tempDate));
  //年  
  let Y = nowDate.getFullYear();
  //月  
  let M = (nowDate.getMonth() + 1 < 10 ? '0' + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1);
  //日  
  let D = nowDate.getDate() < 10 ? '0' + nowDate.getDate() : nowDate.getDate();
  return Y + "-" + M + "-" + D;
};