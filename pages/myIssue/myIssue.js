// pages/Order/order.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menuList: ['点击刷新'],
    TabCur: 0,
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    wx.request({
      url: host + 'me/myIssue',
      data: {
        uid:wx.getStorageSync('uid'),
        type:0
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        that.setData({
          list:res.data.data
        })
        wx.hideLoading();
      },
    })
  },

  tabSelect(e) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    var id = e.currentTarget.dataset.id;
    that.setData({
      TabCur: e.currentTarget.dataset.id
    })
    wx.request({
      url: host + 'me/myIssue',
      data: {
        uid:wx.getStorageSync('uid'),
        type: id
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        that.setData({
          list: res.data.data
        })
        wx.hideLoading();
      },
    })
  },
})