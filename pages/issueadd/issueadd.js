var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk = new QQMapWX({ key: 'MBUBZ-VY2CW-4B5RA-OOIFR-U7L63-4VF6G' });
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgList:[],
    classIndex: 0,
    classList: [],
    typeId:0,
    cid:0,
    isSubBtn: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      typeId:options.id
    })
    wx.request({
      url: host + 'Publish/publishCate',
      method: 'post',
      dataType: 'json',
      success: function(res) {
        that.setData({
          classList:res.data.data
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    // 调用接口
    qqmapsdk.reverseGeocoder({
      success: function (res) {
        that.setData({
          address: res.result.address,
          locationLat: res.result.location.lat,
          locationLng: res.result.location.lng
        });
      },
      fail: function (res) {
        //console.log(res);
      },
      complete: function (res) {
        //console.log(res);
      }
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  ChooseImage() {
    var that=this;
    wx.chooseImage({
      count: 1, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        var tempFilePath=res.tempFilePaths;
        console.log("chooseImage that:"+that);
        wx.showLoading({ title: '图片审核中。。。', })
        //获取图片的临时路径
        const tempFilePaths = res.tempFilePaths[0]
        //使用getFileSystemManager获取图片的Buffer流
        wx.getFileSystemManager().readFile({
          filePath: tempFilePaths,                   
          success: (res)=>{ 
            const buffer = res.data
            //调用云函数进行审核
            wx.cloud.callFunction({
              name: 'checkImg',              
              data:{
                'buffer': buffer
              }            
            }).then(res=>{
              wx.hideLoading();
              console.log("checkImg:"+res);   
              //存在违规
              if(res.result.errCode != 0){
                wx.showModal({ title: '违规提示',content: '图片违规', showCancel: false,  confirmColor: '#DC143C'  })
              }else{
                if (that.data.imgList.length != 0) {
                  that.setData({
                    imgList: that.data.imgList.concat(tempFilePath)
                  })
                } else {
                  that.setData({
                    imgList:tempFilePath
                  })
                }
              }
            })
          }
        })
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '召唤师',
      content: '确定要删除这段回忆吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },

  clickClass: function(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    var id = e.currentTarget.dataset.id;
    that.setData({
      classIndex: index,
      cid:id
    })
  },

    openMap: function(){
      wx.navigateTo({
        url: '../position/position'
      })
    },
formSave:function(e){
  var that = this;
 var data = e.detail.value;
 if (data.descinfo == '') {
  msg = '请输入内容描述';
  wx.showToast({title: msg,icon: 'none'
  })
  return false;
}
  wx.cloud.callFunction({
    config:{ env: 'public-bb1cff'},
    name: 'checkStr',
    data: {
      txt: data.descinfo
    },
    success(res) {
      console.log('ContentCheck-res',res)
      if (res.result.errCode == 87014) {
        console.log(res.errCode)
        wx.showToast({ icon: 'none', title: '内容描述有违规信息', });
        return false;
      }else{
        console.log('合法');
        that.formSubmit(e);
      }
    },fail(err){
      wx.showToast({icon: 'none', title: '内容描述有违规信息', });
      return false;
    }
  })
},
  formCheckImg: function(e){
    var that = this;
    var data = e.detail.value;
 
      wx.chooseImage({
        success: (res)=>{        
         
        },      
      })
    

  },
  formSubmit: function(e){
    var that = this;
    var data = e.detail.value;
    var msg = '';
    if(data.address == ''){
      msg = '请选择具体位置';
    }
   
 
    if (data.mobile == '' || data.mobile.length != 11) {
      msg = '请输入正确的联系方式';
    }
    if (data.name == '') {
      msg = '请输入联系人';
    }
    if(msg != ''){
      wx.showToast({
        title: msg,
        icon: 'none'
      })
      return false;
    }
    var imgList = that.data.imgList;
    if (imgList.length == 0) {
      wx.showToast({
        title: '请上传图片',
        icon: 'none'
      })
      return false;
    }
    if(imgList.length == 1) {
      wx.showToast({
        title: '请上传至少俩张图片',
        icon: 'none'
      })
      return false;
    }

    this.setData({
      isSubBtn: true
    })
    var typeId = that.data.typeId;
    var cid = that.data.cid;
    wx.request({
      url: host + 'publish/addPublish',
      data: {
        data: data,
        uid: wx.getStorageSync("uid"),
        typeId: typeId,
        cid: cid
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        if (res.data.code == 1) {
          var id = res.data.data;
          for (var i = 0; i < imgList.length; i++) {
            wx.uploadFile({
              url: host + 'publish/addimgs?id=' + id,
              filePath: imgList[i],
              name: 'image',
              header: {
                "Content-Type": "multipart/form-data"
              },
              success: function (ress) {

              }
            })
          }
        }
        wx.showToast({
          title: '发布成功',
          duration: 2000,
          success: function () {
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/myIssue/myIssue'
              })
            }, 2000);
          }
        })
      },
      fail: function (res) {
        this.setData({
          isSubBtn: false
        })
      }
    })


  }
})