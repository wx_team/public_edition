const config = require('../../../config.js');
var app = getApp();
Page({
  data: {
    dates: '2018-05-01',
    times: '12:00',
    objectArray: ['中国', '英国', '美国'],
    index: 0,
    ks:'可提现',
    one:'12345678900000',
    two: '大庆',
    three: '大神力杯',
    four: '5000元',
    five: '0',
    six: '王立鹏',
    seven: '1件',
    eight: '0',
    nine: '111',
    mony:'49889元',
    phone: '',
  },

  /*绑定点击事件，将checkbox样式改变为选中与非选中*/
  //拿到下标值，以在carts作遍历指示用
  bindCheckbox: function (e) {
    var index = parseInt(e.currentTarget.dataset.index);
    //原始的icon状态
    var selected = this.data.carts[index].selected;
    var carts = this.data.carts;
    // 对勾选状态取反
    carts[index].selected = !selected;
    // 写回经点击修改后的数组
    this.setData({
      carts: carts
    });
  },

  bindSelectAll: function () {
    // 环境中目前已选状态
    var selectedAllStatus = this.data.selectedAllStatus;
    // 取反操作
    selectedAllStatus = !selectedAllStatus;
    // 购物车数据，关键是处理selected值
    var carts = this.data.carts;
    // 遍历
    console.info("选中状态");
    console.info(selectedAllStatus);
    for (var i = 0; i < carts.length; i++) {
      if (carts[i].state == '可提现' || carts[i].state == '已驳回') {
        carts[i].selected = selectedAllStatus;
      }
    }
    console.info(carts);
    this.setData({
      selectedAllStatus: selectedAllStatus,
      carts: carts
    });
  },

  //  点击时间组件确定事件  
  bindTimeChange: function (e) {
    console.log("谁哦按")
    this.setData({
      times: e.detail.value
    })
  },
  phoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  //  点击日期组件确定事件  
  bindDateChangeStart: function (e) {
    this.setData({
      startDate: e.detail.value
    });
    this.data.startDate = e.detail.value;
    this.doSearchWithDrawalList();
  },
  bindDateChangeEnd: function (e) {
    this.setData({
      endDate: e.detail.value
    })
    this.data.endDate = e.detail.value;
    this.doSearchWithDrawalList();
  },

  resetBtnClick: function (e) {
    this.setData({ infoMess: '', userName: '', userN: '', passWd: '', passW: '', })
  }, 

  doSearchWithDrawalList: function () {
    console.info("取款选择列表查询");
    var that = this;

    if (that.data.startDate > that.data.endDate) {
      app.tip("开始时间不能晚于结束时间", "none");
      return false;
    }

    console.info(that.data.generateFlag);
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: config.requestUrl + "withdraw/searchDCInfo",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        startDate: that.data.startDate,
        endDate: that.data.endDate,
        code: that.data.tenantCode,
        state: "可提现",
        telphone: app.loginUser.mobile
      },
      success: function (res) {
        console.log("取款列表返回结果");
        console.log(res);
        if (res.data.msg != null && res.data.msg == "成功") {
          var content = res.data.content.list;
          console.info(content);
          if (content != null && content.length > 0) {
            that.setData({
              carts: content,
              count: res.data.content.count,
              sum: res.data.content.sum,
              showData: true
            });
            wx.hideLoading();
          } else {
            that.setData({
              carts: content,
              showData: false,
              message: "抱歉，没有找到相关数据！",
              imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
            });
            wx.hideLoading();
          }
        } else {
          wx.hideLoading();
          app.tip(res.data.msg, "none");
        }
      },
      fail: function (msg) {
        wx.hideLoading();
        console.log(msg);
        wx.showModal({
          title: '提示',
          content: msg.data.msg,
          showCancel: false
        })
      }
    })
  },
  doCheck: function (event) {
    var that = this;
    var carts = that.data.carts;
    var content = [];
    var j = 0
    for (var i = 0; i < carts.length; i++) {
      console.log(carts[i]);
      console.log(carts[i].selected);
      if (carts[i].selected == true) {
        console.log("选中的===" + i);
        if (carts[i].state == "已驳回"){
          carts[i].orderPic=""
        }
        content[j] = carts[i];
        j = j + 1;
      }
    }
    console.info("传值");
    console.info(content);
    if (content.length > 0) {
      console.info("跳转");
      //wx.navigateTo({ url: "../addReceiptPhoto/addReceiptPhoto?param=" + JSON.stringify(content) })
      wx.navigateTo({ url: "../messageCheck/messageCheck?param=" + JSON.stringify(content) })
    } else {
      app.tip("请勾选取款数据", "none");
    }
  },
  onLoad: function (options) {
    console.info("withdrawalList初始化");
    console.info(app.companyInfo);
    if (app.loginUser == null || app.loginUser.id == null && app.loginUser.mobile == null) {
      wx.navigateTo({
        url: '/pages/authorizedLogin/authorizedLogin'
      })
      return;
    }
    console.log('onLoad')
    var that = this //调用应用实例的方法获取全局数据  
    that.data.startDate = getDate(-7);
    that.data.endDate = getDate();
    that.data.pageCount = 10;
    that.setData({ startDate: that.data.startDate, endDate: that.data.endDate, tenantCode: options.tenantCode});
    that.doSearchWithDrawalList();
  }
})


/**
 * 获取当前日期
 * num -1  当前的前一天   2  当前的后2天
 */
var getDate = function (num) {
  let tempDate = new Date();
  if(num != undefined && "" != num){
    tempDate.setDate(tempDate.getDate() + num);//获取AddDayCount天后的日期 
  }

  let nowDate = new Date(Date.parse(tempDate));
  //年  
  let Y = nowDate.getFullYear();
  //月  
  let M = (nowDate.getMonth() + 1 < 10 ? '0' + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1);
  //日  
  let D = nowDate.getDate() < 10 ? '0' + nowDate.getDate() : nowDate.getDate();
  return Y + "-" + M + "-" + D;
};

