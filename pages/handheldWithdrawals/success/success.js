const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    yi:'恭喜您提交成功',
    er:'查询取款记录',
    three:'掌上取款数据状态说明',
    four:'待提现：未回款代收',
    five:'可提现：可取款提现',
    six:'已提现：已完成提现',
    seven:'待确认：取款已提交，等待物流公司审核确认',
    eight:'已驳回：物流公司审核驳回微信取款申请',
    nine:'已确认：物流公司审核确认通过微信取款申请',
    add:'已转账：物流公司已提交银行转账',
    width:'已挂失：该票已在物流公司挂失，不可取款',
    left:'代收取消：代收已取消',
  },

  //返回首页
  toIndex: function (e) {
    wx.switchTab({
      url: '../../index/index',
    })
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: app.appName,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
  // ,

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // }
})