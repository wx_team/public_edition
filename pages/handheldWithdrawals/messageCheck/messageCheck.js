const config = require('../../../config.js');
var app = getApp();
Page({

  data: {
    VerifyCode: "发送"
  },

  blurTel: function (e) {
    var linkTel = e.detail.value.replace(/\s/g, "");
    this.setData({
      linkTel: linkTel
    })
  },

  blurSecurityCode: function (e) {
    var securityCode = e.detail.value.replace(/\s/g, "");
    this.setData({
      securityCode: securityCode
    })
  },

  onLoad: function (option) {
    this.setData({
      param: option.param,
    });
    // console.log("短信参数：" + option.param);
  },

  setVerify: function (e) {//发送验证码
    this.setData({
      able: true
    });
    var linkTel = this.data.linkTel;
    var _Url = config.requestUrl + "user/sendVaildateCodeByMobile";
    var total_micro_second = 120 * 1000;
    //验证码倒计时
    count_down(this, total_micro_second);
    // console.log(_Url)
    wx.request({
      url: _Url,
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: {
        mobile: app.loginUser.mobile,
        sendType: 'withDraw',
        code: app.companyInfo.code,
        password: app.companyInfo.tenantPass
      },
      success: function (res) {
        console.log(res);
        if (res.data.code == "100") {
          wx.showModal({
            title: '提示',
            content: '发送验证码成功！',
            showCancel: false
          })
        }
      },
      fail: function (res) {
        // console.log("error res=")
        // console.log(res.data)
      }
    });
  },

  confirm: function (e) {//确定
    var param = this.data.param;
    // wx.navigateTo({ url: "../addReceiptPhoto/addReceiptPhoto?param=" + param })
    // return;
    var linkTel = this.data.linkTel;
    var securityCode = this.data.securityCode;
    var _Url = config.requestUrl + "user/checkValidateCodeByMobile";
    // console.log(_Url)
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: _Url,
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: {
        validateCode: securityCode,
        mobile: app.loginUser.mobile
      },
      success: function (res) {
        wx.hideLoading();
        console.log("验证短信返回结果");
        console.log(res);
        console.log(res.data.content.success);
        if (res.data.code == "100" && res.data.content.success == "true") {
          console.log("验证短信成功");
          // wx.showModal({
          //   title: '提示',
          //   content: '验证成功！',
          //   showCancel: false
          // })
          //wx.navigateTo({ url: "addReceiptPhoto?param=" + param })
          wx.navigateTo({ url: "../addReceiptPhoto/addReceiptPhoto?param=" + param })
        } else {
          wx.showModal({
            title: '提示',
            content: '验证失败,请输入正确验证码！',
          })
        }
      },
      fail: function (res) {
        wx.hideLoading();
        // console.log("error res=")
        // console.log(res.data)
      }
    });
  }
})

/* 毫秒级倒计时 */
function count_down(that, total_micro_second) {
  if (total_micro_second <= 0) {
    that.setData({
      VerifyCode: "重新发送",
      able: false
    });
    // timeout则跳出递归
    return;
  }
  // 渲染倒计时时钟
  that.setData({
    VerifyCode: date_format(total_micro_second) + " 秒"
  });
  setTimeout(function () {
    // 放在最后--
    total_micro_second -= 10;
    count_down(that, total_micro_second);
  }, 10)
}
// 时间格式化输出，如03:25:19 86。每10ms都会调用一次
function date_format(micro_second) {
  // 秒数
  var second = Math.floor(micro_second / 1000);
  // 小时位
  var hr = Math.floor(second / 3600);
  // 分钟位
  var min = fill_zero_prefix(Math.floor((second - hr * 3600) / 60));
  // 秒位
  var sec = fill_zero_prefix((second - hr * 3600 - min * 60));// equal to => var sec = second % 60;
  // 毫秒位，保留2位
  var micro_sec = fill_zero_prefix(Math.floor((micro_second % 1000) / 10));
  return sec;
}
// 位数不足补零
function fill_zero_prefix(num) {
  return num < 10 ? "0" + num : num
}