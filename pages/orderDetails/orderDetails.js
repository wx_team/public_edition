// pages/occupancy/occupancy.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk = new QQMapWX({ key: 'MBUBZ-VY2CW-4B5RA-OOIFR-U7L63-4VF6G' });
Page({

  /**
   * 页面的初始数据
   */
  data: {
    driver: [
      {
        name: "韩鹏杰",
        card: "140524199706111037"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  NavTab: function (e) {
    var that = this;
    that.setData({
      currentActive: e.currentTarget.dataset.index
    })
  },
  name: function (e) {
    this.setData({
      name: e.detail.value
    })
  },
  card: function (e) {
    this.setData({
      card: e.detail.value
    })
  },
  amendName: function (e) {
    console.log(e)
  },
  amendCard: function (e) {

  },
  queding: function () {
    var that = this;
    var json = {
      name: that.data.name,
      card: that.data.card
    };
    // var json = JSON.parse(json);
    that.data.driver.push(json)
    that.setData({
      driver: that.data.driver,
      modalName: null
    })
  },
  amend: function (e) {
    console.log(e)
    this.setData({
      modalName: e.currentTarget.dataset.target,
      amendName: e.currentTarget.dataset.name,
      amendCard: e.currentTarget.dataset.card
    })
  },
  baocun: function (e) {
    this.setData({
      modalName: null
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },

})