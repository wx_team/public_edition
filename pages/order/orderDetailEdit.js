const app = getApp();
var wxbarcode = require('../../utils/orderDetail.js');
const config = require('../../config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    var order = JSON.parse(options.order);

    // 根据订单中金融代码 获取公司信息
    wx.request({
      url: config.requestUrl + "company/getCompanyInfoByCode",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "id": "", "code": order.tenantCode },
      success: function (msg) {
        order.senderAddressStr = order.senderAddress != undefined ? order.senderAddress.replace('⊙', ' ') : "";
        var resObj = msg.data.content;
        if (resObj != undefined) {
          //设置物流公司名称和订单信息
          _this.setData({ companyName: resObj.name});
          //设置订单信息
          // getInfo(_this, order.id, order.tenantCode);
          _this.setData({ res: order });
        }
      },
      fail: function (msg) {

      }
    });
    
    // 生成二维码
    wxbarcode.qrcode('qrcode', order.id, 300, 300);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // },


  /**
   * 修改
   */
  editE:function(e){
    wx.redirectTo({
      url: '../order/order?order=' + JSON.stringify(this.data.res),
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  /**
   * 删除
   */
  delE:function(e){
    let _this = this;
    wx.showModal({
    title: '提示',
    content: '确定要删除吗？',
    success: function (sm) {
      if (sm.confirm) {
        // 用户点击了确定 可以调用删除方法了
        let tempid = e.target.dataset.eid;
        // console.log(tempid);
        wx.showLoading({ mask: true, title: "加载中" });
        wx.request({
          url: config.requestUrl + "order/deleteOrder",
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
          },
          data: { data: '{"id":' + tempid + ',"tenantCode":"' + config.tenantCode + '"}' },
          success: function (msg) {
            // console.log(msg);
            wx.hideLoading();
            if (msg.data.code == 100) {
              wx.navigateBack({
                delta: 1
              });
            } else {
              app.tip("删除失败！", 'none');
            }
          },
          fail: function (msg) {
            // console.log(msg);
            _this.setData({
              showView: false,
              message: "抱歉，没有找到相关数据！",
              imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
            });
            wx.hideLoading();
          }
        });
      } else if (sm.cancel) {
        // console.log('用户点击取消')
      }
    }
  })
  
  }
});

/**
 * 获取信息
 */
// function getInfo(_this, dh, orderNumber) {
//   queryInfo(dh, orderNumber, function (msg) {
//     // console.log(msg.data.content);
//     let result = msg.data.content;
//     if (undefined != result) {
//       _this.data.order = result;
//       let real = {};
//       for (let t in result) {
//         if (result[t] != null && result[t] != "null" && result[t] != "undefined") {
//           real[t] = result[t];
//         }
//       }
//       // console.log(real);
//       // real.senderAddress = real.senderAddress.replace('⊙', '');
//       real.senderAddressStr = real.senderAddress != undefined ? real.senderAddress.replace('⊙', '') : "";
//       _this.setData({ res: real });
//     }
//   })
// }

/**
 * 请求数据
 */
// function queryInfo(dh, orderNumber, callback) {
//   wx.showLoading({ mask: true, title: "加载中" });
//   wx.request({
//     url: config.requestUrl + "order/getOrderInfo",
//     method: "GET",
//     header: {
//       "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
//     },
//     data: { "id": dh, "code": config.tenantCode, "orderNo": orderNumber },
//     success: function (msg) {
//       callback(msg);
//       wx.hideLoading();
//     },
//     fail: function (msg) {
//       // console.log(msg);
//       _this.setData({
//         showView: false,
//         message: "抱歉，没有找到相关数据！",
//         imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
//       });
//       wx.hideLoading();
//     }
//   });
// }

/**
 * 获取信息
 */
function getInfo(_this, orderNumber, tenantCode) {
  queryInfo(_this, orderNumber, tenantCode, function (msg) {
    let result = msg.data.content;
    if (undefined != result && typeof result == "object") {
      let real = {};
      for (let t in result) {
        if (result[t] != null && result[t] != "null" && result[t] != "undefined") {
          real[t] = result[t];
        }
      }
      real.consignerAddress = real.consignerAddress != undefined ? real.consignerAddress.replace('⊙', ' ') : "";
      _this.setData({ res: real });
    } else {
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
    }
  })
}

/**
 * 请求订单数据 根据 
 */
function queryInfo(_this, orderNumber, tenantCode, callback) {
  wx.showLoading({ mask: true, title: "加载中" });
  wx.request({
    url: config.requestUrl + "order/getOrderInfo",
    method: "GET",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: { "id": "", "code": tenantCode, "orderNo": orderNumber },
    success: function (msg) {
      console.log("param--Message: orderNumber:" + orderNumber + " tenantCode: " + tenantCode);
      console.log("orderDetailEdit-Message: " + JSON.stringify(msg) );
      callback(msg);
      wx.hideLoading();
    },
    fail: function (msg) {
      console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}