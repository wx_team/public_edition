const app = getApp();
var wxbarcode = require('../../utils/orderDetail.js');
const config = require('../../config.js');
var WxParse = require('../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    var order = JSON.parse(options.order);
    
    // 根据订单中金融代码 获取公司信息
    wx.request({
      url: config.requestUrl + "company/getCompanyInfoByCode",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "id": "", "code": order.tenantCode },
      success: function (msg) {
        var resObj = msg.data.content;
        if(resObj != undefined){
          //设置物流公司名称
          _this.setData({ companyName: resObj.name });
          //托运清单，富文本处理
          var article = resObj.consignAgreement == null ? '<strong style="color:red">请联系管理员设置托运协议</strong>' : resObj.consignAgreement;
          WxParse.wxParse('article', 'html', article, _this, 5);
          //生成二维码    
          // wxbarcode.qrcode('qrcode', order.tenantCode + "," + order.orderNumber, 300, 300);
          wxbarcode.qrcode('qrcode', order.tenantCode + "," + order.orderNumber, 300, 300);
          //查询运单详情并赋值
          getInfo(_this, order.orderNumber, order.tenantCode);
        }
      },
      fail: function (msg) {

      }
    });


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
});

/**
 * 获取信息
 */
function getInfo(_this, orderNumber, tenantCode) {
  queryInfo(_this, orderNumber, tenantCode, function (msg) {
    let result = msg.data.content;
    if (undefined != result && typeof result == "object") {
      let real = {};
      for (let t in result) {
        if (result[t] != null && result[t] != "null" && result[t] != "undefined") {
          real[t] = result[t];
        }
      }
      real.consignerAddress = real.consignerAddress != undefined ? real.consignerAddress.replace('⊙', ' ') : "";
      _this.setData({ res: real });
    } else {
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
    }
  })
}

/**
 * 请求数据
 */
function queryInfo(_this, orderNumber, tenantCode, callback) {
  wx.showLoading({ mask: true, title: "加载中" });
  wx.request({
    url: config.requestUrl + "order/getOrderInfo",
    method: "GET",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: { "id": "", "code": tenantCode, "orderNo": orderNumber },
    success: function (msg) {
      console.log("param--Message: orderNumber:" + orderNumber + " tenantCode: " + tenantCode);
      console.log("orderDetail-Message: " + JSON.stringify(msg));
      callback(msg);
      wx.hideLoading();
    },
    fail: function (msg) {
      console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}