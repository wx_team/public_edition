const app = getApp()
const config = require('../../config.js');
const util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showFileUrl: config.showFileUrl,
    //遮挡层begin
    display: '',
    unloadPlaceMask: '',
    placeOfLoadingMask: '',
    senderMask: '',
    receiverMask: '',
    createReceiverMask: '',
    createSenderMask: '',
    senderBankMask: '',
    vipCardMask: '',
    goodsMask: '',
    //遮挡层end

    //显示元素begin
    sgdhShowFlag: false,
    bankShowFlag: false,
    vipCardShowFlag: false,
    weightShowFlag: false,
    bulkShowFlag: false,
    packingStylesShowFlag: false,
    collectionOfMoneyShowFlag: false,
    //显示元素end

    // 1 中转公司  公版暂不添加
    // transferCompanyName:'',
    // transferCompanyCode:'',
    // transferCompanyArray:[],
    // transferCompanyObjArray:[],
    // transferCompanyArrayIndex: 0,
    longitude: '',
    latitude: '',
    id: '',
    storage: '',
    index: 0,
    saveActionFlag:false,
    payWayArray: [],
    payWayIndex: 0,
    pickUpWayArray: [],
    pickUpWayIndex: 0,
    circleH: false,
    spread: false,
    addMsg2: [],
    selGoodsNames: [],
    packingStyles: [],
    saveSenderCheckBoxs: [{ name: '', value: '1', check: 'true'}],
    defSenderCheckBoxs: [{ name: '', value: '0' ,check:''}],
    saveRecevierCheckBoxs: [{ name: '', value: '1', check: 'true' }],
    defRecevierCheckBoxs: [{ name: '', value: '0', check: ''}],
    isSaveSenderZheZhao:"1",
    isDefaultSenderZheZhao:"0",
    isSaveReceiverZheZhao:"1",
    isDefaultReceiverZheZhao:"0",
    isDefaultSender : 0,
    isDefaultReceiver : 0,
    sendWay: "网点发货",
    radioitems: [
      { name: '0', value: '网点发货',checked: 'true'},
      { name: '1', value: '上门取件',check:'' }
    ],
    locationAddr : "",
    tempshowvipcardnodata:false,
    isshowindex : "block",

  },
  onShareAppMessage: function () {
    var that = this;
    return {
        title: '在线下单',

        desc: '小程序快速下单',

        path: '/order/order?shareId='+that.data.tenantCode // 路径，传递参数到指定页面。
    }

},

    bindMultiPayWay: function (e) {
      var payWayArray = this.data.payWayArray;
      var payWay = payWayArray[e.detail.value];
      this.setData({
        payWay: payWay,
        payWayIndex: 0
      })
    },

    bindMultiSelGoodsNames: function (e) {
      var selGoodsNames = this.data.selGoodsNames;
      var packingStyle = selGoodsNames[e.detail.value];

      console.info(this.data.inputGoodsNameValueZheZhao);

      // 追加输入框中的
      // this.setData({
      //   inputGoodsNameValueZheZhao: packingStyle
      // })
      
    },
    bindMultiPackingStyles: function (e) {
      var packingStyles = this.data.packingStyles;
      var packingStyle = packingStyles[e.detail.value];
      this.setData({
        inputPackageTypeValueZheZhao: packingStyle
      })
    },

    bindMultiPickUpWay: function (e) {
      var pickUpWayArray = this.data.pickUpWayArray;
      var pickUpWay = pickUpWayArray[e.detail.value];
      this.setData({
        pickUpWay: pickUpWay,
        pickUpWayIndex: 0
      })
    },

    // 2 中转公司 暂不需要
    // transferCompanyPicker: function (e) {
    //   // 设置页面展示中转公司信息,后端获取的选中中转公司对象
    //   var transferCompanyObjArray = this.data.transferCompanyObjArray;
    //   var transferCompanyObj = transferCompanyObjArray[e.detail.value];
    //   this.setData({
    //     transferCompanyCode: transferCompanyObj.transferCompanyCode,
    //     transferCompanyName: transferCompanyObj.transferCompanyName,
    //     transferCompanyArrayIndex: 0
    //   });
    // },
    
  /** 保存表单formId方法 */
  saveFormId: function (e) {
    // 获取参数与表单formID信息
    var formId = e.detail.formId;
    var mobile = app.loginUser.mobile;
    var openId = app.openid;
    // 判断是手机点击的botton按钮，
    if (formId != "the formId is a mock one") {
      if (mobile != null && mobile != undefined && openId != undefined) {
        console.log("mobile: " + mobile + "  formID: " + formId);
        app.updateUserFormId(mobile, formId, openId);
      }
    }
    console.log("====FORMID: " + formId + "  MOBILE: " + mobile + " OPENID:" + openId + "====================");
  },

  /**
   * 生命周期函数--监听页面加载
   */
    onLoad: function (options) {
      // 获取订单信息，将订单信息转换为JSON对象
      var orderObj;
      var comCode="";
     
      if (options.tenantCode != null & options.tenantCode != "" && options.tenantCode != undefined) {
        comCode=options.tenantCode;
      }
      if (options.order != null & options.order != "" && options.order != undefined) {
        orderObj = JSON.parse(options.order);
      }
      var  shareId="";
      if(options.shareId != null & options.shareId != "" && options.shareId != undefined){
         shareId=options.shareId;
         comCode=options.shareId;
      }
      console.log("shareId:"+shareId);
      // app.senderLongitude = "";
      // app.senderLatitude = "";
      app.senderAddr = "";
      app.checkOrderLogin(shareId);

      var that = this;

      var longitude = '';
      var latitude = '';
      if (app.currentPosition && app.currentPosition.location) {
        longitude = app.currentPosition.location.lng;
        latitude = app.currentPosition.location.lat;
      }
      that.setData({
        tenantCode: comCode,
        saveActionFlag: false,
        longitude: longitude,
        latitude: latitude
      });

      if (options.jjfs == "smjh"){
        let tempradio = [
          { name: '0', value: '网点发货' },
          { name: '1', value: '上门取件', checked: 'true'}
        ];
        that.setData({ radioitems: tempradio, sendWay: "上门取件"});
      } else if (options.jjfs == "wdfh") {
        let tempradio = [
          { name: '0', value: '网点发货', checked: 'true' },
          { name: '1', value: '上门取件' }
        ];
        that.setData({ radioitems: tempradio, sendWay: "网点发货" });
      }
      if (undefined != options.sendc && "" != options.sendc && "null" != options.sendc){
        that.setData({ placeOfLoading: options.sendc });
      }
      //获取存储信息
      wx.getStorage({
        key: 'storage',
        success: function (res) {
          // success
          that.setData({
            storage: res.data
          })
        }
      });

      // 判断订单信息，根据订单信息找到订单对应公司网点的设置信息。
      if ((orderObj != null & orderObj != "" && orderObj != undefined)||shareId!=""){
        // 根据金融代码获取公司与设置信息
        var latitude = "";
        var longitude = "";
        if (app.latitude != undefined && app.latitude != null && app.latitude != "") {
          latitude = app.latitude;
        }
        if (app.longitude != undefined && app.longitude != null && app.longitude != "") {
          longitude = app.longitude;
        }
     
        wx.request({
          url: config.publicBasicinfoUrl,
          method: 'POST',
          header: {
            "content-type": "application/x-www-form-urlencoded"
          },
          data: {
            "userId": app.loginUser.id,
            "tenantCode": comCode, 
            //"tenantId": orderObj.tenantId,
            "latitude": latitude,
            "longitude": longitude
          },
          success: function (result) {
            var data = result.data;
            if (data.success) {
              app.setCompanyInfo(data.company)
            }

            // 3 中转公司  暂不需要
            // // 获取中转公司列表
            // var tempTransferCompanyArray = app.companyInfo.businessSetting.transferCompany;
            // // 拼装中转公司控件显示列表数据
            // var transferCompany = that.gettransferCompanyArrayName(tempTransferCompanyArray);
            // console.log(JSON.stringify(transferCompany))
            //网点数据
            //old:请求后台 getWebList 接口
            //now:发货网点已经设置到 app.companyInfo.businessSetting 中了，从这里获取即可
            that.setData({
              // 4 中转公司  暂不需要
              // transferCompanyArray: transferCompany,
              // transferCompanyObjArray: tempTransferCompanyArray,
              branchList: app.companyInfo.businessSetting.webList,
              searchPlaceOfLoadingList: app.companyInfo.businessSetting.webList
              
            });
            // //加载下单的基本信息
            that.loadingOrderBaseInfo(options);
          },
          fail: function ({ errMsg }) {

          },
        });
      }else{

        // 5 中转公司  暂不需要
        //  获取中转公司列表
        // var tempTransferCompanyArray = app.companyInfo.businessSetting.transferCompany;
        // // // 拼装中转公司控件显示列表数据
        // var transferCompany = that.gettransferCompanyArrayName(tempTransferCompanyArray);
        // console.log(JSON.stringify(transferCompany));
        //网点数据
        //old:请求后台 getWebList 接口
        //now:发货网点已经设置到 app.companyInfo.businessSetting 中了，从这里获取即可
        that.setData({
          // 6 中转公司  暂不需要
          // transferCompanyArray: transferCompany,
          // transferCompanyObjArray: tempTransferCompanyArray,
          branchList: app.companyInfo.businessSetting.webList,
          searchPlaceOfLoadingList: app.companyInfo.businessSetting.webList
          
        });
        // //加载下单的基本信息
        this.loadingOrderBaseInfo(options);
      }
      
      //修改
      if (options.order != null & options.order != "" && options.order != undefined){
        var order = JSON.parse(options.order);
        if (order != null & order != "" && order != undefined) {
          //获取一卡通信息
          wx.request({
            url: config.requestUrl + "order/vipCardInfo",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
            },
            data: { "code": that.data.tenantCode, "mobile": order.senderMobile },
            success: function (msg) {
              if (undefined != order.bankCardNumber && "" != order.bankCardNumber && "undefined" != order.bankCardNumber && "null" != order.bankCardNumber) {
                let isShowVipCardData = false;
                if (!Array.isArray(msg.data.content) && msg.data.content.length == 0) {
                  that.data.showVipOrBank = "bank";
                } else {
                  if (undefined != msg.data.content && Array.isArray(msg.data.content) && msg.data.content.length > 0) {
                    for (let i = 0; i < msg.data.content.length; i++) {
                      if (msg.data.content[i].vipNo == order.vipNumber && msg.data.content[i].accountNo == order.bankCardNumber) {
                        //展示vipcard
                        isShowVipCardData = true;
                        break;
                      }
                    }
                  }
                  if (isShowVipCardData) {
                    that.data.showVipOrBank = "vip";
                  } else {
                    that.data.showVipOrBank = "bank";
                  }
                }
              }
            },
            fail: function (msg) {

            },
            complete:function(){
              let tempradio = [
                { name: '0', value: '网点发货', checked: 'true' },
                { name: '1', value: '上门取件' }
              ];
              if (order.sendWay == "上门取件") {
                tempradio = [
                  { name: '0', value: '网点发货' },
                  { name: '1', value: '上门取件', checked: 'true' }
                ];
              }

              let locationAddrTemp = "";
              let senderAddressTemp = "";
              if (undefined != order.senderAddress && order.senderAddress.indexOf('⊙') != -1) {
                locationAddrTemp = order.senderAddress.split('⊙')[0];
                app.senderAddr = locationAddrTemp;
                if (order.senderAddress.split('⊙').length > 1) {
                  senderAddressTemp = order.senderAddress.split('⊙')[1];
                }
              } else {
                senderAddressTemp = order.senderAddress == undefined ? "" : order.senderAddress;
              }

              if (order.isDefaultSender=="1"){
                that.setData({
                  defSenderCheckBoxs: [{ name: '', value: '0', check: 'true' }],
                  isDefaultReceiverZheZhao: "0",
                  isDefaultReceiver: "1"
                });
              }
              if (order.isDefaultReceiver == "1") {
                that.setData({
                  defRecevierCheckBoxs: [{ name: '', value: '0', check: 'true' }],
                  isDefaultReceiverZheZhao: "0",
                  isDefaultReceiver: "1"
                });
              }
              


              that.setData({
                id: order.id,
                tenantId: order.tenantId,
                tenantCode: order.tenantCode,
                pickUpWay: order.pickUpWay,
                receiverCompany: order.receiverCompany,
                receiver: order.receiver,
                receiverMobile: order.receiverMobile,
                receiverAddress: order.receiverAddress,
                inputVipNoValueZheZhao: order.vipNumber,
                inputReceiverValueZheZhao: order.receiver,
                inputReceiverMobileValueZheZhao: order.receiverMobile,
                inputReceiverAddressValueZheZhao: order.receiverAddress,
                inputReceiverValue: order.receiver,
                inputReceiverMobileValue: order.receiverMobile,
                inputReceiverAddressValue: order.receiverAddress,
                sender: order.sender,
                senderMobile: order.senderMobile,
                senderAddress: order.senderAddress,
                inputSenderValueZheZhao: order.sender,
                inputSenderMobileValueZheZhao: order.senderMobile,
                locationAddr: locationAddrTemp,
                inputSenderAddressValueZheZhao: senderAddressTemp,
                inputSenderValue: order.sender,
                inputSenderMobileValue: order.senderMobile,
                inputSenderAddressValue: order.senderAddress,
                vipNumber: order.vipNumber,
                goodsName: order.goodsName,
                numberOfGoods: order.numberOfGoods,
                inputGoodsNameValueZheZhao: order.goodsName,
                inputNumberOfGoodsValueZheZhao: order.numberOfGoods,
                inputGoodsNameValue: order.goodsName,
                inputNumberOfGoodsValue: order.numberOfGoods,
                collectionOfMoney: order.collectionOfMoney,
                packageType: order.packageType,
                remarks: order.remarks,
                weight: order.weight,
                bulk: order.bulk,
                payWay: order.payWay,
                inputCollectionOfMoneyValue: order.collectionOfMoney,
                inputPackageTypeValue: order.packageType,
                inputPackageTypeValueZheZhao: order.packageType,
                inputRemarksValue: order.remarks,
                inputWeightValue: order.weight,
                inputBulkValue: order.bulk,
                inputWeightValueZheZhao: order.weight,
                inputBulkValueZheZhao: order.bulk,
                payWay: order.payWay,
                unloadPlace: order.unloadPlace,
                placeValue: order.unloadPlace,
                receiveCompany: order.receiveCompany,
                bankName: order.bankName,
                bankCardNumber: order.bankCardNumber,
                inputBankCardNoValueZheZhao: order.bankCardNumber,
                bankAccount: order.bankAccount,
                inputBankNameValue: order.bankName,
                inputBankCardNoValue: order.bankCardNumber,
                inputBankAccountValue: order.bankAccount,
                placeOfLoading: order.placeOfLoading,
                placeValueZheZhao: order.unloadPlace,
                sendWay: order.sendWay,
                isSaveSender: order.isSaveSender,
                isDefaultSender: order.isDefaultSender,
                isSaveReceiver: order.isSaveReceiver,
                isDefaultReceiver: order.isDefaultReceiver,
                userId: order.userId,
                radioitems: tempradio,
                inputsgdhValue: order.manualNumber
              });
            }
          });
          
        }
      }

    },

    login:function(e){
      wx.navigateTo({
        url: '../text/four/four',
      })
    },

    bindKeyInputGoodsName: function (e) {
      this.setData({
        inputGoodsNameValueZheZhao: e.detail.value
      })
    },

    bindKeyInputNumberOfGoods: function (e) {
      this.setData({
        inputNumberOfGoodsValueZheZhao: e.detail.value
      })
    },

    bindKeyInputWeight: function (e) {
      var values = e.detail.value.split(".");
      var value = "";
      if (values.length > 1) {
        if (values[1].length > 2) {
          value = values[0] + "." + values[1].substr(0, 2);
        } else {
          value = values[0] + "." + values[1];
        }
      } else {
        value = e.detail.value
      }
      this.setData({
        inputWeightValueZheZhao: value
      })
    },
    bindKeyInputBulk: function (e) {
      var values = e.detail.value.split(".");
      var value = "";
      if (values.length > 1) {
        if (values[1].length > 2) {
          value = values[0] + "." + values[1].substr(0, 2);
        } else {
          value = values[0] + "." + values[1];
        }
      } else {
        value = e.detail.value
      }
      this.setData({
        inputBulkValueZheZhao: value
      })
    },

    bindKeyInputCollectionOfMoney: function (e) {
      var values = e.detail.value.split(".");
      var value = "";
      if (values.length > 1) {
        if (values[1].length > 2) {
          value = values[0] + "." + values[1].substr(0, 2);
        } else {
          value = values[0] + "." + values[1];
        }
      } else {
        value = e.detail.value
      }
      this.setData({
        inputCollectionOfMoneyValue: value
      })
    },

    bindKeyInputPackageType: function (e) {
      this.setData({
        inputPackageTypeValueZheZhao: e.detail.value
      })
    },

    bindKeyInputRemarks: function (e) {
      this.setData({
        inputRemarksValue: e.detail.value
      })
    }, 

    bindKeyInputBankCardNo: function (e) {
      this.setData({
        inputBankCardNoValue: e.detail.value
      })
    },
    checkboxChangeSaveSender: function (e) {
      this.setData({
        isSaveSenderZheZhao: e.detail.value
      })
    },
    checkboxChangeDefSender: function (e) {
      let isDefaultSender = 0;
      if (e.detail.value && e.detail.value.length > 0 && e.detail.value[0] == 0){
        isDefaultSender = 1;
      }
      this.setData({
        isDefaultSenderZheZhao: e.detail.value,
        isDefaultSender: isDefaultSender

      })
    },
    checkboxChangeSaveRecevier: function (e) {
      this.setData({
        isSaveReceiverZheZhao: e.detail.value
      })
    },
    checkboxChangeDefRecevier: function (e) {
      let isDefaultReceiver = 0;
      if (e.detail.value && e.detail.value.length > 0 && e.detail.value[0] == 0) {
        isDefaultReceiver = 1;
      }
      this.setData({
        isDefaultReceiverZheZhao: e.detail.value,
        isDefaultReceiver: isDefaultReceiver
      })
    },
    radioChange: function (e) {
      if (e.detail.value=="0"){
        this.setData({
          sendWay: "网点发货"
        })
      } else {
        this.setData({
          sendWay: "上门取件"
        })
      }
    },

    bindKeyInputReceiver: function (e) {
      this.setData({
        inputReceiverValueZheZhao: e.detail.value
      })
    },

    bindKeyInputReceiverMobile: function (e) {
      this.setData({
        inputReceiverMobileValueZheZhao: e.detail.value,
        receiverMobile: ""
      })
    },

    bindKeyInputReceiverAddress: function (e) {
      this.setData({
        inputReceiverAddressValueZheZhao: e.detail.value,
        receiverAddress: ""
      })
    },


    bindKeyInputSender: function (e) {
      this.setData({
        inputSenderValueZheZhao: e.detail.value
      })
    },

    bindKeyInputSenderMobile: function (e) {
      this.setData({
        inputSenderMobileValueZheZhao: e.detail.value,
        SenderMobile: ""
      })
    },

    bindKeyInputSenderAddress: function (e) {
      this.setData({
        inputSenderAddressValueZheZhao: e.detail.value,
        SenderAddress: ""
      })
    },

    doSave: function (event) {
      var that = this;
      that.data.tenantCode;
      if (that.data.tenantCode == undefined){
        that.setData({
          tenantCode: app.companyInfo.code
        })
      }
      
      var totalTenants = that.data.totalTenants;
      var code = that.data.logisticsCompanyCode;
      var tenantId = that.data.logisticsCompanyId;
      if (that.data.sendWay =="网点发货"){
        if (that.data.placeOfLoading == "" || that.data.placeOfLoading == null || that.data.placeOfLoading==undefined){
          app.tip('请选择网点');
          return false;
        }
      } else {
        if (this.data.sendWay == '上门取件') {
          if (that.data.inputSenderAddressValue == undefined || that.data.inputSenderAddressValue == "") {
            app.tip('请选择发货人地址', 'none');
            return;
          }
        }
        // if (that.data.inputSenderAddressValue == "" || that.data.inputSenderAddressValue == null || that.data.inputSenderAddressValue == undefined) {
        //   app.tip('请输入发货人地址');
        //   return false;
        // }
      }

      if (that.data.placeValueZheZhao == undefined || that.data.placeValueZheZhao == null || that.data.placeValueZheZhao == "") {
        app.tip('请选择目的地');
        return false;
      }
      if (that.data.inputReceiverValue == undefined || that.data.inputReceiverValue == null || that.data.inputReceiverValue == "") {
        app.tip('请输入收货人');
        return false;
      }
      if (that.data.inputReceiverMobileValue == undefined || that.data.inputReceiverMobileValue == null || that.data.inputReceiverMobileValue == "") {
        app.tip('请输入收货人电话号');
        return false;
      }

      if (that.data.inputSenderValue == undefined || that.data.inputSenderValue == null || that.data.inputSenderValue == "") {
        app.tip('请输入发货人');
        return false;
      }

      if (!util.checkMobile(that.data.inputSenderMobileValue)) {
        app.tip('请输入正确的发货人电话号');
        return false;
      }

      if (!util.checkMobile(that.data.inputReceiverMobileValue)){
        app.tip('请输入正确的收货人电话号');
        return false;
      }
      if (that.data.inputGoodsNameValue == undefined || that.data.inputGoodsNameValue == null || that.data.inputGoodsNameValue == "") {
        app.tip('请输入货物名称');
        return false;
      }
      if (that.data.payWay == undefined || that.data.payWay == null || that.data.payWay == "") {
        app.tip('请选择付款方式');
        return false;
      }
      if (that.data.pickUpWay == undefined || that.data.pickUpWay == null || that.data.pickUpWay == "") {
        app.tip('请选择配送方式');
        return false;
      }
      if (that.data.inputNumberOfGoodsValue == undefined || that.data.inputNumberOfGoodsValue == null || that.data.inputNumberOfGoodsValue == ""){
        app.tip('请输入件数');
        return false;
      } else if (that.data.inputNumberOfGoodsValue == 0) {
        app.tip('请输入正确的件数');
        return false;
      }
      var packageType = ""
      if (!(that.data.inputPackageTypeValue == undefined || that.data.inputPackageTypeValue == null || that.data.inputPackageTypeValue == "")) {
        packageType = that.data.inputPackageTypeValue
      }
      var receiverCompany = ""
      if (!(that.data.inputReceiverCompanyValue == undefined || that.data.inputReceiverCompanyValue == null || that.data.inputReceiverCompanyValue == "")) {
        receiverCompany = that.data.inputReceiverCompanyValue
      }
      var receiverAddress = ""
      if (!(that.data.inputReceiverAddressValue == undefined || that.data.inputReceiverAddressValue == null || that.data.inputReceiverAddressValue == "")) {
        receiverAddress = that.data.inputReceiverAddressValue
      }

      //有代收时检测银行信息;
      var collectionOfMoney = "0"
      var inputBankNameValue = that.data.inputBankNameValue
      if (inputBankNameValue == undefined || inputBankNameValue == null) {
        inputBankNameValue = "";
      }
      var inputBankCardNoValue = that.data.inputBankCardNoValue
      if (inputBankCardNoValue == undefined || inputBankCardNoValue == null) {
        inputBankCardNoValue = "";
      }
      var inputBankAccountValue = that.data.inputBankAccountValue
      if (inputBankAccountValue == undefined || inputBankAccountValue == null) {
        inputBankAccountValue = "";
      }
      if (!(that.data.inputCollectionOfMoneyValue == undefined || that.data.inputCollectionOfMoneyValue == null || that.data.inputCollectionOfMoneyValue == "")) {
        //如果存在必须输入银行设置,验证是否存在银行信息;
        if (that.data.needBankFlag) {
          if ((inputBankCardNoValue == undefined || inputBankCardNoValue == null || inputBankCardNoValue == "")
            || (inputBankNameValue == undefined || inputBankNameValue == null || inputBankNameValue == "")) {
            app.tip('银行信息不能为空');
            return false;
          } else {
            //银行信息不为空的情况下;
            if (that.data.SameAsDefaultDankFlag) {
              if (inputBankNameValue != that.data.defaultBank) {
                app.tip('请输入' + that.data.defaultBank + '信息');
                return false;
              } else {
                collectionOfMoney = that.data.inputCollectionOfMoneyValue;
              }
            } else {
              collectionOfMoney = that.data.inputCollectionOfMoneyValue;
            }
          }
        } else {
          if (that.data.SameAsDefaultDankFlag) {
            if (!(inputBankNameValue == undefined || inputBankNameValue == null || inputBankNameValue == "")) {
              if (inputBankNameValue != that.data.defaultBank) {
                app.tip('请输入' + that.data.defaultBank + '信息');
                return false;
              } else {
                collectionOfMoney = that.data.inputCollectionOfMoneyValue;
              }
            } else {
              collectionOfMoney = that.data.inputCollectionOfMoneyValue;
            }
          } else {
            collectionOfMoney = that.data.inputCollectionOfMoneyValue;
          }
        }
      }

      var remark = ""
      if (!(that.data.inputRemarksValue == undefined || that.data.inputRemarksValue == null || that.data.inputRemarksValue == "")) {
        remark = that.data.inputRemarksValue
        if (!app.inputZhengZe(remark)) {
          app.tip("请删除备注中的特殊符号", "none");
          return false;
        }
      }
      var weight = ""
      if (!(that.data.inputWeightValue == undefined || that.data.inputWeightValue == null || that.data.inputWeightValue == "")) {
        weight = that.data.inputWeightValue
      }
      var bulk = ""
      if (!(that.data.inputBulkValue == undefined || that.data.inputBulkValue == null || that.data.inputBulkValue == "")) {
        bulk = that.data.inputBulkValue
      }
      var senderAddress = that.data.inputSenderAddressValue;
      if (senderAddress == undefined || senderAddress == null || senderAddress == "null") {
        senderAddress = "";
      }
      var vipNumber = that.data.inputVipNoValue;
      if (vipNumber == undefined || vipNumber == null || vipNumber == "null" || vipNumber == "") {
        vipNumber = app.loginUser.mobile;
      }

      if (!(that.data.inputsgdhValue == undefined || that.data.inputsgdhValue == null || that.data.inputsgdhValue == "")) {
        if (!app.inputZimuHanziShuziZhengZe(that.data.inputsgdhValue)) {
          app.tip("请删除手工单号中的特殊符号", "none");
          return false;
        }
      }
      
      let manualNumber = that.data.inputsgdhValue;
      var dhgs = "";
      if (!(that.data.dhgsValue == undefined || that.data.dhgsValue == null || that.data.dhgsValue == "")) {
        dhgs = that.data.dhgsValue;
      }
      var url = "";
      var tishimsg="";
      if (that.data.id == "" || that.data.id == null || that.data.id == undefined){
        url = "order/saveOrder";
        tishimsg = "恭喜您下单成功！";
      } else {
        url = "order/editOrder";
        tishimsg = "修改成功！";
      }
      wx.showModal({
        title: '提示',
        content: "是否确定提交运单",
        showCancel: true,
        success: function (res) { 
          if (res.confirm) {
            that.setData({
              saveActionFlag: true
            })
            let submitData = {
              data: '{' +
              '"id":"' + that.data.id + '",' +
              '"tenantId":"' + app.companyInfo.id + '",' +
              '"tenantCode":"' + that.data.tenantCode + '",' +
              '"pickUpWay": "' + that.data.pickUpWay + '",' +
              '"receiverCompany": "' + receiverCompany + '",' +
              '"receiver": "' + that.data.inputReceiverValue + '",' +
              '"receiverMobile": "' + that.data.inputReceiverMobileValue + '",' +
              '"receiverAddress": "' + receiverAddress + '",' +
              '"sender": "' + that.data.inputSenderValue + '",' +
              '"senderMobile": "' + that.data.inputSenderMobileValue + '",' +
              '"senderAddress": "' + senderAddress + '",' +
              '"vipNumber": "' + vipNumber + '",' +
              '"goodsName": "' + that.data.inputGoodsNameValue + '",' +
              '"numberOfGoods": "' + that.data.inputNumberOfGoodsValue + '",' +
              '"collectionOfMoney": "' + collectionOfMoney + '",' +
              '"packageType": "' + packageType + '",' +
              '"remarks": "' + remark + '",' +
              '"weight": "' + weight + '",' +
              '"bulk": "' + bulk + '",' +
              '"payWay": "' + that.data.payWay + '",' +
              '"unloadPlace": "' + that.data.placeValueZheZhao + '",' +
              '"receiveCompany": "' + dhgs + '",' +
              '"bankName": "' + inputBankNameValue + '",' +
              '"bankCardNumber": "' + inputBankCardNoValue + '",' +
              '"bankAccount": "' + inputBankAccountValue + '",' +
              '"manualNumber": "' + (manualNumber == undefined ? "" : manualNumber) + '",' +
              '"placeOfLoading": "' + (that.data.placeOfLoading == undefined ? "" : that.data.placeOfLoading) + '",' +
              '"sendWay": "' + that.data.sendWay + '",' +
              '"isSaveSender": "' + (that.data.isSaveSender == undefined ? 0 : that.data.isSaveSender) + '",' +
              '"isDefaultSender": "' + (that.data.isDefaultSender == undefined ? 0 : that.data.isDefaultSender) + '",' +
              '"isSaveReceiver": "' + (that.data.isSaveReceiver == undefined ? 0 : that.data.isSaveReceiver) + '",' +
              '"isDefaultReceiver": "' + (that.data.isDefaultReceiver == undefined ? 0 : that.data.isDefaultReceiver) + '",' +
              '"senderLongitude": "' + (that.data.senderLongitude == undefined ? "" : that.data.senderLongitude) + '",' +
              '"senderLatitude": "' + (that.data.senderLatitude == undefined ? "" : that.data.senderLatitude) + '",' +
               //  '"transferCompanyName": "' + (that.data.transferCompanyName == undefined ? "" : that.data.transferCompanyName) + '",' +
               //  '"transferCompanyCode": "' + (that.data.transferCompanyCode == undefined ? "" : that.data.transferCompanyCode) + '",' +
              '"userId": "' + app.loginUser.id + '"}'
            };
            console.log(submitData)
            wx.request({
              url: config.requestUrl + url,
              method: "POST",
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              data: submitData,
              success: function (msg) {
                wx.hideLoading()
                if (msg.data.msg != null && msg.data.msg == "成功") {

                  wx.showModal({
                    title: '提示',
                    content: tishimsg,
                    showCancel: true,
                    confirmText:"再来一单",
                    cancelText:"查看运单",
                    success: function (res) { 
                      if (res.confirm) {
                        wx.redirectTo({
                          url: 'order?tenantCode=' + that.data.tenantCode,
                        })
                      } else {
                        wx.redirectTo({
                          url: 'myOrderList'
                        })
                      }
                    },
                  })

                } else {
                  app.tip(msg.data.msg, "none");
                  return;
                }
              },
              fail: function (msg) {
                wx.showModal({
                  title: '提示',
                  content: msg.data.msg,
                  showCancel: false
                })
              }
            })
          }
        }
      })
    },




  /**
   * 显示遮罩层
   */
  showMask: function (e) {
    var flag = e.currentTarget.dataset.index;
    var that = this;
    switch (flag) {
      case "placeOfLoading":
        if (that.data.searchPlaceOfLoadingList == undefined || that.data.searchPlaceOfLoadingList.length <= 0){
          app.tip("没有设置发货网点！", "none");
          return false;
        }
        that.setData({ placeOfLoadingMask: 'block', display: "block", isshowindex: 'none'})
        break;
      case "createSender": 
        let tempisshowvipcard = false;
        let tempisshowbank = false;
        that.data.tempshowvipcardnodata = false;
        if (!checkNull(that.data.inputBankNameValue) && !checkNull(that.data.inputBankAccountValue) && !checkNull(that.data.inputBankCardNoValueZheZhao) && !checkNull(that.data.inputVipNoValueZheZhao) && undefined != that.data.vipNoArray) {
          for (let i = 0; i < that.data.vipNoArray.length; i++) {
            if (that.data.inputVipNoValueZheZhao == that.data.vipNoArray[i].vipNo) {
              tempisshowvipcard = true;
              break;
            }
          }
        }
        if(that.data.showVipOrBank == "bank"){
          tempisshowvipcard = false;
          tempisshowbank = true;
        }else if (that.data.showVipOrBank == "vip") {
          tempisshowvipcard = true;
          tempisshowbank = false;
        }
        that.setData({ createSenderMask: 'block', display: "block", isShowVipCardInfo: tempisshowvipcard, isShowBankInfo: tempisshowbank, tempshowvipcardnodata: that.data.tempshowvipcardnodata, isshowindex:'none'})
        break;
      case "sender":
        if (that.data.senderArray.length == 0) {
          app.tip("您还没有关联的发货人呢！", "none");
          return false;
        }
        that.setData({ senderMask: 'block', display: "block", isshowindex: 'none'})
        break;
      //货物信息
      case "goods":
        that.setData({ goodsMask: 'block', display: "block", isshowindex: 'none'})
        break;
      case "createReceiver":
        that.setData({ createReceiverMask: 'block', display: "block", isshowindex: 'none' })
        break;
      case "receiver":
        if (this.data.receivers.length == 0) {
          app.tip("您还没有关联的收货人呢！", "none");
          return false;
        }
        that.setData({ receiverMask: 'block', display: "block", isshowindex: 'none'})
        break;
      case "senderBank":
        // if (that.data.bankArray.length == 0) {
        //   app.tip("您还没有绑定银行卡呢！", "none");
        //   return false;
        // }
        that.setData({
          senderBankMask: 'block', display: "block", isshowindex: 'none'
        })
        break;
      case "vipCard":
        if (that.data.inputSenderMobileValueZheZhao == "" || that.data.inputSenderMobileValueZheZhao == undefined || that.data.inputSenderMobileValueZheZhao == null) {
          app.tip("请填写手机号", "none");
          return false;
        }

        //获取一卡通信息
        wx.request({
          url: config.requestUrl + "order/vipCardInfo",
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
          },
          data: { "code": that.data.tenantCode, "mobile": that.data.inputSenderMobileValueZheZhao },
          success: function (msg) {
            if (msg.data.content.length == 0) {
              app.tip("您还没有VIP会员卡呢！", "none");
              return false;
            } else {
              that.setData({
                vipCardMask: 'block', display: "block", isshowindex: 'none'
              })
            }
            that.setData({
              vipNoArray: msg.data.content
            })
          },
          fail: function (msg) {
            that.setData({
              showView: false,
              tempshowvipcardnodata :true,
              message: "抱歉，您还没有会员卡呢！",
              message1: "快到物流公司办理吧！",
              imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/banka.png?sign=395db97191c465864a45990acfd9dabd&t=1570583848"
            });

          }
        });
        break;
      case "unloadPlace":
        that.setData({ unloadPlaceMask: 'block', display: "block", isshowindex: 'none' })
        break;
    }
  },
  onShow:function(){
    if (undefined != app.senderAddr && "" != app.senderAddr){
      this.setData({ locationAddr: app.senderAddr });
    }
  },

  /**
   * 获取发货人当前位置
   */
  getSenderLocation : function(e){

    var _this = this;
    wx.chooseLocation({
      success: function (res) {
        wx.hideLoading();
        if (null != res.address && "" != res.address && null != res.name && "" != res.name) {
          if (res.address != res.name) {
            app.senderAddr = res.address + res.name;
          } else {
            app.senderAddr = res.address;

          }
          _this.setData({ 
            locationAddr: app.senderAddr, 
            senderLongitude: res.longitude, 
            senderLatitude: res.latitude 
          });
        }
        // wx.navigateTo({
        //   url: "/pages/index/index?address=" + res.name
        // });
      },
      fail: function (err) {
        app.tip("未授权获取位置信息");
      }
    });
  },

  hideMask: function(e) {
    var flag = e.currentTarget.dataset.index;
    var that = this;
    switch (flag) {
      case "placeOfLoading":
        that.setData({ placeOfLoadingMask: 'none', display: 'none', isshowindex: "block" })
        break;
      case "createSender":
        that.setData({ createSenderMask: 'none', display: 'none', isshowindex: "block" })
        break;
      case "sender":
        that.setData({ senderMask: 'none', display: 'none', isshowindex: "block" })
        break;
      case "goods":
        that.setData({ goodsMask: 'none', display: 'none', isshowindex: "block" })
        break;
      case "createReceiver": 
        that.setData({ createReceiverMask: 'none', display: 'none', isshowindex: "block" })
        break;
      case "receiver":
        that.setData({ receiverMask: 'none', display: 'none', isshowindex: "block" })
        break;
      case "senderBank":
        that.setData({
          senderBankMask: 'none', display: 'none', isshowindex: "block"
          })
        break;
      case "vipCard":
        // that.setData({ vipCardMask: 'none', display: 'none'})
        that.setData({
          vipCardMask: 'none',
          display: 'none', isshowindex: "block"
        });
        break;
      case "unloadPlace":
        that.setData({ unloadPlaceMask: 'none', display: 'none', isshowindex: "block"})
        break;
    }
  },

  doChoose(e) {

    //隐藏遮罩
    this.setData({ display: "none" })
    
    var flag = e.currentTarget.dataset.index;
    switch (flag) {
      case "placeOfLoading":
        this.setData({
          placeOfLoadingMask: 'none',
          display: 'none',
          placeOfLoading: this.data.searchPlaceOfLoadingList[e.currentTarget.dataset.id].name,
          isshowindex: "block"
        })
        break;
      case "createSender":
        if (this.data.inputSenderValueZheZhao == "" || this.data.inputSenderValueZheZhao == null || this.data.inputSenderValueZheZhao == undefined) {
          app.tip("请输入发货人姓名", "none");
          return false;
        } else {
          if (!app.inputZhengZe(this.data.inputSenderValueZheZhao)) {
            app.tip("请删除发货人姓名中的特殊符号", "none");
            return false;
          }
        }
        if (this.data.inputSenderMobileValueZheZhao == "" || this.data.inputSenderMobileValueZheZhao == null || this.data.inputSenderMobileValueZheZhao == undefined) {
          app.tip("请输入手机号", "none");
          return false;
        } else {
          // var regMobile = /^[1][3,4,5,7,8][0-9]{9}$/
          // if (!regMobile.test(this.data.inputSenderMobileValueZheZhao)) {
          //   app.tip("手机号码格式不正确", "none");
          //   return false;
          // }
        }
        if (this.data.sendWay == '上门取件'){
          if (app.senderAddr == undefined || app.senderAddr == "") {
            app.tip('请选择地址', 'none');
            return;
          }
        }
        var menpaihao = "";
        if (this.data.inputSenderAddressValueZheZhao != undefined && this.data.inputSenderAddressValueZheZhao != null) {
          menpaihao = this.data.inputSenderAddressValueZheZhao
        }
        this.setData({
          createSenderMask: 'none',
          display: 'none',
          inputVipNoValue: this.data.inputVipNoValueZheZhao,
          inputBankCardNoValue: this.data.inputBankCardNoValueZheZhao,
          inputSenderValue: this.data.inputSenderValueZheZhao,
          inputBankNameValue: this.data.inputBankNameValue,
          inputBankAccountValue: this.data.inputBankAccountValue,
          inputBankCardNoValueZheZhao: this.data.inputBankCardNoValueZheZhao,
          inputVipNoValueZheZhao: this.data.inputVipNoValueZheZhao,
          inputSenderMobileValue: this.data.inputSenderMobileValueZheZhao,
          inputSenderAddressValue: this.data.locationAddr + "⊙" + menpaihao,
          isSaveSender: this.data.isSaveSenderZheZhao,
          isDefaultSender: this.data.isDefaultSender,
          isshowindex:"block"
        })
        break;
      case "sender":
        var senderInfo = this.data.senderArray[e.currentTarget.dataset.id];
        let inputSenderAddressValueZheZhaoTemp = "";
        let locationAddrTemp = "";
        if (undefined != senderInfo.contactAddress && senderInfo.contactAddress.indexOf('⊙') != -1) {
          locationAddrTemp = senderInfo.contactAddress.split('⊙')[0];
          app.senderAddr = locationAddrTemp;
          if (senderInfo.contactAddress.split('⊙').length > 1) {
            inputSenderAddressValueZheZhaoTemp = senderInfo.contactAddress.split('⊙')[1];
          }
        }else{
          inputSenderAddressValueZheZhaoTemp = senderInfo.contactAddress == undefined ? "" : senderInfo.contactAddress;
        }
        this.setData({
          senderLongitude: senderInfo.longitude,
          senderLatitude: senderInfo.latitude,
          inputSenderValue: senderInfo.contactName,
          inputSenderMobileValue: senderInfo.contactMobile,
          inputSenderAddressValue: senderInfo.contactAddress,
          inputSenderValueZheZhao: senderInfo.contactName,
          inputSenderMobileValueZheZhao: senderInfo.contactMobile,
          locationAddr: locationAddrTemp,
          inputSenderAddressValueZheZhao: inputSenderAddressValueZheZhaoTemp,
          senderMask: 'none',
          display: 'none',
          isshowindex: "block"
        })
        break;
      case "receiver":
        var receiverInfo = this.data.receiverArray[e.currentTarget.dataset.id];
        this.setData({
          inputReceiverValue: receiverInfo.contactName,
          inputReceiverMobileValue: receiverInfo.contactMobile,
          inputReceiverAddressValue: receiverInfo.contactAddress,
          inputReceiverValueZheZhao: receiverInfo.contactName,
          inputReceiverMobileValueZheZhao: receiverInfo.contactMobile,
          inputReceiverAddressValueZheZhao: receiverInfo.contactAddress,
          receiverMask: 'none',
          display: 'none',
          isshowindex: "block"
        })
      case "createReceiver":

        if (this.data.inputReceiverValueZheZhao == "" || this.data.inputReceiverValueZheZhao == null || this.data.inputReceiverValueZheZhao == undefined) {
          app.tip("请输入收货人姓名", "none");
          return false;
        } else {
          if (!app.inputZhengZe(this.data.inputReceiverValueZheZhao)) {
            app.tip("请删除收货人姓名中的特殊符号", "none");
            return false;
          }
        }

        if (this.data.inputReceiverMobileValueZheZhao == "" || this.data.inputReceiverMobileValueZheZhao == null || this.data.inputReceiverMobileValueZheZhao == undefined) {
          app.tip("请输入手机号", "none");
          return false;
        } else {
          // var regMobile = /^[1][3,4,5,7,8][0-9]{9}$/
          // if (!regMobile.test(this.data.inputReceiverMobileValueZheZhao)) {
          //   app.tip("手机号码格式不正确", "none");
          //   return false;
          // }
        }
        this.setData({
          createReceiverMask: 'none',
          display: 'none',
          inputReceiverValue: this.data.inputReceiverValueZheZhao,
          inputReceiverMobileValue: this.data.inputReceiverMobileValueZheZhao,
          inputReceiverAddressValue: this.data.inputReceiverAddressValueZheZhao,
          isSaveReceiver: this.data.isSaveReceiverZheZhao,
          isDefaultReceiver: this.data.isDefaultReceiver,
          placeValue: this.data.placeValueZheZhao,
          isshowindex: "block"
        })
        break;
      case "senderBank":
        var targetBankInfo = this.data.bankArray[e.currentTarget.dataset.id];
        var number = targetBankInfo.bankCardNo;
        var bankCardNoFour = number.substr(number.length - 4);
        this.setData({
          inputBankNameValue: targetBankInfo.bankName,
          inputBankAccountValue: targetBankInfo.bankAccount,
          inputBankCardNoValueZheZhao: targetBankInfo.bankCardNo,
          inputVipNoValueZheZhao: "",
          bankCardNoFour: bankCardNoFour,
          senderBankMask: 'none',
          display: 'none', 
          isShowBankInfo: true,
          isShowVipCardInfo: false,
          showVipOrBank: "bank",
          isshowindex: "block"
        })
        break;
      case "vipCard":
        var targetVipNoInfo = this.data.vipNoArray[e.currentTarget.dataset.id];
        this.setData({
          inputBankNameValue: targetVipNoInfo.bankName,
          inputBankAccountValue: targetVipNoInfo.accountName,
          inputBankCardNoValueZheZhao: targetVipNoInfo.accountNo,
          inputVipNoValueZheZhao: targetVipNoInfo.vipNo,
          vipCardMask: 'none',
          display: 'none',
          isShowBankInfo: false,
          isShowVipCardInfo: true,
          showVipOrBank: "vip",
          isshowindex: "block"
        })
        break;
      case "goods":
        if (this.data.inputGoodsNameValueZheZhao == "" || this.data.inputGoodsNameValueZheZhao == null || this.data.inputGoodsNameValueZheZhao == undefined) {
          app.tip("请输入货物名称", "none");
          return false;
        } else {
          if (!app.inputZhengZe(this.data.inputGoodsNameValueZheZhao)) {
            app.tip("请删除货物名称中的特殊符号", "none");
            return false;
          }
        }
        if (this.data.inputNumberOfGoodsValueZheZhao == "" || this.data.inputNumberOfGoodsValueZheZhao == null || this.data.inputNumberOfGoodsValueZheZhao == undefined) {
          app.tip("请输入件数", "none");
          return false;
        }
        if (this.data.inputPackageTypeValueZheZhao != null && this.data.inputPackageTypeValueZheZhao != "" && this.data.inputPackageTypeValueZheZhao != undefined) {
          if (!app.inputZhengZe(this.data.inputPackageTypeValueZheZhao)) {
            app.tip("请删除包装样式中的特殊符号", "none");
            return false;
          }
        }
        this.setData({
          goodsMask: 'none',
          display: 'none',
          inputGoodsNameValue: this.data.inputGoodsNameValueZheZhao,
          inputNumberOfGoodsValue: this.data.inputNumberOfGoodsValueZheZhao,
          inputWeightValue: this.data.inputWeightValueZheZhao,
          inputBulkValue: this.data.inputBulkValueZheZhao,
          inputPackageTypeValue: this.data.inputPackageTypeValueZheZhao,
          isshowindex: "block"
        })
        break;
      case "unloadPlace":
        this.setData({
          unloadPlaceMask: 'none',
          display: 'none',
          placeValueZheZhao: this.data.searchUnloadPlaceList[e.currentTarget.dataset.id].XHDD,
          
          dhgsValue: this.data.searchUnloadPlaceList[e.currentTarget.dataset.id].DYDHGS,
          isshowindex: "block"
        })
        break;
    }
  },
  
  /**
   * 根据输入框查询发货网点或者目的地（条件：网点名称或目的地是否包含输入的值）
   */
  doSearch: function (e) {

    //发货网点或目的地的标识
    var flag = e.currentTarget.dataset.index;

    //文本框的值
    var name = app.trim(e.detail.value);

    //发货网点查询
    if (flag == 'placeOfLoading'){
      var arrayObj = new Array();
      var that = this;
      var arr = that.data.branchList;
      if (name.length > 0) {
        for (var i = 0; i < arr.length; i++) {
          if (arr[i].name.indexOf(name) >= 0) {
            arrayObj.push(arr[i]);
          }
        }
      } else {
        arrayObj = that.data.branchList;
      }
      that.setData({
        placeOfLoadingInputValue: name,
        searchPlaceOfLoadingList: arrayObj
      })

    //目的地查询
    } else if (flag == 'unloadPlace'){
      var arrayObj = new Array();
      var that = this;
      var arr = that.data.unloadPlaceArray;
      if (name.length > 0) {
        for (var i = 0; i < arr.length; i++) {
          if (arr[i].XHDD.indexOf(name) >= 0) {
            arrayObj.push(arr[i]);
          }
        }
      } else {
        arrayObj = that.data.unloadPlaceArray;
      }
      that.setData({
        UnloadPlaceInputValue: name,
        searchUnloadPlaceList: arrayObj
      })
    }
  }, 

  /**
   * 发货网点弹出层，点击网点发货或上门取件，发货页面显示该网点，发货方式勾选该焦点控件的值
   */
  chooseSendWay: function (e) {

    //网点发货 0 上门取件 1
    var flag = e.currentTarget.dataset.index;

    //网点名称
    var name = e.currentTarget.dataset.id;
    var sendWay ="网点发货"
    let tempradio = [
      { name: '0', value: '网点发货', checked: 'true' },
      { name: '1', value: '上门取件' }
    ];
    if (flag == 1){
      tempradio = [
        { name: '0', value: '网点发货' },
        { name: '1', value: '上门取件', checked: 'true' }
      ];
      var sendWay = "上门取件"
    }
    this.setData({
      placeOfLoadingMask: 'none',
      placeOfLoading: name,
      radioitems: tempradio,
      sendWay: sendWay
    })
    


  },
  //扫描手工单号
  toScanQRcode:function() {
    let _this = this;
    wx.scanCode({
      success: (res) => {
        _this.setData({ inputsgdhValue: res.result});
      },
      fail: (res) => {
        _this.setData({ inputsgdhValue: _this.data.inputsgdhValue == undefined ? "" : _this.data.inputsgdhValue});
        if (res.errMsg.indexOf("cancel") == -1) {
          wx.showModal({
            title: '提示',
            content: "请扫描正确手工单号",
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
              }
            }
          })
        }
        return false
      }
    })
  },

  //手工单号
  bindinputsgdh: function (e) {
    this.setData({ inputsgdhValue: e.detail.value});
  },

  //---加载下单的基本信息---//
  //1.目的地
  //2.业务相关设置 : 小程序代收是否必须输入银行信息
  //3.业务相关设置 : 小程序有代收货款必须与默认开户银行一致
  //4.业务相关设置 : 默认银行
  //5.业务相关设置 : 配送（提货）方式
  //6.默认的付款方式
  //7.默认的配送（提货）方式
  //8.付款方式
  //9.包装样式
  //10.发货人
  //11.默认发货人
  //12.收货人
  //13.默认收货人
  //14.发货网点（从认证中心获取的子公司信息）
  //15.绑定的银行卡信息
  loadingOrderBaseInfo: function (options){
    var needBankFlag = app.companyInfo.businessSetting.needBankFlag;
    var defaultBank = app.companyInfo.businessSetting.defaultBank;
    var SameAsDefaultDankFlag = app.companyInfo.businessSetting.SameAsDefaultDankFlag;
    var unloadPlaces = app.companyInfo.businessSetting.unloadPlaces;
    var receiverContactInfos = app.companyInfo.businessSetting.receiverContactInfos;
    for(let i = 0; i<app.companyInfo.businessSetting.senderContactInfos.length; i++) {
      if (app.companyInfo.businessSetting.senderContactInfos[i].contactAddress != null) {
        app.companyInfo.businessSetting.senderContactInfos[i].contactAddressTemp = app.companyInfo.businessSetting.senderContactInfos[i].contactAddress.replace('⊙', '');
      }
    }
    var senderContactInfos = app.companyInfo.businessSetting.senderContactInfos;
    var defaultReceiver = app.companyInfo.businessSetting.defaultReceiver;
    var defaultSender = app.companyInfo.businessSetting.defaultSender;
    var receivers = [];

    if (receiverContactInfos != undefined && receiverContactInfos != null && receiverContactInfos != "") {
      for (var i = 0; i < receiverContactInfos.length; i++) {
        receivers[i] = receiverContactInfos[i];
      }
    } else {
      receivers = ["暂无可选择数据"]
    }
    if (receivers != "" && receivers.length > 0) {
    } else {
      receivers = ["暂无可选择数据"]
    }

    //获取-常用货物名称
    var selGoodsNames = app.companyInfo.businessSetting.selGoodsNames;
    if (selGoodsNames != null && selGoodsNames != undefined && selGoodsNames.length > 0) {
      this.setData({
        selGoodsNames: selGoodsNames
      })
    }
    //获取-包装
    var packingStyles = app.companyInfo.businessSetting.packingStyles;
    if (packingStyles != null && packingStyles != undefined && packingStyles.length > 0) {
      this.setData({
        packingStyles: packingStyles
      })
    }


    var that = this;
    // 根据用户openId 获取用户默认收发货人信息
    wx.request({
      url: config.requestUrl + "senderContactInfo/getDefaultReceiverSenderByOpenId",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: { "openId": app.openid },
      success: function (data) {
        console.info(data.data.content);
        if (data.data.code == 100) {
          var defaultContent = data.data.content;
          if (defaultContent != null) {
            // 获取最新收货人信息  进行设置
            defaultReceiver = defaultContent.defaultReceiver;
            if (defaultReceiver != null && defaultReceiver != undefined) {
              // 设置收货人信息
              that.setData({
                inputReceiverValue: defaultReceiver.contactName,
                inputReceiverMobileValue: defaultReceiver.contactMobile,
                inputReceiverAddressValue: defaultReceiver.contactAddress,
                inputReceiverValueZheZhao: defaultReceiver.contactName,
                inputReceiverMobileValueZheZhao: defaultReceiver.contactMobile,
                inputReceiverAddressValueZheZhao: defaultReceiver.contactAddress,
                defRecevierCheckBoxs: [{ name: '', value: '0', check: 'true' }],
                isDefaultReceiverZheZhao: "0",
                isDefaultReceiver: "1"
              })
            }
            console.log("=======================================================");
            console.log("默认收货人: " + JSON.stringify(defaultReceiver));

            // 获取最新发货人信息 进行设置
            defaultSender = defaultContent.defaultSender
            if (defaultSender != null && defaultSender != undefined) {
              var address = defaultSender.contactAddress.split("⊙");
              that.setData({
                inputSenderValue: defaultSender.contactName,
                inputSenderMobileValue: defaultSender.contactMobile,
                inputSenderAddressValue: defaultSender.contactAddress,
                inputSenderValueZheZhao: defaultSender.contactName,
                inputSenderMobileValueZheZhao: defaultSender.contactMobile,
                inputSenderAddressValueZheZhao: address[1],
                locationAddr: address[0],
                defSenderCheckBoxs: [{ name: '', value: '0', check: 'true' }],
                isDefaultSenderZheZhao: "0",
                isDefaultSender: "1"
              })
            }
            console.log("=======================================================");
            console.log("默认发货人: " + JSON.stringify(defaultSender));
          }

        }
      },
      fail: function (msg) {

      }
    });

    // if (defaultSender != null && defaultSender != undefined && defaultSender != "" && options.order == undefined) {
    //   var address = defaultSender.contactAddress.split("⊙");
    //   this.setData({
    //     inputSenderValue: defaultSender.contactName,
    //     inputSenderMobileValue: defaultSender.contactMobile,
    //     inputSenderAddressValue: defaultSender.contactAddress,
    //     inputSenderValueZheZhao: defaultSender.contactName,
    //     inputSenderMobileValueZheZhao: defaultSender.contactMobile,
    //     inputSenderAddressValueZheZhao: address[1],
    //     locationAddr: address[0],
    //     defSenderCheckBoxs: [{ name: '', value: '0', check: 'true' }],
    //     isDefaultSenderZheZhao: "0",
    //     isDefaultSender: "1"
    //   })
    // }
    // if (defaultReceiver != null && defaultReceiver != undefined && defaultReceiver != "" && options.order == undefined) {
    //   this.setData({
    //     inputReceiverValue: defaultReceiver.contactName,
    //     inputReceiverMobileValue: defaultReceiver.contactMobile,
    //     inputReceiverAddressValue: defaultReceiver.contactAddress,
    //     inputReceiverValueZheZhao: defaultReceiver.contactName,
    //     inputReceiverMobileValueZheZhao: defaultReceiver.contactMobile,
    //     inputReceiverAddressValueZheZhao: defaultReceiver.contactAddress,
    //     defRecevierCheckBoxs: [{ name: '', value: '0', check: 'true' }],
    //     isDefaultReceiverZheZhao: "0",
    //     isDefaultReceiver: "1"
    //   })
    // }

    //绑定的银行卡信息
    var bankInfoList = [];
    var bankReturnResult = app.companyInfo.businessSetting.vipBankList;
    if (bankReturnResult != undefined && bankReturnResult != null && bankReturnResult != "") {
      for (var i = 0; i < bankReturnResult.length; i++) {
        bankInfoList[i] = bankReturnResult[i];
      }
    }

    //字段扩展信息
    var orderColumnExtResult = app.companyInfo.businessSetting.orderColumnExtList;
    if (orderColumnExtResult != undefined && orderColumnExtResult != null && orderColumnExtResult != "") {
      for (var i = 0; i < orderColumnExtResult.length; i++) {
        if (orderColumnExtResult[i].useFlag==1){
          //console.info(orderColumnExtResult[i].columnKey);
          if (orderColumnExtResult[i].columnKey =="手工单号"){
            this.setData({
              sgdhShowFlag: true
            })
          } else if (orderColumnExtResult[i].columnKey == "银行卡号"){
            this.setData({
              bankShowFlag: true
            })
          } else if (orderColumnExtResult[i].columnKey == "一卡通") {
            this.setData({
              vipCardShowFlag: true
            })
          } else if (orderColumnExtResult[i].columnKey == "重量") {
            this.setData({
              weightShowFlag: true
            })
          } else if (orderColumnExtResult[i].columnKey == "体积") {
            this.setData({
              bulkShowFlag: true
            })
          } else if (orderColumnExtResult[i].columnKey == "包装") {
            this.setData({
              packingStylesShowFlag: true
            })
          } else if (orderColumnExtResult[i].columnKey == "代收货款") {
            this.setData({
              collectionOfMoneyShowFlag: true
            })
          }
        }
      }
    }

    this.setData({
      unloadPlaceIndex: 0,
      unloadPlaceArray: app.companyInfo.businessSetting.unloadPlaces,
      searchUnloadPlaceList: app.companyInfo.businessSetting.unloadPlaces,
      receiverIndex: 0,
      receiverArray: receivers,
      senderArray: senderContactInfos,
      receivers: receiverContactInfos,
      defaultReceiver: defaultReceiver,
      defaultSender: defaultSender,
      needBankFlag: needBankFlag,
      defaultBank: defaultBank,
      SameAsDefaultDankFlag: SameAsDefaultDankFlag,
      pickUpWayArray: app.companyInfo.businessSetting.deliveryMethodsArr,
      payWay: app.companyInfo.businessSetting.defaultPayment,
      pickUpWay: app.companyInfo.businessSetting.defaultDeliveryMethod,
      payWayArray: app.companyInfo.businessSetting.paymentArr,
     //  transferCompanyObjArray: app.companyInfo.businessSetting.transferCompany,
      bankArray: bankInfoList
    })
},

  gettransferCompanyArrayName:function(companyArray){
    var resultArray = [];
    if (companyArray != null && companyArray != undefined) {
      for (var i = 0; i < companyArray.length; i++) {
        var temp = companyArray[i];
        if (temp != null && temp != undefined) {
          resultArray[i] = temp.transferCompanyName;
          console.log(resultArray[i] );
        }
      }
    }

    return resultArray;
  }


});


/**
 * 验证空
 */
function checkNull(obj){
  if(undefined != obj && 'undefined' != obj && 'null' != obj && "" != obj){
    return false;
  }
  return true;
}
