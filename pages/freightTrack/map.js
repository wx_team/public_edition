// pages/freightTrack/map.js
// 引入SDK核心类
var QQMapWX = require('../../utils/qqmap-wx-jssdk.js');

// 实例化API核心类
var qqmapsdk = new QQMapWX({
  key: 'RPTBZ-NX6LJ-ETVF5-KRBHK-GAFM6-SIFKA' // 必填
});
var config = require('../../config.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    showMap(app, this);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //在Page({})中使用下列代码
  //数据回填方法
  backfill: function (e) {
    var id = e.currentTarget.id;
    var lng="";
    var lat = "";
    for (var i = 0; i < this.data.suggestion.length; i++) {
      if (i == id) {
        this.setData({
          backfill: this.data.suggestion[i].title
        });
        lat = this.data.suggestion[i].latitude;
        lng = this.data.suggestion[i].longitude; 
      }
    }

    this.setData({
      flag: true,
      tab_wd: "",
      tab_dt: "map_2_h",
      showView: false,
      mapc: "show",
      tabbtn_h: false,
      isLocation: "",
      lng: lng,
      lat: lat,
      controls: [{
        id: 1,
        iconPath: 'https://7075-public-bb1cff-1257643776.tcb.qcloud.la/jian.png?sign=57721f87a1e11c34a1efb44caf2c8608&t=1570585942',
        position: {
          left: 260,
          top: 20,
          width: 30,
          height: 30
        },
        clickable: true
      },
      {
        id: 2,
        iconPath: 'https://7075-public-bb1cff-1257643776.tcb.qcloud.la/jia.png?sign=c556c26f83553d3561df985d7d406366&t=1570585893',
        position: {
          left: 310,
          top: 20,
          width: 30,
          height: 30
        },
        clickable: true
      }
      ],
      markers: [{
        id: "1",
        label: {
          content: "aa",
          textAlign: "center",
          color: "#9932CC"
        },
        latitude: lat,
        longitude: lng,
        width: 50,
        height: 50,
        iconPath: "",
        title: "aa"
      }]
    });

  },

  //触发关键词输入提示事件
  getsuggest: function (e) {
    var _this = this;
    _this.setData({
      flag: false
    });
    //调用关键词提示接口
    qqmapsdk.getSuggestion({
      //获取输入框值并设置keyword参数
      keyword: e.detail.value, //用户输入的关键词，可设置固定值,如keyword:'KFC'
      //region:'北京', //设置城市名，限制关键词所示的地域范围，非必填参数
      success: function (res) {//搜索成功后的回调
        console.log(res);
        var sug = [];
        for (var i = 0; i < res.data.length; i++) {
          sug.push({ // 获取返回结果，放到sug数组中
            title: res.data[i].title,
            id: res.data[i].id,
            addr: res.data[i].address,
            city: res.data[i].city,
            district: res.data[i].district,
            latitude: res.data[i].location.lat,
            longitude: res.data[i].location.lng
          });
        }
        _this.setData({ //设置suggestion属性，将关键词搜索结果以列表形式展示
          suggestion: sug
        });
      },
      fail: function (error) {
        console.error(error);
      },
      complete: function (res) {
        console.log(res);
      }
    });
  }

})

function showMap(app, _this) {

  // if (app.companyInfo.longitude == null || "" == app.companyInfo.longitude || "null" == app.companyInfo.longitude && null == app.companyInfo.latitude || "" == app.companyInfo.latitude || "null" == app.companyInfo.latitude) {
  //   app.tip("无法获取位置信息！", "none");
  //   return;
  // }
  // if (_this.data.isLocation == false) {
  //   app.tip('未授权获取位置信息', 'none');
  // }
  // let _this = this;
  console.info("坐标")
  _this.setData({
    tab_wd: "",
    tab_dt: "map_2_h",
    showView: false,
    mapc: "show",
    tabbtn_h: false,
    isLocation: "",
    lng: 126.76279,
    lat: 45.80625,
    controls: [{
      id: 1,
      iconPath: 'https://7075-public-bb1cff-1257643776.tcb.qcloud.la/jian.png?sign=57721f87a1e11c34a1efb44caf2c8608&t=1570585942',
      position: {
        left: 260,
        top: 20,
        width: 30,
        height: 30
      },
      clickable: true
    },
    {
      id: 2,
      iconPath: 'https://7075-public-bb1cff-1257643776.tcb.qcloud.la/jia.png?sign=c556c26f83553d3561df985d7d406366&t=1570585893',
      position: {
        left: 310,
        top: 20,
        width: 30,
        height: 30
      },
      clickable: true
    }
    ],
    markers: [{
      id: "1",
      label: {
        content: "aa",
        textAlign: "center",
        color: "#9932CC"
      },
      latitude: 45.80625,
      longitude: 126.76279,
      width: 50,
      height: 50,
      iconPath: "",
      title: "aa"
    }]
  });
}