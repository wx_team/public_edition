var config = require('../../config.js');
const app = getApp()

Page({
  data: {
    placeOfLoadingMask: '', 
    pageIndex:1
  },
  signUpSubmit: function (e) {
    // 表单中获取订单编号
    var orderNoByForm = e.detail.value.orderNo;
    var that = this;
    var tenantName = that.data.companyName;
    var tenantCode = that.data.tenantCode;
    var orderNo = orderNoByForm;
    if (tenantName == undefined || tenantName == null || tenantName==""){
      app.tip("请选择物流公司","none");
      return false;
    }
    if (orderNo == undefined || orderNo == null || orderNo == "") {
      app.tip("请输入单号", "none");
      return false;
    }
    wx.navigateTo({ url: "../freightTrack/freightTrackDetail?dh=" + orderNo + "&tenantCode=" + tenantCode })
  },
  input_orderNo: function (e) {
    var that = this;
     var orderNo = e.detail.value
     that.setData({
       orderNo: orderNo
     })
  },

  onLoad: function (options) {
    var that =this;
    that.queryCompanyInfo(this);
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  topLoad: function () {
    console.info("onPullDownRefresh拉");
    var arrayAll = new Array();
    
    if (pageIndex>1){
      var pageIndex = this.data.pageIndex - 1;
      var num = (pageIndex - 1) * 20;
      for (var i = num; i < num + 20; i++) {
        arrayAll.push(this.data.allCompanyInfoList[i]);
      }
    } else {
      for (var i = 0; i < 20; i++) {
        arrayAll.push(this.data.allCompanyInfoList[i]);
      }
    }

    this.setData({
      pageIndex: pageIndex,
      selCompanyInfoList: arrayAll
    })

    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  bindDownLoad: function () {
    console.info("onReachBottom拉");
    var pageIndex = this.data.pageIndex + 1;
    var num = (pageIndex - 1)*20;
    var arrayAll = new Array();
    for (var i = num; i < num+20; i++) {
      arrayAll.push(this.data.allCompanyInfoList[i]);
    }

    this.setData({
      pageIndex: pageIndex,
      selCompanyInfoList: arrayAll
    })

  },

  /**
   * 显示遮罩层
   */
  showMask: function (e) {
    var that = this;
    that.setData({ placeOfLoadingMask: 'block'})
  },
  /**
   * 隐藏遮罩层
   */
  hideMask: function (e) {
    var that = this;
    that.setData({ placeOfLoadingMask: '' })
  },
  changeProvince: function (e) {
    //e.detail.value中的值是数组的下标
    this.setData({
      provinceIndex: e.detail.value,
      province: this.data.provinceList[e.detail.value].administrativeDivisionName
    })
    getCityList(this.data.provinceList[e.detail.value].administrativeDivisionId, this);
  },
  changeCity: function (e) {
    //e.detail.value中的值是数组的下标
    this.setData({
      cityIndex: e.detail.value,
      city: this.data.cityList[e.detail.value].administrativeDivisionName
    })
    getCityList(this.data.provinceIndex, this);
  },

  queryCompanyInfo: function (th) {
    console.info("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    var that = this;
    wx.request({
      url: config.requestUrl + "company/selectCompanyList",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (msg) {
        console.info("获取全部公司信息");
        console.info(msg);
        wx.hideLoading()
        if (msg.data.msg != null && msg.data.msg == "成功") {
          that.setData({
            allCompanyInfoList: msg.data.content
          })
          getProvinceList(th);
        } else {
          app.tip(msg.data.msg, "none");
          return;
        }
      },
      fail: function (msg) {
        wx.showModal({
          title: '提示',
          content: msg.data.msg,
          showCancel: false
        })
      }
    })
  },
  companyChange: function (e) {
    //文本框的值
    var name = app.trim(e.detail.value);
    var that = this;
    that.setData({
      companyInputValue: name,
    })
    that.doSearch();
  },
  doChoose(e) {
    var that = this;
    var name = e.currentTarget.dataset.name;
    var code = e.currentTarget.dataset.index;
    that.setData({
      companyInputValue: name,
      companyName:name,
      tenantCode: code,
      placeOfLoadingMask: ''
    })
  },
  doSearch: function (e) {
    var that = this;
    //文本框的值
    var province = that.data.province;
    var city = that.data.city;
    console.info("加载完省市之后的");
    console.info(that.data.companyInputValue);
    var name = (that.data.companyInputValue == undefined || that.data.companyInputValue == null) ? "" : that.data.companyInputValue;
    var arrayObj = new Array();
    var that = this;
    var arr = that.data.allCompanyInfoList;
    console.info(arr);
    if (province.length > 0) {
      if (province == "全部") {
        if (name.length > 0) {
          for (var i = 0; i < arr.length; i++) {
            console.info(arr[i].name);
            if (arr[i].name.indexOf(name) >= 0) {
              arrayObj.push(arr[i]);
            }
          }
        } else {
          console.info("赋值开始");
          //arrayObj = that.data.allCompanyInfoList;
          var arrayAll = new Array();
          for (var i = 0; i < 20;i++){
            arrayAll.push(that.data.allCompanyInfoList[i]); 
          }
          arrayObj = arrayAll;
          console.info("赋值结束");
        }
      } else {
        if (name.length > 0) {
          for (var i = 0; i < arr.length; i++) {
            console.info(arr[i].name);
            if (arr[i].name.indexOf(name) >= 0 && province == arr[i].province && city == arr[i].city) {
              arrayObj.push(arr[i]);
            }
          }
        } else {
          for (var i = 0; i < arr.length; i++) {
            if (province == arr[i].province && city == arr[i].city) {
              arrayObj.push(arr[i]);
            }
          }
        }
      }
    }

    that.setData({
      selCompanyInfoList: arrayObj
    })

  }, 

})


//省选择器的数据加载
function getProvinceList(that) {
  wx.showLoading({
    title: '加载中',
    mask: true
  })
  wx.request({
    url: config.requestUrl + "administrativeDivision/getProvinceAllList",
    method: "POST",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.hideLoading()

      var data = res.data;
      if (data && data.code && data.code == 100) {
        var provinceLists = data.content;
        console.log(provinceLists)
        if (app.currentPosition != null && app.currentPosition != "" && that.data.province == undefined) {
          that.setData({ provinceList: provinceLists, province: app.currentPosition.address_component.province })
          var administrativeDivisionId = ""
          for (var i = 0; i < provinceLists.length; i++) {
            if (app.currentPosition.address_component.province == provinceLists[i].administrativeDivisionName) {
              administrativeDivisionId = provinceLists[i].administrativeDivisionId
            }
          }
          getCityList(administrativeDivisionId, that);
        } else {
          that.setData({ provinceList: provinceLists, province: provinceLists[0].administrativeDivisionName })
          getCityList(provinceLists[0].administrativeDivisionId, that);
        }
      } else {
        var errMsg;
        if (data && data.msg) {
          errMsg = data.msg
        } else {
          errMsg = res.statusCode + ":" + res.errMsg
        }
        app.tip(errMsg, 'none')
      }
    },
    fail: function ({ errMsg }) {
      wx.hideLoading()
      app.tip(errMsg, 'none')
    }
  })
}

//市选择器的数据加载
function getCityList(provinceId, that) {

  wx.showLoading({
    title: '加载中',
    mask: true
  })
  wx.request({
    url: config.requestUrl + "administrativeDivision/getCityAllList",
    method: "POST",
    data: {
      "provinceId": provinceId
    },
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.hideLoading()

      var data = res.data;
      if (data && data.code && data.code == 100) {
        var cityLists = data.content;
        console.log(that.data.city)
        console.log(app.loginUser)
        //if (app.loginUser.city != null && app.loginUser.city != "" && that.data.city == undefined) {
        if (app.currentPosition != null && app.currentPosition != "" && that.data.city == undefined) {
          console.info(app.currentPosition);
          console.info(app.currentPosition.address_component.city);
          var areaCode = app.currentPosition.ad_info.city_code.substr(3, app.currentPosition.ad_info.city_code.length);
          console.info(areaCode);
          that.setData({ cityList: cityLists, city: app.currentPosition.address_component.city, areaCode: areaCode })
        } else {
          console.info("初始化市的值");
          console.info(cityLists[0].administrativeDivisionCode);
          that.setData({ cityList: cityLists, city: cityLists[0].administrativeDivisionName, areaCode: cityLists[0].administrativeDivisionCode })
        }

      } else {
        var errMsg;
        if (data && data.msg) {
          errMsg = data.msg
        } else {
          errMsg = res.statusCode + ":" + res.errMsg
        }
        app.tip(errMsg, 'none')
      }
    },
    fail: function ({ errMsg }) {
      wx.hideLoading()
      app.tip(errMsg, 'none')
    },
    complete: function () {
      console.info("改变地区之后的公司");
      console.info(that.data.allCompanyInfoList);
      that.doSearch();
    }
  })
}