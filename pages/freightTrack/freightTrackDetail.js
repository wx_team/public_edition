const app = getApp();
const config = require('../../config.js');
var coors
var coors1
var coors2
var coors3
var a
Page({

  /**
   * 页面的初始数据
   */
  data: {
    companyName: "",
    trackingno:"",
    polyline: [],
    ohmOrder:[],
    markers: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    console.info(2132131231231)
    console.info(options)
    wx.showLoading({ mask: true, title: "加载中" });

    //设置物流公司名称
    //this.setData({ companyName: app.companyInfo.name })

    let _this = this;
    // console.log(options.dh);
    _this.setData({ trackingno: options.trackingno});
    getList(options.trackingno,function (msg) {
      console.info("货物跟踪返回信息");
      console.info(msg);
      //货物跟踪地图显示
      _this.map(msg);
      var company = msg.data.content.object.company;
      var result = msg.data.content.object.list;
      var ohmOrder=msg.data.content.object.ohmOrder
      // console.log("公司：" + company)
      // if (company != null && company!=""){
        _this.setData({
          companyName: result[0].companyName , 
          ohmOrder:ohmOrder,
          wuliu: result,
        });
      // }
     
      if (Object.prototype.toString.call(result) === "[object Array]" && result.length > 0) {
         console.info(result);
        let arr = new Array();
        for (let i = 0; i < result.length; i++) {
          let str = result[i].trackStatusName;
          if (str == "" || str == " ") {
            str = "已开始处理";
          }
          let temp = { trackStatusName: str, statusChangeDateTime: result[i].statusChangeDateTime, shippingNote: result[i].shippingNote, createTime: result[i].createTime };
          // if (6 == result[i].standardStateCode || 7 == result[i].standardStateCode) {
          //   temp.status = 'qiyi';
          // } else if (13 == result[i].standardStateCode) {
          //   temp.status = 'qisan';
          // } else if (38 == result[i].standardStateCode){
          //   temp.status = 'qisi';
          // }else {
          //   temp.status = 'qier';
          // }
          arr.push(temp);
        }
        // console.log(arr);
        _this.setData({
          wuliu: arr,
          showView: true
        });
      } else {
        _this.setData({
          showView: false,
          message: "抱歉，没有找到相关数据！",
          imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
        });
      }
      wx.hideLoading();
    });
  },

  map: function (msg) {
    var that2 = this
    console.info("货物跟踪地图信息");
    console.info(msg);
    var res = msg.data.content;
    var senderLatitude = res.senderLatitude;
    var senderLongitude = res.senderLongitude;
    var placeOfLoadingLatitude = res.placeOfLoadingLatitude;
    var placeOfLoadingLongitude = res.placeOfLoadingLongitude;
    var sendCompanyLatitude = res.sendCompanyLatitude;
    var sendCompanyLongitude = res.sendCompanyLongitude;
    var receiveCompanyLatitude = res.receiveCompanyLatitude;
    var receiveCompanyLongitude = res.receiveCompanyLongitude;
    that2.setData({
      markers: [{
        iconPath: res.content1 != "" ?"https://7075-public-bb1cff-1257643776.tcb.qcloud.la/blueCar.png?sign=36c3b3470d9512e254b110c8caeb5357&t=1570583946":"",
        latitude: res.senderLatitude,
        longitude: res.senderLongitude,
        width: 15,
        height: 25,
        label: { color: "red", fontSize: 11, content: res.content1, x: 0, y: 0 },
        anchor: { x: 1, y: 1 },
        callout: {
          content: res.senderAddress,
          color: "red",
          fontSize: 11,
          display: "ALWAYS"
        }
      }, {
          iconPath: res.content2 != "" ? "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/blueCar.png?sign=36c3b3470d9512e254b110c8caeb5357&t=1570583946" : "",
        latitude: res.placeOfLoadingLatitude,
        longitude: res.placeOfLoadingLongitude,
        width: 15,
        height: 25,
        label: { color: "red", fontSize: 11, content: res.content2, x: 0, y: 0 },
        anchor: { x: 1, y: 1 },
        callout: {
          content: res.placeOfLoading,
          color: "red",
          fontSize: 11,
          display: "ALWAYS"
        }
        }, {
          iconPath: res.content3 != "" ? "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/blueCar.png?sign=36c3b3470d9512e254b110c8caeb5357&t=1570583946" : "",
          latitude: res.sendCompanyLatitude,
          longitude: res.sendCompanyLongitude,
          width: 15,
          height: 25,
          label: { color: "red", fontSize: 11, content: res.content3, x: 0, y: 0 },
          anchor: { x: 1, y: 1 },
          callout: {
            content: res.sendCompany,
            color: "red",
            fontSize: 11,
            display: "ALWAYS"
          }
      }, {
          iconPath: res.content4 != "" ? "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/blueCar.png?sign=36c3b3470d9512e254b110c8caeb5357&t=1570583946" : "",
        latitude: res.receiveCompanyLatitude,
        longitude: res.receiveCompanyLongitude,
        width: 15,
        height: 25,
        label: { color: "red", fontSize: 11, content: res.content4, x: 0, y: 0 },
        anchor: { x: 1, y: 1 },
        callout: {
          content: res.receiveCompany,
          color: "red",
          fontSize: 11,
          display: "ALWAYS"
        }
      }],
    })

    // wx.request({
    //   url: 'https://apis.map.qq.com/ws/direction/v1/driving/?from=' + senderLatitude + ',' + senderLongitude + '&to=' + placeOfLoadingLatitude + ',' + placeOfLoadingLongitude +'&output=json&callback=cb&key=RPTBZ-NX6LJ-ETVF5-KRBHK-GAFM6-SIFKA',

    //   success: function (res) {
    //     console.info("sssssssssssssssssssss");
    //     console.info(res);
    //     coors = res.data.result.routes[0].polyline
    //     for (var i = 2; i < coors.length; i++)
    //     { coors[i] = coors[i - 2] + coors[i] / 1000000 }
    //     //console.log(coors)

    //     var b = []
    //     for (var i = 0; i < coors.length; i = i + 2) {
    //       b[i / 2] = {
    //         latitude: coors[i], longitude: coors[i + 1]
    //       }
    //       //console.log(b[i / 2])
    //     }
    //     //console.log(b.length)

    //     wx.request({
    //       url: 'https://apis.map.qq.com/ws/direction/v1/driving/?from=' + placeOfLoadingLatitude + ',' + placeOfLoadingLongitude + '&to=' + sendCompanyLatitude + ',' + sendCompanyLongitude + '&output=json&callback=cb&key=RPTBZ-NX6LJ-ETVF5-KRBHK-GAFM6-SIFKA',

    //       success: function (res) {
    //         console.info("asdasdasdasdasd");
    //         console.info(res);
    //         coors1 = res.data.result.routes[0].polyline
    //         for (var i = 2; i < coors1.length; i++)
    //         { coors1[i] = coors1[i - 2] + coors1[i] / 1000000 }
    //         //console.log(coors1)

    //         var c = []
    //         for (var i = 0; i < coors1.length; i = i + 2) {
    //           c[i / 2] = {
    //             latitude: coors1[i], longitude: coors1[i + 1]
    //           }
    //           //console.log(c[i / 2])
    //         }
    //         //console.log(c.length)

    //         a = b.concat(c);

    //         wx.request({
    //           url: 'https://apis.map.qq.com/ws/direction/v1/driving/?from=' + sendCompanyLatitude + ',' + sendCompanyLongitude + '&to=' + receiveCompanyLatitude + ',' + receiveCompanyLongitude +'&output=json&callback=cb&key=RPTBZ-NX6LJ-ETVF5-KRBHK-GAFM6-SIFKA',

    //           success: function (res) {
    //             coors2 = res.data.result.routes[0].polyline
    //             for (var i = 2; i < coors2.length; i++)
    //             { coors2[i] = coors2[i - 2] + coors2[i] / 1000000 }
    //             //console.log(coors2)

    //             var d = []
    //             for (var i = 0; i < coors2.length; i = i + 2) {
    //               d[i / 2] = {
    //                 latitude: coors2[i], longitude: coors2[i + 1]
    //               }
    //               //console.log(d[i / 2])
    //             }
    //             //console.log(d.length)

    //             a = a.concat(d);
    //             that2.setData({
    //               polyline: [{
    //                 points: a,
    //                 color: "#FF0000",
    //                 width: 4,
    //                 dottedLine: false
    //               }],
    //             })
    //           }
    //         })

    //       }
    //     })
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
});

/**
 * 查询货物跟踪
 */

function getList(ph,callback) {
  wx.showLoading({ mask: true, title: "加载中" });
  wx.request({
    url: config.requestUrl + "trace/getGoodsTracesFromTrack",
    method: "POST",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: {
      trackingNo: ph
    },
    success: function (msg) {
      // console.info("货物跟踪详细返回结果");
      // console.info(msg);
      wx.hideLoading();
      callback(msg);
    },
    fail: function (msg) {
      // console.log(msg);
      wx.hideLoading();
    }
  })
}

/**
 * 根据状态码获取值
 */
function getStatusByCode(str) {
  let res = "";
  switch (str) {
    case 6:
      res = "已完成预约";
      break;
    case 7:
      res = "已取消预约";
      break;
    case 13:
      res = "已托收/提货";
      break;
    case 15:
      res = "集货";
      break;
    case 24:
      res = "已发车";
      break;
    case 31:
      res = "在途";
      break;
    case 72:
      res = "已确认收货";
      break;
    case 117:
      res = "下落不明";
      break;
    case 218:
      res = "损坏";
      break;
    case 279:
      res = "拒绝交付，自动退回";
      break;
    case 313:
      res = "被收货人退回";
      break;
    case 364:
      res = "到达现场";
      break;
    case 365:
      res = "离开货场";
      break;
    default:
  }
  return res;
}