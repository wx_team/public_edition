const app = getApp();
const config = require('../../config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag : 0,
    pageSize : 10,
    winHeight: "",
    //窗口高度    
    currentTab: 0,
    //预设当前项的值  
    scrollLeft: 0,
    //tab标题的滚动条位置  
    showViewTotal: false,
    showView: true,
    userInfo: null,
    loginUser: null,
    isFromSearch: true,
    // 用于判断searchSongList数组是不是空数组，默认true，空的数组    searchPageNum: 1, // 设置加载的第几次，默认是第一次      
    callbackcount: 10,
    //返回数据的个数    
    searchLoading: false,
    //"上拉加载"的变量，默认false，隐藏    
    searchLoadingComplete: false
    //“没有数据”的变量，默认false，隐藏   
  },
  swichNav: function (e) {
    var cur = e.target.dataset.current; console.info(cur); console.info(this.data.currentTab);
    if (this.data.currentTab == cur) {
      return false;
    } else {
      this.setData({
        currentTab: cur,
        showView: false
      })
    }
  },
  // 点击标题切换当前页时改变样式  
  swichNavLeft: function (e) {
    var cur = e.target.dataset.current;
    if (this.data.currentTab == cur) { return false; } else {
      this.setData({
        currentTab: cur, showView: true
      })
    }
  },
  /**
   * 我收到的
   */
  goWSDD: function (e) {
    this.setData({ tab1_class: "", tab2_class: "active" });
    this.data.flag = 1;
    this.data.pageSize = 10;
    getList(this, 1, app);
  },
  /**
   * 我寄出的
   */
  goWJCD: function (e) {
    this.setData({ tab1_class: "active", tab2_class: "" });
    this.data.flag = 0;
    this.data.pageSize = 10;
    getList(this, 0,  app);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
      this.data.flag = 0;
      this.data.pageSize = 10;
    this.setData({ tenantCode: options.tenantCode, tenantName: options.tenantName});
      getList(this, 0, app);
    this.setData({ tab1_class: "active", tab2_class: ""});
  },

  //扫码
  toScanQRcode() {

    wx.scanCode({
      success: (res) => {
        console.log("扫码结果");
        console.log(res.result);

        if (res.result.indexOf(",") == -1) {
          // wx.showToast({
          //   title: "抱歉，该订单状态为未处理",
          //   icon: 'none',
          //   duration: 3000
          // })
          wx.showModal({
            title: '提示',
            content: '抱歉，该运单状态为未受理',
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              }
            }
          })

          return false
        }

        var values = res.result.split(",");
        var tenantCode = values[0];
        var orderNumber = values[1];

        if (tenantCode == undefined || tenantCode == null || tenantCode == "" || tenantCode == "null") {
          //app.tip("金融代码不存在", "none");
          // wx.showToast({
          //   title: "金融代码不存在",
          //   icon: 'none',
          //   duration: 3000
          // })

          wx.showModal({
            title: '提示',
            content: '金融代码不存在',
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              }
            }
          })
          return false
        } else {
          if (tenantCode == config.tenantCode) {
            wx.navigateTo({ url: "freightTrackDetail?dh=" + orderNumber })
          } else {
            //app.tip("抱歉！无法找到该订单，请检查订单号。", "none");
            // wx.showToast({
            //   title: "抱歉, 当前小程序为【" +app.appName+"】，请核实运单！",
            //   icon: 'none',
            //   duration: 3000
            // })
            wx.showModal({
              title: '提示',
              content: "抱歉, 当前小程序为【" + app.appName + "】，请核实运单！",
              showCancel: false,
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                }
              }
            })
            return false
          }
        }
      },
      fail: (res) => {
        console.log(res);
        // wx.showToast({
        //   title: res,
        //   icon: 'none',
        //   duration: 3000
        // })
        wx.showModal({
          title: '提示',
          content: res,
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        })
        return false
      }
    })
  },
  orderNumberInput: function (e) {
    this.setData({
      inputOrderNumberValue: e.detail.value
    })
  },
  freightTrackDetail: function (e) {
    var that =this;
    var trackingno = e.currentTarget.dataset.trackingno;
    wx.navigateTo({
      url: '/pages/freightTrack/freightTrackDetail?trackingno='+trackingno
    })

  },
  //放大镜跳转货物跟踪页
  toFreightTrack: function (e) {
    var that =this;
    var orderNumber = that.data.inputOrderNumberValue;
   
    if (orderNumber == undefined || orderNumber == "") {
      app.tip("运单号不可以为空，请输入运单号", 'none')
    } else {
      console.info(that.data.allList);
      var all = that.data.allList;
      let arr = new Array();
      orderNumber=orderNumber.toUpperCase();
      for (var i = 0; i < all.length; i++) {
        console.log("toFreightTrack:"+all[i].body.shippingNoteNumber);
        if (orderNumber == all[i].body.shippingNoteNumber){
          let temp = all[i];
          arr.push(temp);
        }
      }
      that.setData({
        wuliu: arr
      })
    }
    
    //wx.navigateTo({ url: "../freightTrack/searchFreightTrack" });
    // var orderNumber = this.data.inputOrderNumberValue;
    // if (orderNumber == undefined || orderNumber == "") {
    //   app.tip("运单号不可以为空，请输入运单号", 'none')
    // } else {
    //   wx.navigateTo({ url: "../freightTrack/freightTrackDetail?dh=" + orderNumber })
    // }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  /**
   * 
  复制单号
   */
  copyText: function (e) {
    var txtValue =e.currentTarget.dataset.ordernumber;
    wx.setClipboardData({
      data: txtValue,
      success (res) {
        wx.getClipboardData({
          success (res) {
            console.log(res.data) // data
            wx.showToast({
              title: '复制成功'
            })
          }
        })
      }
    })

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    getList(this, this.data.flag,  app);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.pageSize = this.data.pageSize + 10;
    getList(this, this.data.flag,  app);
  }
  // ,

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // }
});


/**
 * 查询查件
 */
function getList(_this, flag, app) {
// console.log(_this.data.pageSize);
  // console.log(_this.data.flag);
  if (app == undefined || app.loginUser == undefined || app.loginUser.mobile == undefined){
    _this.setData({
      showView: false,
      message: "抱歉，没有找到相关数据！",
      imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
    });
    return;
  }
  searchList(_this, flag, app.loginUser.mobile,  function (msg) {
    let arr = new Array();
    if (Object.prototype.toString.call(msg.data.content.object.list) === "[object Array]" && msg.data.content.object.list.length > 0) {
      arr=msg.data.content.object.list;
      _this.setData({ wuliu: arr,allList:arr ,showView: true});
    }else{
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
    }
    
  });
}

/**
 * 获取查件
 * flag  0：该手机号只作为发货人手机；1：该手机号作为收货人手机；2：该手机号作为发货人或者收货人手机
 */
function searchList(_this,flag, mobile,  callback) {
  wx.showLoading({ mask: true, title: "加载中" });
  let par = { 
    "data": { 
      "code": _this.data.tenantCode, 
      "userId": app.loginUser.id,
      "mobile":mobile,
      "flag":flag,
      "currentPage":"1",
      "pageSize": _this.data.pageSize,
      "startDate": "",
      "endDate": ""
    }
  };
  // console.log(par);
  wx.request({
    url: config.requestUrl + "order/searchGoodsTraceListNew",
    method: "GET",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: par,
    success: function (msg) {
      console.info("查件查询结果");
      console.info(msg);
      callback(msg);
      wx.hideLoading();
    },
    fail: function (msg) {
      // console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}