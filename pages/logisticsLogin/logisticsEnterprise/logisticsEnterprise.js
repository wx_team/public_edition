var config = require('../../../config.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tenantName: "",
    tenantLogoImgUrl: "",
    tenantAddress: "",
    tenantLine: "",
    tenantMobile: "",
    webList: [],
    tenantId: null,
    loginUser: null,
    flag: "",
    tab_wd: "map_1_h",
    tab_dt: "",
    mapc: "hide",
    lng: "",
    lat: "",
    name: "",
    addr: "",
    scale: 15,
    showModalStatus: false,
    tabbtn_h: true,
    isLocation: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.info("初始化企业信息")
    console.info(options)
    this.setData({
      formatTopCompanyName: options.formatTopCompanyName,
      companyCode: options.companyCode,
      companyName: options.companyName
    })

  },

  toWebDetail: function (e) {
    let webInfo = e.currentTarget.dataset.type;
    var that = this.data;
    wx.navigateTo({
      url: "webInfo?webInfo=" + JSON.stringify(webInfo)
    })
  },

  /**
   * 拨打电话
   */
  toTelPhone: function (e) {
    var telNo = e.currentTarget.dataset.current;
    wx.makePhoneCall({
      phoneNumber: telNo
    })
  },

  /**
   * 跳转到投保页面
   */
  toInsure: function () {
    wx.navigateTo({
      url: '../insure/insure',
    })
  },
  /**
   * 跳转到投保记录查询页面
   */
  toInsureSearch: function () {
    wx.navigateTo({
      url: '../insureSearch/insureSearch',
    })
  },
  /**
 * 跳转到雇主险页面
 */
  toEmployerInsurance: function () {
    wx.navigateTo({
      url: '../employerInsurance/employerInsurance',
    })
  },
  /**
 * 跳转到在保人员管理页面
 */
  toInsurancerManagement: function () {
    wx.navigateTo({
      url: '../insurancerManagement/insurancerManagement',
    })
  },
  /**
   * 跳转到货物跟踪页面
   */
  toFreightTrack: function () {
    wx.navigateTo({
      url: '../freightTrack/searchFreightTrack',
    })
  },

  /**
   * 关注物流公司
   */
  followTenant: function () {
    var _this = this;
    var userId = app.loginUser.id;
    if ("" != userId && "" != this.data.tenantId) {
      wx.request({
        url: config.requestUrl + "company/followCompany",
        method: 'POST',
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          "companyId": this.data.tenantId,
          "userId": userId
        },
        success: function (result) {
          app.tip(result.data.msg, 'success');
          _this.setData({
            flag: '1'
          });
        },
        fail: function ({
          errMsg
        }) {
          app.tip(errMsg, 'none');
        }
      })
    }
  },
  /**
   * 取消关注物流公司
   */
  cancelFollowTenant: function () {
    var _this = this;
    var userId = app.loginUser.id;
    if ("" != userId && "" != this.data.tenantId) {
      wx.request({
        url: config.requestUrl + "/company/cancelFollowCompany",
        method: 'POST',
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          "companyId": this.data.tenantId,
          "userId": userId
        },
        success: function (result) {
          app.tip(result.data.msg, 'success');
          showView: false;
          _this.setData({
            flag: '0'
          });
        },
        fail: function ({
          errMsg
        }) {
          app.tip(errMsg, 'none');

        }
      })
    }
  },
  /**
   * 初始化显示页面数据加载;
   */
  onShow: function () {

    var that = this;

    wx.getSetting({
      success: (res) => {
        console.log(res.authSetting['scope.userLocation']);
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] == true) {
          that.setData({
            isLocation: true
          });
        } else {
          that.setData({
            isLocation: false
          });
        }

        if (that.data.tabbtn_h) {
          //old:请求后台 getWebList 接口
          //now:发货网点已经设置到 app.companyInfo.businessSetting 中了，从这里获取即可

          if (app.companyInfo == null || app.companyInfo == "" || app.companyInfo == undefined || app.companyInfo.businessSetting == null || app.companyInfo.businessSetting == "" || app.companyInfo.businessSetting == undefined) {
            
            return false;
          }
          that.setData({
            //获取所有网点
            webList: app.companyInfo.businessSetting.webList,
            showView: true
          });
        }
      }
    });

  },
  /**
   * 点击企业网点
   */
  wangdian: function () {
    this.setData({
      tab_wd: "map_1_h",
      tab_dt: "",
      showView: true,
      mapc: "hide",
      tabbtn_h: true
    });
  },
  /**
   * 点击地图显示
   */
  mapshow: function () {
    // app.companyInfo.longitude = 126.76279;
    // app.companyInfo.latitude = 45.80625;
    showMap(app, this);
  },
  controltap: function (e) {
    var that = this;
    // console.log("scale===" + this.data.scale)
    if (e.controlId === 1) {
      // if (this.data.scale === 13) {
      that.setData({
        scale: --this.data.scale
      })
      // }
    } else {
      //  if (this.data.scale !== 13) {
      that.setData({
        scale: ++this.data.scale
      })
      // }
    }


  },
  /**
   *位置权限
   */
  setLocationSetting: function (e) {
    // console.log(e);
    if (e.detail != undefined && e.detail.authSetting != undefined && e.detail.authSetting["scope.userLocation"] == true) {
      this.data.isLocation = true;
    } else {
      this.data.isLocation = false;
    }
    showMap(app, this);
  },
  /**
   * 导航位置权限
   */
  setLocationSetting_nvato: function (e) {
    // console.log(e);
    if (e.detail != undefined && e.detail.authSetting != undefined && e.detail.authSetting["scope.userLocation"] == true) {
      this.data.isLocation = true;
    } else {
      this.data.isLocation = false;
      app.tip("未授权获取位置信息", "none");
    }
    let templng = e.currentTarget.dataset.lng;
    let templat = e.currentTarget.dataset.lat;
    let tempname = e.currentTarget.dataset.name;
    let tempaddr = e.currentTarget.dataset.addr;
    // templng = 126.76279;
    // templat = 45.80625;
    // console.log(templng + "----" + templat);
    if (null == templat || "" == templat || "null" == templat || null == templng || "" == templng || "null" == templng) {
      app.tip("无法获取位置信息！", "none");
      return;
    }

    if (null == tempname || "" == tempname || "null" == tempname) {
      tempname = "";
    }
    if (null == tempaddr || "" == tempaddr || "null" == tempaddr) {
      tempaddr = "";
    }

    this.data.lng = templng;
    this.data.lat = templat;
    this.data.name = tempname;
    this.data.addr = tempaddr;

    wx.openLocation({
      latitude: templat,
      longitude: templng,
      name: tempname,
      address: tempaddr,
      scale: 15
    })
  },
  /**
   * 点击导航
   */
  navTo: function (e) {
    // console.log(e.currentTarget.dataset.lat + "----" + e.currentTarget.dataset.lng);
    // console.log(e.currentTarget.dataset.name + "----" + e.currentTarget.dataset.addr);
    if (this.data.isLocation == false) {
      app.tip("未授权获取位置信息", "none");
    }
    let templng = e.currentTarget.dataset.lng;
    let templat = e.currentTarget.dataset.lat;
    let tempname = e.currentTarget.dataset.name;
    let tempaddr = e.currentTarget.dataset.addr;
    // templng = 126.76279;
    // templat = 45.80625;
    // console.log(templng + "----" + templat);
    if (null == templat || "" == templat || "null" == templat || null == templng || "" == templng || "null" == templng) {
      app.tip("无法获取位置信息！", "none");
      return;
    }

    if (null == tempname || "" == tempname || "null" == tempname) {
      tempname = "";
    }
    if (null == tempaddr || "" == tempaddr || "null" == tempaddr) {
      tempaddr = "";
    }

    this.data.lng = templng;
    this.data.lat = templat;
    this.data.name = tempname;
    this.data.addr = tempaddr;

    wx.openLocation({
      latitude: templat,
      longitude: templng,
      name: tempname,
      address: tempaddr,
      scale: 15
    })

    // 126.76279----45.80625
    // 126.56279-- - 45.80825
  }
});

function showMap(app, _this) {
  // app.companyInfo.longitude = 126.76279;
  // app.companyInfo.latitude = 45.80625;
  if (app.companyInfo.longitude == null || "" == app.companyInfo.longitude || "null" == app.companyInfo.longitude && null == app.companyInfo.latitude || "" == app.companyInfo.latitude || "null" == app.companyInfo.latitude) {
    app.tip("无法获取位置信息！", "none");
    return;
  }
  if (_this.data.isLocation == false) {
    app.tip('未授权获取位置信息', 'none');
  }
  // let _this = this;
  console.info("坐标")
  console.info(app.companyInfo.longitude + "---" + app.companyInfo.latitude);
  _this.setData({
    tab_wd: "",
    tab_dt: "map_2_h",
    showView: false,
    mapc: "show",
    tabbtn_h: false,
    isLocation: _this.data.isLocation,
    lng: app.companyInfo.longitude,
    lat: app.companyInfo.latitude,
    controls: [{
      id: 1,
      iconPath: 'https://7075-public-bb1cff-1257643776.tcb.qcloud.la/jian.png?sign=57721f87a1e11c34a1efb44caf2c8608&t=1570585942',
      position: {
        left: 260,
        top: 20,
        width: 30,
        height: 30
      },
      clickable: true
    },
    {
      id: 2,
      iconPath: 'https://7075-public-bb1cff-1257643776.tcb.qcloud.la/jia.png?sign=c556c26f83553d3561df985d7d406366&t=1570585893',
      position: {
        left: 310,
        top: 20,
        width: 30,
        height: 30
      },
      clickable: true
    }
    ],
    markers: [{
      id: "1",
      label: {
        content: app.appName,
        textAlign: "center",
        color: "#9932CC"
      },
      latitude: app.companyInfo.latitude,
      longitude: app.companyInfo.longitude,
      width: 50,
      height: 50,
      iconPath: "",
      title: app.appName
    }]
  });
}