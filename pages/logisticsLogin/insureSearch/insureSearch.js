const app = getApp();
const config = require('../../../config.js');
Page({
  data: {
    pageSize: 20
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    this.data.cardId = options.cardId;
    this.data.kpstart = getDate(-7);
    this.data.kpend = getDate();
    this.data.ffstart = getDate(-7);
    this.data.ffend = getDate();
    this.setData({ kpstart: this.data.kpstart, kpend: this.data.kpend, ffstart: this.data.ffstart, ffend: this.data.ffend, cardId: options.cardId, phone: options.phone });
    this.getAccountBalance();
    this.getBalance();
    getList(this);
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.data.pageSize = 20;
    getList(this);
    wx.stopPullDownRefresh();
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.pageSize = this.data.pageSize + 20;
    getList(this);
  },
  //  点击时间组件确定事件  
  bindTimeChange: function (e) {
    // console.log("谁哦按")
    this.setData({
      times: e.detail.value
    })
  },
  companyChange: function (e) {
    //文本框的值
    var name = app.trim(e.detail.value);
    var that = this;
    that.setData({
      companyInputValue: name,
    })
    that.doSearch();
  },
  getAccountBalance: function (e) {
    
    var that = this;
    let par = {
      userName: app.insureUserName,
      password: app.insurePassword,
      typeCode: "保险"
    };

    wx.request({
      url: config.requestUrl + "insurance/queryCompanyAccount",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "data": par },
      success: function (msg) {
        console.info("账户余额返回结果");
        console.info(msg);
        that.setData({
          accountBalance: msg.data.content
        })
      },
      fail: function (msg) {
        console.log(msg);
      }
    });
  },

  getBalance: function (e) {

    var that = this;
    let par = {
      userName: app.insureUserName,
      password: app.insurePassword,
      typeCode: "保险"
    };

    wx.request({
      url: config.requestUrl + "insurance/queryBalance",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "data": par },
      success: function (msg) {
        console.info("优惠券余额返回结果");
        console.info(msg);
        that.setData({
          balance: msg.data.content
        })
      },
      fail: function (msg) {
        console.log(msg);
      }
    });
  },

  //  点击日期组件确定事件  
  bindDateChange: function (e) {
    let temptype = e.target.dataset.t;
    let temidate = e.detail.value;
    if ("ks" == temptype) {
      this.data.kpstart = temidate;
    } else if ("ke" == temptype) {
      this.data.kpend = temidate;
    } else if ("fs" == temptype) {
      this.data.ffstart = temidate;
    } else if ("fe" == temptype) {
      this.data.ffend = temidate;
    }
    this.setData(this.data);
    getList(this)
  }
});

/**
 * 获取数据
 */
function getList(_this) {
  // console.log(_this.data);
  searchList(_this, function (msg) {
    //console.log(msg.data.content.list);
    if (Object.prototype.toString.call(msg.data.content.rows) === "[object Array]" && msg.data.content.rows.length > 0) {
      // for (let i = 0; i < msg.data.content.rows.length; i++) {
      //   msg.data.resultObject[i].kpDate = msg.data.resultObject[i].kpDate.replace('T', ' ');
      //   msg.data.resultObject[i].fkDate = msg.data.resultObject[i].fkDate.replace('T', ' ');
      // }


      var arr = msg.data.content.rows;

      _this.setData({
        wuliu: arr,
        showView: true
      });
      

    } else {
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
    }


  });
}

/**
 * 请求数据
 */

function searchList(_this, callback) {
  if (_this.data.kpstart > _this.data.kpend) {
    app.tip("开始时间不能晚于结束时间", "none");
    return false;
  }
  wx.showLoading({ mask: true, title: "加载中" });
  let par = {
    page: 1,
    pageSize: _this.data.pageSize,
    userName: app.insureUserName,
    password: app.insurePassword,
    startDate: _this.data.ffstart + " 00:00:00",
    endDate: _this.data.ffend + " 23:59:59",
    stateFlag:""
  };
  // console.log(par);
  wx.request({
    url: config.requestUrl + "insurance/queryList",
    method: "GET",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: { "data": par },
    success: function (msg) {
      console.info("投保查询返回结果");
      console.info(msg);
      callback(msg);
      wx.hideLoading();
    },
    fail: function (msg) {
      // console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}

/**
 * 获取当前日期
 */
var getDate = function (num) {
  let tempDate = new Date();
  if (num != undefined && "" != num) {
    tempDate.setDate(tempDate.getDate() + num);//获取AddDayCount天后的日期 
  }
  let nowDate = new Date(Date.parse(tempDate));
  //年  
  let Y = nowDate.getFullYear();
  //月  
  let M = (nowDate.getMonth() + 1 < 10 ? '0' + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1);
  //日  
  let D = nowDate.getDate() < 10 ? '0' + nowDate.getDate() : nowDate.getDate();
  return Y + "-" + M + "-" + D;
};
