//获取应用实例  
const app = getApp();
var config = require('../../../config.js');
var util = require('../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checkBoxs: [{ name: '', value: '0', check: '' }],
    checkValue:"0",
    insuranceCodeListAll: [],
    insuranceCodeList: [],
    insuranceCodeListIndex: 0,
    insurancePersonListAll: [],
    insurancePersonList: [],
    insurancePersonListIndex: 0,
    insuranceToPersonListAll: [],
    insuranceToPersonList: [],
    insuranceToPersonListIndex: 0,
    insuranceCodeId: "",
    insuranceName: "",
    insuranceCompanyName: "",
    insuranceCompanyId: "",
    saveActionFlag: "",
    toubaoxuzhiValue:"",
    photos:"",
    applicantUserName: "",
    insuredUserName: "",
    applicantUserGender: "",
    applicantUserBirthday: "",
    applicantIdType: "",
    applicantPhone: "",
    applicantCell: "",
    applicantZip: "",
    applicantAddress: "",
    applicantEmail: "",
    insuredUserGender: "",
    insuredUserBirthday: "",
    insuredIdType: "",
    insuredPhone: "",
    insuredCell: "",
    insuredZip: "",
    insuredAddress: "",
    insuredEmail: "",
    insuredIdNo: "",
    insuranceIdType: "",
    applicantIdNo: "",
    applicantIdType: "",
    yhqFlag: "",
    taiPingFlag: "",
    noticeTitlesList: [],
    noticeUrlsList: [], 
    orderPic: [], //图片列表
    orderPicName : [], //图片名称列表
    toubaoxuzhi:"<<投保须知>>"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var date = util.formatTime(new Date());
    this.setData({
      qiyunDate: date
    })
    console.info("投保页面初始化");
    this.getBalance();
    //this.searchInsuranceCodeList();
    //this.searchInsuranceUserList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  //点击按钮
  doSave: function () {
    var that=this;
    if (that.data.saveActionFlag==""){
      that.calculation();
    } else {
      that.saveInsure();
    }
  },

/**
 * 计算保费
 */
  calculation: function () {
    var that = this;

    if (that.data.insuranceCompanyId == null || that.data.insuranceCompanyId.length==0){
      app.tip("保险公司ID取得错误","none");
      return false;
    }
    if (that.data.insuranceCodeId == null || that.data.insuranceCodeId.length == 0) {
      app.tip("险种ID取得错误", "none");
      return false;
    }
    if (that.data.sumInsuredValue == null || that.data.sumInsuredValue.length == 0) {
      app.tip("请输入保险金额", "none");
      return false;
    } else {
      if (that.data.sumInsuredValue==0){
        app.tip("保险金额必须大于0", "none");
        return false;
      }
    }
    var placeOfOriginatingValue="";
    if (that.data.placeOfOriginatingValue != null && that.data.placeOfOriginatingValue.length > 0) {
      placeOfOriginatingValue = that.data.placeOfOriginatingValue;
    }
    var placeOfLoadingValue = "";
    if (that.data.placeOfLoadingValue != null && that.data.placeOfLoadingValue.length > 0) {
      placeOfLoadingValue = that.data.placeOfLoadingValue;
    }

    let par = {
      userName: app.insureUserName,
      password: app.insurePassword,
      insuranceCompanyId: that.data.insuranceCompanyId,
      insuranceCodeId: that.data.insuranceCodeId,
      sumInsured: that.data.sumInsuredValue,
      fromArea: placeOfOriginatingValue,
      toArea: placeOfLoadingValue
    };
    console.info("计算保费参数")
    console.info(par)
    wx.request({
      url: config.requestUrl + "insurance/calculate", 
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "data": par },
      success: function (msg) {
        console.info("计算保费返回结果");
        console.info(msg);
        if (msg.data.code=="100"){
          var houAmount = Number(that.data.balance) - Number(msg.data.content.couponAmount);
          that.setData({
            premium: msg.data.content.premium,
            premiumRate: msg.data.content.premiumRate,
            couponAmount: msg.data.content.couponAmount,
            balanceAmount: msg.data.content.balanceAmount,
            houAmount: houAmount,
            saveActionFlag: "1"
          })
        } else {
          app.tip("计算保费失败:" + msg.data.msg, "none");
        }

        console.info("计算保费返回结果结束");
      },
      fail: function (msg) {
        console.log(msg);
        app.tip("计算保费失败:" + msg,"none");
      }
    });
  },

  /**
 * 投保
 */
  saveInsure: function () {
    var that = this;
    if(that.data.noticeTitlesList.length > 0 && that.data.noticeTitlesList[0].length > 0){
      if(that.data.noticeTitles!=null&&that.data.noticeTitles!=""){
        if (that.data.checkValue == null || that.data.checkValue.length == 0 || that.data.checkValue == "0") {
          app.tip("请阅读保险须知", "none");
          return false;
        }
      }
    }

    if (that.data.inputTransportNumberValue == null || that.data.inputTransportNumberValue.length == 0) {
      app.tip("请输入配载单号", "none");
      return false;
    }
    if (that.data.inputGoodsNameValue == null || that.data.inputGoodsNameValue.length == 0) {
      app.tip("请输入货物名称", "none");
      return false;
    }
    if (that.data.inputTotalNumberValue == null || that.data.inputTotalNumberValue.length == 0) {
      app.tip("请输入总件数", "none");
      return false;
    }
    if (that.data.inpitTotalVotesValue == null || that.data.inpitTotalVotesValue.length == 0) {
      app.tip("请输入总票数", "none");
      return false;
    }
    if (that.data.placeOfOriginatingValue == null || that.data.placeOfOriginatingValue.length == 0) {
      app.tip("请输入起运地", "none");
      return false;
    }
    if (that.data.placeOfLoadingValue == null || that.data.placeOfLoadingValue.length == 0) {
      app.tip("请输入目的地", "none");
      return false;
    }
    if (that.data.plateNumberValue == null || that.data.plateNumberValue.length == 0) {
      app.tip("请输入车牌号", "none");
      return false;
    }
    if (that.data.driverValue == null || that.data.driverValue.length == 0) {
      app.tip("请输入司机", "none");
      return false;
    }
    if (that.data.driverTelValue == null || that.data.driverTelValue.length == 0) {
      app.tip("请输入司机电话", "none");
      return false;
    } 
    if (that.data.driverLicenseValue == null || that.data.driverLicenseValue.length == 0) {
      app.tip("请输入驾驶证号", "none");
      return false;
    }
    if (that.data.insuranceCompanyId == null || that.data.insuranceCompanyId.length == 0) {
      app.tip("保险公司ID取得错误", "none");
      return false;
    }
    if (that.data.insuranceCodeId == null || that.data.insuranceCodeId.length == 0) {
      app.tip("险种ID取得错误", "none");
      return false;
    }
    if (that.data.sumInsuredValue == null || that.data.sumInsuredValue.length == 0) {
      app.tip("请输入保险金额", "none");
      return false;
    } else {
      if (that.data.sumInsuredValue == 0) {
        app.tip("保险金额必须大于0", "none");
        return false;
      }
    }
    if (that.data.photos == null || that.data.photos.length == 0) {
      app.tip("请选择照片", "none");
      return false; 
    }
 
   
    var insuranceName = that.data.insuranceName.replace("(惠)","");
    console.info("险种=====" + insuranceName)
    let parData = {
      consignmentNumber: that.data.inputTransportNumberValue,
      goodsNames: that.data.inputGoodsNameValue,
      orderQuantity: that.data.inputTotalNumberValue,               
      orderCount: that.data.inpitTotalVotesValue,
      fromArea: that.data.placeOfOriginatingValue,
      toArea: that.data.placeOfLoadingValue,
      consignmentTime: "",
      vehicleNumber: that.data.plateNumberValue,
      driver: that.data.driverValue,
      driverPhone: that.data.driverTelValue,
      insuranceCodeId: that.data.insuranceCodeId,
      insuranceName: that.data.insuranceName,
      insuranceCompanyName: that.data.insuranceCompanyName,
      insuredUserName: that.data.applicantUserName,
      applicantUserName: that.data.insuredUserName,
      sumInsured: that.data.sumInsuredValue,
      premiumRate: that.data.premiumRate,
      premium: that.data.premium,
      imageCount : that.data.photos,
      images: that.data.orderPic.toString(),
      imageNames : that.data.orderPicName.toString(),
      insuredUserGender: that.data.applicantUserGender,
      insuredUserBirthday: that.data.applicantUserBirthday,
      insuredIdType: that.data.applicantIdType,
      insuredPhone: that.data.applicantPhone,
      insuredCell: that.data.applicantCell,
      insuredZip: that.data.applicantZip,
      insuredAddress: that.data.applicantAddress,
      insuredEmail: that.data.applicantEmail,
      insuredIdNo: that.data.applicantIdNo,
      insuranceIdType: that.data.applicantIdType,
      applicantUserGender: that.data.insuredUserGender,
      applicantUserBirthday: that.data.insuredUserBirthday,
      applicantIdType: that.data.insuredIdType,
      applicantPhone: that.data.insuredPhone,
      applicantCell: that.data.insuredCell,
      applicantZip: that.data.insuredZip,
      applicantAddress: that.data.insuredAddress,
      applicantIdNo: that.data.insuredIdNo,
      applicantIdType: that.data.insuranceIdType,
      applicantEmail: that.data.insuredEmail,
      driverLicense: that.data.driverLicenseValue,
      roadLicenseNumber: that.data.roadLicenseNumberValue,
      frameNumber: that.data.frameNumberValue,
      transportVehiclesApprovedTotal: that.data.transportVehiclesApprovedTotalValue,
      detailedList: [{
        orderCube: 0,
        consignee: "默认",
        goodsReceiptPlace: "默认",
        remark: "默认",
        orderWeight: 0,
        shippingNoteNumber: "默认",
        packageType: "默认",
        orderQuantity: that.data.inputTotalNumberValue,
        placeOfLoading: "默认",
        goodsName: that.data.inputGoodsNameValue,
        consignor: "默认"
      }],
      notifyUrl: ""
    };
    let par = {
      userName: app.insureUserName,
      password: app.insurePassword,
      calculateWithArea: "1",
      data: parData
    };
    console.info("投保参数")
    console.info(par)
    wx.showLoading({
      title: '投保中',
      mask: true
    })
    wx.request({
      url: config.requestUrl + "insurance/addInsurance",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "data": par },
      success: function (msg) {
        wx.hideLoading()
        console.info("投保返回结果");
        console.info(msg);
        if (msg.data.code == "100") {

          wx.showModal({
            title: '系统提示',
            showCancel:false,
            content: '保险平台已受理,请稍后确认投保结果!\r\n未获得保单号视同投保失败',
            success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定');
              wx.navigateBack({//返回
                delta: 1
              })
            } else if (res.cancel) {
                 console.log('用户点击取消')
            }
            }
            })



         
        } else {
          app.tip("投保失败:" + msg.data.msg, "none");
        }

        console.info("投保返回结果结束");
      },
      fail: function (msg) {
        console.log(msg);
        app.tip("投保失败:" + msg, "none");
      }
    });
  },

  //改变checkbox
  checkboxChange: function (e) {
    console.info("checkBox值");
    console.info(e.detail.value);
    console.info(this.data.checkValue);
    if (e.detail.value=="0"){
      this.setData({
        checkBoxs: [{ name: '', value: '1', check: 'true' }],
        checkValue: "1"
      })
    } else {
      this.setData({
        checkBoxs: [{ name: '', value: '0', check: '' }],
        checkValue: "0"
      })
    }

  },

  doChoose(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var url = that.data.noticeUrlsList[index];
    wx.navigateTo({ url: "toubaoxuzhi?url=" + url})
  },

  getBalance: function (e) {
    var that = this;
    let par = {
      userName: app.insureUserName,
      password: app.insurePassword,
      typeCode: "保险"
    };

    wx.request({
      url: config.requestUrl + "insurance/queryBalance",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "data": par },
      success: function (msg) {
        console.info("优惠券余额返回结果");
        console.info(msg);
        that.setData({
          balance: msg.data.content
        })
        that.searchInsuranceCodeList();
      },
      fail: function (msg) {
        console.log(msg);
      }
    });
  },

  /**
   * 点击投保须知
   */
  showTouBaoXuZhi: function () {
    var that = this;
    wx.showModal({
      title: '投保须知',
      content: that.data.toubaoxuzhiValue,
      showCancel: true,
      confirmText: "同意",
      cancelText: "取消",
      success: function (res) {
        if (res.confirm) {
          that.setData({ checkBoxs: [{ name: '', value: '1', check: 'true' }], checkValue: "1"});
        } else {
          that.setData({ checkBoxs: [{ name: '', value: '0', check: '' }] , checkValue: "0"});
        }
      },
    })

  },

  /**
   * 调取险种
   */
  searchInsuranceCodeList: function () {
    var that = this;
    let par = {
      userName: app.insureUserName,
      password: app.insurePassword,
      insuranceCompanyId: "",
      bindingFlag: 0
    };

    wx.request({
      url: config.requestUrl + "insurance/insuranceCodeList",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "data": par },
      success: function (msg) {
        console.info("险种返回结果");
        console.info(msg);
        var insuranceCodeList=[];
        var insuranceName="";
        var yhqFlag = "";
        var taiPingFlag = "";
        var noticeTitles = "";
        var noticeUrls = "";



        if (Array.isArray(msg.data.content) && msg.data.content.length > 0) {

          for (var i = 0; i < msg.data.content.length; i++) {
            if (msg.data.content[i].couponFlag=="1" && that.data.balance>0){
              insuranceCodeList[i] = "(惠)" + msg.data.content[i].insuranceName;
              insuranceName = "(惠)" + msg.data.content[i].insuranceName;
              yhqFlag = "1";
              if (msg.data.content[i].insuranceCompany.insuranceCompanyCode == "taibao"){
                taiPingFlag = "1";
              }
              noticeTitles = msg.data.content[i].noticeTitles;
              noticeUrls = msg.data.content[i].noticeUrls;
            }else{
              insuranceCodeList[i] = msg.data.content[i].insuranceName;
              if (insuranceName == "" && msg.data.content[i].defaultFlag == "1"){
                insuranceName = msg.data.content[i].insuranceName;
                if (msg.data.content[i].insuranceCompany.insuranceCompanyCode == "taibao") {
                  taiPingFlag = "1";
                }
                noticeTitles = msg.data.content[i].noticeTitles;
                noticeUrls = msg.data.content[i].noticeUrls;
              }
            }
          }
        }else{
          insuranceCodeList="";
        }

      
        if (insuranceName==""&&msg.data.content!=null&&msg.data.content!=""){
          insuranceName = msg.data.content[0].insuranceName;
          noticeTitles = msg.data.content[0].noticeTitles;

          noticeUrls = msg.data.content[0].noticeUrls;
          if (msg.data.content[0].insuranceCompany.insuranceCompanyCode == "taibao") {
            taiPingFlag = "1";
          }
        }
        if(noticeTitles==undefined||noticeTitles=="undefined"){
          noticeTitles="";
        }
        if(noticeUrls==undefined||noticeUrls=="undefined"){
          noticeUrls="";
        }
        var noticeTitlesList = noticeTitles.split(",");
        var noticeUrlsList = noticeUrls.split(",");
        var insuranceCodeId="";
        var insuranceCompanyName="";
        var insuranceCompanyId="";

        if(msg.data.content!=null&&msg.data.content!=""){
          insuranceCodeId=msg.data.content[0].insuranceCodeId;
          insuranceCompanyName= msg.data.content[0].insuranceCompany.insuranceCompanyName;
          insuranceCompanyId = msg.data.content[0].insuranceCompany.insuranceCompanyId;
        }

        that.setData({
          insuranceCodeListAll:msg.data.content,
          insuranceCodeList: insuranceCodeList,
          insuranceCodeId:insuranceCodeId,
          insuranceName: insuranceName,
          noticeTitles:noticeTitles,
          yhqFlag: yhqFlag,
          taiPingFlag: taiPingFlag,
          noticeTitlesList: noticeTitlesList,
          noticeUrlsList:noticeUrlsList,
          insuranceCompanyName: insuranceCompanyName,
          insuranceCompanyId: insuranceCompanyId
        })

        that.searchInsuranceNotice(insuranceCodeId);
        that.searchInsuranceUserList();
        console.info("险种返回结果结束");
      },
      fail: function (msg) {
        console.log(msg);
      }
    });
  },

  /**
   * 参保人员列表查询
   */
  searchInsuranceUserList: function () {
    var that = this;
    let par = {
      userName: app.insureUserName,
      password: app.insurePassword,
      insuranceUserType: "",
      insuranceCodeId: that.data.insuranceCodeId,
    };

    wx.request({
      url: config.requestUrl + "insurance/insuranceUserList",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "data": par },
      success: function (msg) {
        console.info("参保人员列表返回结果");
        console.info(msg);
        var insurancePersonList = [];
        if (Array.isArray(msg.data.content.insurancePreson) && msg.data.content.insurancePreson.length > 0) {
          for (var i = 0; i < msg.data.content.insurancePreson.length; i++) {
            insurancePersonList[i] = msg.data.content.insurancePreson[i].insuranceUserName;
          }
        }
        var insuranceToPersonList = [];
        if (Array.isArray(msg.data.content.insurancePreson) && msg.data.content.insurancePreson.length > 0) {
          for (var i = 0; i < msg.data.content.insurancePreson.length; i++) {
            insuranceToPersonList[i] = msg.data.content.insuranceToPreson[i].insuranceUserName;
          }
        }
        console.info(msg.data.content.insurancePreson);
        console.info(msg.data.content.insurancePreson.length);
        if (msg.data.content.insurancePreson.length > 0 && msg.data.content.insuranceToPreson.length>0){
          that.setData({
            insurancePersonListAll: msg.data.content.insurancePreson,
            insurancePersonList: insurancePersonList,
            insuranceToPersonListAll: msg.data.content.insuranceToPreson,
            insuranceToPersonList: insuranceToPersonList,
            applicantUserName: msg.data.content.insuranceToPreson[0].insuranceUserName,
            insuredUserName: msg.data.content.insurancePreson[0].insuranceUserName,
            applicantUserGender: msg.data.content.insuranceToPreson[0].insuranceUserGender,
            applicantUserBirthday: msg.data.content.insuranceToPreson[0].insuranceUserBirthday,
            applicantIdType: msg.data.content.insuranceToPreson[0].insuranceIdType,
            applicantPhone: msg.data.content.insuranceToPreson[0].insurancePhone,
            applicantCell: msg.data.content.insuranceToPreson[0].insuranceCell,
            applicantZip: msg.data.content.insuranceToPreson[0].insuranceZip,
            applicantAddress: msg.data.content.insuranceToPreson[0].insuranceAddress,
            applicantEmail: msg.data.content.insuranceToPreson[0].insuranceEmail,
            applicantIdNo: msg.data.content.insuranceToPreson[0].insuranceIdNo,
            applicantIdType: msg.data.content.insuranceToPreson[0].insuranceIdType,
            insuredUserGender: msg.data.content.insurancePreson[0].insuranceUserGender,
            insuredUserBirthday: msg.data.content.insurancePreson[0].insuranceUserBirthday,
            insuredIdType: msg.data.content.insurancePreson[0].insuranceIdType,
            insuredPhone: msg.data.content.insurancePreson[0].insurancePhone,
            insuredCell: msg.data.content.insurancePreson[0].insuranceCell,
            insuredZip: msg.data.content.insurancePreson[0].insuranceZip,
            insuredAddress: msg.data.content.insurancePreson[0].insuranceAddress,
            insuredEmail: msg.data.content.insurancePreson[0].insuranceEmail,
            insuredIdNo: msg.data.content.insurancePreson[0].insuranceIdNo,
            insuranceIdType: msg.data.content.insurancePreson[0].insuranceIdType
          })
        } else {
          that.setData({
            insurancePersonListAll: "",
            insurancePersonList: "",
            insuranceToPersonListAll: "",
            insuranceToPersonList: "",
            applicantUserName: "",
            insuredUserName: "",
            applicantUserGender: "",
            applicantUserBirthday: "",
            applicantIdType: "",
            applicantPhone: "",
            applicantCell: "",
            applicantZip: "",
            applicantAddress: "",
            applicantEmail: "",
            applicantIdNo: "",
            applicantIdType: "",
            insuredUserGender: "",
            insuredUserBirthday: "",
            insuredIdType: "",
            insuredPhone: "",
            insuredCell: "",
            insuredZip: "",
            insuredAddress: "",
            insuredEmail: "",
            insuredIdNo: "",
            insuranceIdType: ""
          })
        }
        
        console.info("参保人员列表返回结果结束");
      },
      fail: function (msg) {
        console.log(msg);
      }
    });
  },

  /**
   * 获取投保须知
   */
  searchInsuranceNotice: function (insuranceCodeId) {
    var that = this;
    let par = {
      userName: app.insureUserName,
      password: app.insurePassword,
      insuranceCodeId: insuranceCodeId
    };

    wx.request({
      url: config.requestUrl + "insurance/insuranceCodeNoticeContent",
      method: "GET",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: { "data": par },
      success: function (msg) {
        console.info("投保须知返回结果");
        console.info(msg);

        that.setData({
          toubaoxuzhiValue: msg.data.content
        })
        console.info("投保须知返回结果结束");
      },
      fail: function (msg) {
        console.log(msg);
      }
    });
  },

  //  点击日期组件确定事件  
  bindDateChageStart: function (e) {
    this.setData({
      startDate: e.detail.value
    });
    this.data.startDate = e.detail.value;
  },

  //  点击时间组件确定事件  
  bindTimeChageStart: function (e) {
    this.setData({
      startTime: e.detail.value
    });
    this.data.startTime = e.detail.value;
  },

  chooseimage: function (e) {
    var id = e.target.dataset.id;
    var _this = this;
    // let ch = _this.data.carts.length * 100;
    // _this.setData({ isCover: "block", coverHeight: ch + "%" });
    // console.info("请求上传图片地址：");
    // console.info(app.sysInfo.uploadFileUrl);
    wx.chooseImage({
      count: 1, // 默认9  
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有  
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有  
      success: function (result) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        wx.showLoading({
          title: '图片上传中...',
        })
        console.info(result.tempFilePaths)  
        wx.uploadFile({
          url: app.sysInfo.uploadFileUrl,
          filePath: result.tempFilePaths[0],
          name: 'file',
          header: {
            "Content-Type":
              "multipart/form-data"
          },
          success: function (res) {
            console.info(res);
            if(res.statusCode == 200){
              
              var arr = _this.data.orderPic;
              var arrName = _this.data.orderPicName;
              arr.push(app.sysInfo.showFileUrl + JSON.parse(res.data).url);
              arrName.push(result.tempFilePaths[0].substring(result.tempFilePaths[0].lastIndexOf("/")+1))
              
              _this.setData({ 
                orderPic: arr,
                orderPicName : arrName,
                photos: arr.length
              });
              setTimeout(function () { _this.setData({ "isCover": "none" }); }, 1000);
            }else{
              app.tip("图片上传失败", "none")
            }
            wx.hideLoading();
          },
          fail: function (res) {
            wx.hideLoading();
            console.log('上传失败');
          }
        })
      },
      complete: function () {
        setTimeout(function () { _this.setData({ "isCover": "none" }); wx.hideLoading(); }, 1000);
      }
    })
  },
  imgYu: function (event) {
    var src = event.currentTarget.dataset.current;//获取data-src
    console.info("点击图片");
    console.info(src);
    var imgList = [];
    imgList[0] = src
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList
    })
  },

  deleteImage : function(e){
    var that = this;
    var orderPic = that.data.orderPic;
    var orderPicName = that.data.orderPicName;
    var index = e.currentTarget.dataset.imgindex;//获取当前长按图片下标
    console.log(index)
    wx.showModal({
      title: '提示',
      content: '确定要删除此图片吗？',
      success: function (res) {
       if (res.confirm) {
        orderPic.splice(index, 1);
        orderPicName.splice(index, 1);
        that.setData({
          orderPic : orderPic,
          orderPicName :  orderPicName,
          photos: orderPic.length
        })
       } else if (res.cancel) {
         console.log('点击取消了');
         return false;    
        }
      }
     })
  },

  bindMultiInsuranceCodeList(e) {
    var that=this;
    console.log('picker发送选择改变，携带值为', e.detail.value)
    console.log(that.data.insuranceCodeListAll)
    if (that.data.insuranceCodeListAll[e.detail.value].insuranceCompany.insuranceCompanyCode == "taibao") {
      that.setData({
        taiPingFlag: "1"
      })
    } else {
      that.setData({
        taiPingFlag: ""
      })
    }

    if (that.data.insuranceCodeListAll[e.detail.value].couponFlag=="1"){
      that.setData({
          yhqFlag:"1"
      })
    } else {
      that.setData({
        yhqFlag: ""
      })
    }
    var noticeTitles = that.data.insuranceCodeListAll[e.detail.value].noticeTitles;
    var noticeUrls = that.data.insuranceCodeListAll[e.detail.value].noticeUrls;

    var noticeTitlesList = noticeTitles.split(",")
    console.log("noticeTitlesList:" + noticeTitlesList)
    var noticeUrlsList = noticeUrls.split(",")

    that.setData({
      insuranceCodeListIndex: e.detail.value,
      insuranceCodeId: that.data.insuranceCodeListAll[e.detail.value].insuranceCodeId,
      insuranceName: that.data.insuranceCodeList[e.detail.value],
      insuranceCompanyName: that.data.insuranceCodeListAll[e.detail.value].insuranceCompany.insuranceCompanyName,
      insuranceCompanyId: that.data.insuranceCodeListAll[e.detail.value].insuranceCompany.insuranceCompanyId,
      noticeTitlesList: noticeTitlesList,
      noticeUrlsList: noticeUrlsList,
      premiumRate: "",
      premium: "",
      saveActionFlag: ""
    })
    that.searchInsuranceUserList();
    that.searchInsuranceNotice(that.data.insuranceCodeListAll[e.detail.value].insuranceCodeId);
  },
  bindMultiInsurancePersonList(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      insurancePersonListIndex: e.detail.value,
      insuredUserName: this.data.insurancePersonListAll[e.detail.value].insuranceUserName,
      insuredUserGender: this.data.insurancePersonListAll[e.detail.value].insuranceUserGender,
      insuredUserBirthday: this.data.insurancePersonListAll[e.detail.value].insuranceUserBirthday,
      insuredIdType: this.data.insurancePersonListAll[e.detail.value].insuranceIdType,
      insuredPhone: this.data.insurancePersonListAll[e.detail.value].insurancePhone,
      insuredCell: this.data.insurancePersonListAll[e.detail.value].insuranceCell,
      insuredZip: this.data.insurancePersonListAll[e.detail.value].insuranceZip,
      insuredAddress: this.data.insurancePersonListAll[e.detail.value].insuranceAddress,
      insuredEmail: this.data.insurancePersonListAll[e.detail.value].insuranceEmail,
      insuredIdNo: this.data.insurancePersonListAll[e.detail.value].insuranceIdNo,
      insuranceIdType: this.data.insurancePersonListAll[e.detail.value].insuranceIdType
    })
  },
  bindMultiInsuranceToPersonList(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      insuranceToPersonListIndex: e.detail.value,
      applicantUserName: this.data.insuranceToPersonListAll[e.detail.value].insuranceUserName,
      applicantUserGender: this.data.insuranceToPersonListAll[e.detail.value].insuranceUserGender,
      applicantUserBirthday: this.data.insuranceToPersonListAll[e.detail.value].insuranceUserBirthday,
      applicantIdType: this.data.insuranceToPersonListAll[e.detail.value].insuranceIdType,
      applicantPhone: this.data.insuranceToPersonListAll[e.detail.value].insurancePhone,
      applicantCell: this.data.insuranceToPersonListAll[e.detail.value].insuranceCell,
      applicantZip: this.data.insuranceToPersonListAll[e.detail.value].insuranceZip,
      applicantAddress: this.data.insuranceToPersonListAll[e.detail.value].insuranceAddress,
      applicantEmail: this.data.insuranceToPersonListAll[e.detail.value].insuranceEmail,
      applicantIdNo: this.data.insuranceToPersonListAll[e.detail.value].insuranceIdNo,
      applicantIdType: this.data.insuranceToPersonListAll[e.detail.value].insuranceIdType
    })
  },
  bindTransportNumber: function (e) {
    this.setData({
      inputTransportNumberValue: e.detail.value
    })
  }, 
  bindGoodsName: function (e) {
    this.setData({
      inputGoodsNameValue: e.detail.value
    })
  }, 
  bindTotalNumber: function (e) {
    this.setData({
      inputTotalNumberValue: e.detail.value
    })
  }, 
  bindTotalVotes: function (e) {
    this.setData({
      inpitTotalVotesValue: e.detail.value
    })
  }, 
  bindPlaceOfOriginating: function (e) {
    this.setData({
      placeOfOriginatingValue: e.detail.value
    })
  }, 
  bindPlaceOfLoading: function (e) {
    this.setData({
      placeOfLoadingValue: e.detail.value
    })
  }, 
  bindPlateNumber: function (e) {
    this.setData({
      plateNumberValue: e.detail.value
    })
  },
  bindDriver: function (e) {
    this.setData({
      driverValue: e.detail.value
    })
  }, 
  bindDriverTel: function (e) {
    this.setData({
      driverTelValue: e.detail.value
    })
  },  
  bindDriverLicense: function (e) {
    this.setData({
      driverLicenseValue: e.detail.value
    })
  },
  bindRoadLicenseNumber: function (e) {
    this.setData({
      roadLicenseNumberValue: e.detail.value
    })
  },
  bindFrameNumber: function (e) {
    this.setData({
      frameNumberValue: e.detail.value
    })
  },
  bindTransportVehiclesApprovedTotal: function (e) {
    this.setData({
      transportVehiclesApprovedTotalValue: e.detail.value
    })
  },  
  bindSumInsured: function (e) {
    this.setData({
      sumInsuredValue: e.detail.value,
      premiumRate: "",
      premium: "",
      saveActionFlag: ""
    })
  }, 
  aaaaa: function (e) {
    wx.downloadFile({
      url: 'https://b114.feiyang56.cn/insuranceCode/huatai2-3601.html', //仅为示例，并非真实的资源
      success(res) {
        // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
        if (res.statusCode === 200) {
          wx.playVoice({
            filePath: res.tempFilePath
          })
        }
      }
    }) 
  }, 
})