const app = getApp();
const config = require('../../../config.js');
Page({
  data: {
    pageSize: 20
  },
  /**
   * 生命周期函数--监听页面加载
   */
    onLoad: function (options) {
      getList(this);
    },

  /**
   * 跳转到家人页面
   */
  toAddInsurancer: function () {
    wx.navigateTo({
      url: 'addInsurancer',
    })
  },

  /**
   * 跳转到换人页面
   */
  toHuanren: function () {
    wx.navigateTo({
      url: 'editInsurancer',
    })
  },

  
})

/**
 * 获取数据
 */
function getList(_this) {
  // searchList(_this, function (msg) {
  //   if (Object.prototype.toString.call(msg.data.content.rows) === "[object Array]" && msg.data.content.rows.length > 0) {
  //     var arr = msg.data.content.rows;
       var arr =["","",""]
      _this.setData({
        wuliu: arr,
        showView: true
      });
  //   } else {
      // _this.setData({
      //   showView: false,
      //   message: "抱歉，没有找到相关数据！",
      //   imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      // });
    // }
  // });
}

function searchList(_this, callback) {

  wx.showLoading({ mask: true, title: "加载中" });
  let par = {
    page: 1,
    pageSize: _this.data.pageSize,
    userName: app.insureUserName,
    password: app.insurePassword,
    stateFlag: ""
  };
  // console.log(par);
  wx.request({
    url: config.requestUrl + "insurance/queryList",
    method: "GET",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: { "data": par },
    success: function (msg) {
      console.info("投保查询返回结果");
      console.info(msg);
      callback(msg);
      wx.hideLoading();
    },
    fail: function (msg) {
      // console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}