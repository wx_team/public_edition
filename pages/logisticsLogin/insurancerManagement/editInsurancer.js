const app = getApp()
const config = require('../../../config.js');
const util = require('../../../utils/util.js');
Page({
  data: {
    sexArray: ["男", "女"],
    sex: "男"
  },
  sexbie: function (e) {
    var sexArray = this.data.sexArray;
    var sex = sexArray[e.detail.value];
    this.setData({
      sex: sex
    })
  },
})