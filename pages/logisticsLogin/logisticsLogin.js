
var utilMd5 = require('../../utils/md5.js');
var config = require('../../config.js');
var app = getApp();

Page({
  data: {
    userName: null,
    defSenderCheckBoxs: [{ name: '', value: '0' ,check:true}],
    userPass: null,
    isDefaultSender:'1'
  },
  checkboxChangeDefSender: function (e) {
    console.log("是否记住密码");
    let isDefaultSender = 0;
    if (e.detail.value && e.detail.value.length > 0 && e.detail.value[0] == 0){
      isDefaultSender = 1;
    }
    this.setData({
      isDefaultSender: isDefaultSender
    })
  },
  onLoad: function (options) {
    var _this = this;
      _this.setData({
        userName: wx.getStorageSync("usercode"),
        userPass: wx.getStorageSync("password")
      })
    // if (options.mobile != undefined) {
    //   this.setData({ userName: options.mobile })
    // }
  },
  userNameInput: function (e) {
    this.setData({
      userName: e.detail.value
    })
  },
  userPassInput: function (e) {
    this.setData({
      userPass: e.detail.value
    })
  },
	/**
	 * 登录
	 */
  doLogin: function (e) {
    var userName = this.data.userName;
    var userPass = this.data.userPass;
    var isDefaultSender= this.data.isDefaultSender;
    if (userName == null || userName.length == 0) {
      app.tip('用户名不能为空', 'loading')
      return false;
    }
    if (userPass == null || userPass.length == 0) {
      app.tip('密码不能为空', 'loading')
      return false;
    }
    //userPass = utilMd5.hexMD5(userPass);
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: config.requestUrl + "/insurance/loginInsurance",
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        data: '{' +
          '"loginName":"' + userName + '",' +
          '"password": "' + userPass + '"}'
      },
      success: function (result) {
        console.info("投保登陆返回结果");
        console.info(result);
        wx.hideLoading();
        var data = result.data;
        if (data && data.code && data.code == 100) {

          if (isDefaultSender == 1) {
            wx.setStorageSync("usercode",userName);
            wx.setStorageSync("password",userPass);
          } else{     
            wx.setStorageSync("usercode",""); 
            wx.setStorageSync("password","");
          }
          var content = data.content;
          app.setInsureCompany(content)
          app.setInsureUserName(userName)
          app.setInsurePassword(userPass)
          wx.navigateTo({ url: "logisticsEnterprise/logisticsEnterprise?formatTopCompanyName=" + content.formatTopCompanyName + "&companyCode=" + content.companyCode + "&companyName=" + content.companyName})
        } else {
          var errMsg;
          if (data && data.msg) {
            errMsg = data.msg
          } else {
            errMsg = result.statusCode + ":" + result.errMsg
          }
          app.tip(errMsg, 'none')
        }
      },
      fail: function ({ errMsg }) {
        wx.hideLoading()
        app.tip(errMsg, 'none')
      }
    })
  },
  // doRegedit: function () {
  //   wx.navigateTo({ url: "../register/register" })
  // },
  // doForgetPwd: function () {
  //   var value = this.data.userName;
  //   if (value)
  //     wx.navigateTo({ url: "../user/forgetPwd/forgetPwd?mobile=" + value })
  //   else
  //     wx.navigateTo({ url: "../user/forgetPwd/forgetPwd" })
  // },

})