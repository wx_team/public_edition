// pages/issueDetails/issueDetails.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
      
    friends: [
     
    ],
    mobile:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    wx.showLoading({
      title: '加载中',
    })
    var id = options.id;
    var that = this;
    wx.request({
      url: host + 'publish/publishDetails',
      data: {
        id:id,
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        that.setData({
          list:res.data.data,
          mobile: res.data.data.mobile,
          friends:res.data.data.comment
        })
        wx.hideLoading();
      },
    })
  },
  userComment: function (e) {
    this.setData({
      userComment: e.detail.value
    })
  },
  /**
   * 评论
   */
  pinglun:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    var comment = that.data.userComment;
    wx.request({
      url: host + 'publish/publishComment',
      data: {
        uid:wx.getStorageSync("uid"),
        lid:id,
        comment: comment
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        that.hideModal();
        that.setData({
          friends:res.data.data
        })
        wx.showToast({
          title: res.data.msg,
          icon:'success',
        })

      },
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  fabu:function(e){
    wx.navigateTo({
      url: '/pages/issuenav/issuenav',
    })
  },
  callPhone:function(){
    var mobile = this.data.mobile
    wx.makePhoneCall({
      phoneNumber: mobile,
    })
  },
  returnSy:function(){
    wx.switchTab({
      url: '/pages/index/index',
    })
  }
})