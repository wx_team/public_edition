// pages/insurance/insurance.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: [
      {
        id: 0,
        type: 'image',
        url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big84000.jpg'
      },
      {
        id: 1,
        type: 'image',
        url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big84001.jpg',
      },
      {
        id: 2,
        type: 'image',
        url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big39000.jpg'
      }
    ],
    recommend: ["雇主险","货运险"],
    currentActive: 0,
    recommendList: [
      {
        id: 1,
        child: [
          {
            img: "../../images/icon/sp1.png",
            name: "员工险",
            course: "挑战中小物流企业员工保险极限",
            price:"30000",
          },
          {
            img: "../../images/icon/sp1.png",
            name: "员工险",
            course: "挑战中小物流企业员工保险极限",
            price:"30000",           
          }
        ]
      },
      {
        id: 2,
        child: [
          {
            img: "../../images/icon/sp1.png",
            name: "货运险",
            course: "挑战中小物流企业员工保险极限",
            price: "30000",
          },
          {
            img: "../../images/icon/sp1.png",
            name: "货运险",
            course: "挑战中小物流企业员工保险极限",
            price: "30000",
          }
        ]
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  NavTab: function (e) {
    var that = this;
    that.setData({
      currentActive: e.currentTarget.dataset.index
    })
  },
  insure:function(){
    wx.navigateTo({
      url: '/pages/insuranceDetails/insuranceDetails',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})