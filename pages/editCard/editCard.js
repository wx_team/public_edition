// pages/editCard/editCard.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk = new QQMapWX({ key: 'MBUBZ-VY2CW-4B5RA-OOIFR-U7L63-4VF6G' });
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    classList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    wx.request({
      url: host + 'me/edit',
      data: {
        uid: wx.getStorageSync('uid'),
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        var picker = [];
        var picker_index = null;
        var groupList = res.data.data.cate;
        for (var i = 0; i < groupList.length; i++) {
          picker[i] = groupList[i].title
          if (res.data.data.list.group_id == groupList[i].id){
            picker_index = i;
          }
        }
        console.log(picker);
        console.log(res.data.data.list.group_id + " | " + picker_index + "|" + groupList[picker_index].title);
        var arr = res.data.data.list.label.split(',');
        that.setData({
          list: res.data.data.list,
          groupList: groupList,
          picker: picker,
          classList:arr,
          index: picker_index,
          address: res.data.data.list.address
        })
        wx.hideLoading()
      },
    })
  },
  formSubmit: function (e) {
    var that = this;
    var data = e.detail.value;
    var msg = '';
    if (data.address == '') {
      msg = '请选择具体位置';
    }
    if (data.name == '') {
      msg = '请输入用户名';
    }
    if (data.phone == '' || data.phone.length != 11) {
      msg = '请输入正确的联系方式';
    }
    if (data.company == '') {
      msg = '请输入公司名字';
    }
    if (data.section == '') {
      msg = '请输入部门';
    }
    if (data.position == '') {
      msg = '请输入职位';
    }
    if (msg != '') {
      wx.showToast({
        title: msg,
        icon: 'none'
      })
      return false;
    }
    var tags = that.data.classList.join(',');
    var group_id = that.data.groupList[that.data.index].id;
    console.log(group_id + " | " + that.data.index + "|" + that.data.groupList[that.data.index].title);
    wx.request({
      url: host + 'me/editMyCard',
      data: {
        data: data,
        label: tags,
        group_id: group_id,
        uid: wx.getStorageSync('uid'),
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        wx.showToast({
          title: '保存成功',
          icon: 'success',
          duration: 2000,
          success: function () {
            setTimeout(function () {
              wx.navigateBack({
                delta: 1
              })
            }, 2000);
          }
        })
      },
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    // 调用接口
    qqmapsdk.reverseGeocoder({
      success: function (res) {
        that.setData({
          locationLat: res.result.location.lat,
          locationLng: res.result.location.lng
        });
      },
      fail: function (res) {
        //console.log(res);
      },
      complete: function (res) {
        //console.log(res);
      }
    });
  },
  PickerChange(e) {
    this.setData({
      index: e.detail.value
    })
  },
  clickClass: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    that.setData({
      classIndex: index
    })
  },
  openMap: function () {
    wx.navigateTo({
      url: '../position/position'
    })
  },
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  clear(e) {
    console.log(e)
    var index = e.currentTarget.dataset.index;
    this.data.classList.splice(index, 1);
    this.setData({
      classList: this.data.classList
    })
  },
  hideModalQd(e) {
    var classList = this.data.classList;
    if (this.data.region == null) {
      wx.showModal({
        title: '不可以为空',
      })
    } else {
      classList.push(this.data.region)
    }
    this.setData({
      modalName: null,
      classList: classList
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})