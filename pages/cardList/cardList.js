// pages/cardList/cardList.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showFileUrl: app.globalData.showFileUrl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var search = options.search;
    var index = options.index;
    var that = this;
    wx.showLoading({
      title: '搜索中',
    })
    wx.request({
      url: host + 'index/seek',
      data: {
        index: index,
        search: search
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        if (res.data.code == 1) {
          that.setData({
            list: res.data.data
          })
        }
        wx.hideLoading()
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  like: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.request({
      url: host + 'me/UserLike',
      data: {
        id: id,
        uid: wx.getStorageSync('uid')
      },
      method: 'POST',
      dataType: 'json',
      success: function (res) {
        if (res.data.code == 0) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
        } else {
          wx.showToast({
            title: '点赞成功',
            icon: 'success',
          })
        }
      },
    })
  },
  attention: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.request({
      url: host + 'me/attention',
      data: {
        id: id,
        uid: wx.getStorageSync('uid')
      },
      method: 'POST',
      dataType: 'json',
      success: function (res) {
        if (res.data.code == 0) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
        } else {
          wx.showToast({
            title: '关注成功',
            icon: 'success',
          })
        }
      },
    })
  },
})