var config = require('../../config.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    company: null
  },
  
  /**
  * 跳转到下单页面
  */
  toAddOrder: function (e) {
    wx.navigateTo({
      url: '../order/order?jjfs=' + e.target.dataset.t + "&sendc=" + (e.target.dataset.s == undefined || "undefined" == e.target.dataset.s || "null" == e.target.dataset.s ? "" : e.target.dataset.s)
    })
  },
  /**
   * 跳转到取款页面
   */
  toWithdrawal: function () {
    wx.navigateTo({
      url: '../handheldWithdrawals/withdrawalList',
    })
  },
  /**
 * 跳转到货物跟踪页面
 */
  toFreightTrack: function () {
    wx.navigateTo({
      url: '../freightTrack/searchFreightTrack',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      item: JSON.parse(options.webInfo),
      company: app.companyInfo
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  }
  // ,

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {
    
  // }
})