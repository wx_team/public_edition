// pages/comList/comList.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index:"",
    recommendList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var search = options.search;
    var index = options.index;
    var that = this;
    that.setData({
      index:index
    })
    wx.showLoading({
      title: '搜索中',
    })
    wx.request({
      url: host + 'index/seek',
      data: {
        index:index,
        search: search
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        if (res.data.code == 1){
          that.setData({
            recommendList: res.data.data
          })
        }
        wx.hideLoading()
      },
    })
  },
  details: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/information/information?id=' + id,
    })
  }, 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})