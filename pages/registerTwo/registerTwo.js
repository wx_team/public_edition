var config = require('../../config.js');
const app = getApp()
var getCaptcha = require('../common/js/getCaptcha.js');
var captcha = null
var phoneNum = null
var maxTime = 90
var currentTime = maxTime //倒计时的事件（单位：s）  
var interval = null

Page({
  data: {
    showButton: true,
    time: currentTime,
    mobileDisable: false,
    flag:1,
    imgList: [],
    imgListLogo: [],
  },
  signUpSubmit: function (e) {
    var info = e.detail.value;
    // if (!info.name) {
    //   this.showInfo('请输入姓名')
    // } else 
    if (!info.mobile) {
      this.showInfo('请输入手机号');
    } else if (!info.password) {
      this.showInfo('请输入密码');
    } else if (!info.password2) {
      this.showInfo('请再次输入密码', "none");
    } else if (info.password != info.password2) {
      this.showInfo('两次密码不一样', "none");
    } else if (!info.captcha) {
      this.showInfo('请输入验证码');
    }
    // else if (this.data.captcha == null || this.data.captcha == ''){
    //   this.showInfo('请获取验证码');
    // } 
    else if (this.data.captcha != info.captcha) {
      this.showInfo('验证码不正确');
    } else {
      var regMobile = /^[1][3,4,5,6,7,8][0-9]{9}$/
      if (!regMobile.test(info.mobile)) {
        app.tip("手机号码格式不正确", "none");
        return false;
      }
      if (info.idNo != undefined && info.idNo != null && info.idNo != "") {
        var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if (!regIdNo.test(info.idNo)) {
          app.tip("身份证号填写有误", 'none')
          return false;
        }
      }
      //info.openid = app.openid
      this.signUp(info)
    }
  },
  showInfo: function (msg, icon) {
    if (msg) {
      wx.showToast({
        title: msg,
        icon: icon ? icon : 'loading',
        duration: 1000
      })
    }
  },
  input_phoneNum: function (e) {
    phoneNum = e.detail.value
  },
  // getCaptcha: function () {
  //   if (phoneNum == null || phoneNum == '') {
  //     this.showInfo('请输入电话号码')
  //     return false
  //   }
  //   var regMobile = /^[1][3,4,5,6,7,8][0-9]{9}$/
  //   if (!regMobile.test(phoneNum)) {
  //     app.tip("手机号码格式不正确", "none");
  //     return false;
  //   }
  //   this.setData({
  //     able: true
  //   });
  //   getCaptcha.doRequest(this, phoneNum, 'reg')
  // },
  captchaCallback: function (obj) {
    captcha = obj.message;
    this.data.captcha = captcha;
    console.info("发送验证码回调");
    console.info(captcha);
    this.setData({
      showButton: false
    })
    var that = this;
    currentTime = maxTime;
    interval = setInterval(function () {
      currentTime--;
      that.setData({
        time: currentTime
      })
      if (currentTime <= 0) {
        that.setData({
          able: false
        });
        clearInterval(interval)
        currentTime = -1
      }
    }, 1000)
  },
  reSendPhoneNum: function () {
    if (phoneNum == null || phoneNum == '') {
      this.showInfo('请输入电话号')
      return false
    }
    var regMobile = /^[1][3,4,5,6,7,8][0-9]{9}$/
    if (!regMobile.test(phoneNum)) {
      app.tip("手机号码格式不正确", "none");
      return false;
    }
    this.setData({
      able: true
    });
    if (currentTime < 0) {
      var that = this
      currentTime = maxTime
      // interval = setInterval(function () {
      //   currentTime--
      //   that.setData({
      //     time: currentTime
      //   })
      //   if (currentTime <= 0) {
      //     that.setData({
      //       able: false
      //     });
      //     currentTime = -1
      //     clearInterval(interval)
      //   }
      // }, 1000)

      captcha = null

      // this.getCaptcha.doRequest(this, phoneNum)
      getCaptcha.doRequest(this, phoneNum, 'reg');

    } else {
      this.showInfo('短信已发到您的手机，请稍后重试')
    }
  },
  signUp: function (data) {
    var that = this;
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: config.registerUrl,
      data: { "data": JSON.stringify(data) },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function (res) {
        wx.hideLoading()
        var result = res.data;
        console.log(res.data);
        if (result.code == 100) {
          that.showInfo('注册成功', 'success')
          app.loginMobile = data.mobile;
          wx.navigateTo({
            url: '/pages/authorizedLogin/authorizedLogin?mobile=' + data.mobile
          });
        } else {
          var errMsg;
          if (result && result.msg) {
            errMsg = result.msg
          } else {
            errMsg = res.statusCode + ":" + res.errMsg
          }
          that.showInfo(errMsg)
        }
      },
      fail: function (res) {
        wx.hideLoading()
        console.log(res);
        var msg = '网络连接失败';
        if (res.errMsg) msg = res.errMsg;
        that.showInfo(msg)
      }
    })
  },

  onLoad: function (options) {

  },
  last:function(){
    this.setData({
      flag:2
    })
  },
  DelImgLogo(e) {
    wx.showModal({
      title: '召唤师',
      content: '确定要删除这段回忆吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          this.data.imgListLogo.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgListLogo: this.data.imgListLogo
          })
        }
      }
    })
  },
  ViewImageLogo(e) {
    wx.previewImage({
      urls: this.data.imgListLogo,
      current: e.currentTarget.dataset.url
    });
  },
  ChooseImageLogo() {
    wx.chooseImage({
      count: 1, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        if (this.data.imgListLogo.length != 0) {
          this.setData({
            imgListLogo: this.data.imgListLogo.concat(res.tempFilePaths)
          })
        } else {

          this.setData({
            imgListLogo: res.tempFilePaths
          })
        }
      }
    });
  },
})