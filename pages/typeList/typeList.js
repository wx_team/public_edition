// pages/typeList/typeList.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    tabText:[],
    TabCur: 0,
    id:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that =this;
    that.setData({
      id:options.id
    })
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      url: host + 'publish/typeList',
      data: {
        id:options.id,
        uid: wx.getStorageSync('uid'),
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        
          that.setData({
            list:res.data.data.list,
            tabText: res.data.data.cate,
          })
        wx.hideLoading();
      },
    })
  },
  dianzan: function (e) {
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.request({
      url: host + 'publish/dianzan',
      data: {
        id: id,
        uid: 1
      },
      dataType: 'json',
      method: "post",
      success: function (res) {
        if (res.data.code == 0) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          })
        }
        that.setData({
          dianzan: false
        })
      }
    })
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60
    })
    var ids = e.currentTarget.dataset.ids;
    var that = this;
    var id = that.data.id;
    wx.request({
      url: host + 'publish/typeTab',
      data: {
        ids: ids,
        id:id
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        that.setData({
          list: res.data.data
        })
      },
    })
  },
  card:function(e){
    var uid = e.currentTarget.dataset.uid;
    wx.navigateTo({
      url: '/pages/myCard/myCard?uid='+uid,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  seeImgs: function (e) {
    var that = this;
    var upindex = e.currentTarget.dataset.pid;
    var index = e.currentTarget.dataset.index;
    var list = that.data.list;
    wx.previewImage({
      current: list[upindex].imgs[index], // 当前显示图片的http链接
      urls: list[upindex].imgs // 需要预览的图片http链接列表
    })
  },
  classifyList: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/typeList/typeList?id=' + id,
    })
  },
})