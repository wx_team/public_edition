// pages/publishList/publishList.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var search = options.search;
    var index = options.index;
    var that = this;
    wx.showLoading({
      title: '搜索中',
    })
    wx.request({
      url: host + 'index/seek',
      data: {
        index: index,
        search: search
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        if (res.data.code == 1) {
          that.setData({
            list: res.data.data
          })
        }
        wx.hideLoading()
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 点赞
   */
  dianzan: function (e) {
    var id = e.currentTarget.dataset.id;
    var index = e.currentTarget.dataset.index;
    var that = this;
    var list = that.data.list;
    if (list[index].zan_status == 1) {
      wx.request({
        url: host + 'publish/closeDianzan',
        data: {
          id: id,
          uid: wx.getStorageSync('uid'),
        },
        method: 'POST',
        dataType: 'json',
        success: function (res) {
          list[index].zan_status = 0;
          list[index].praise_num = list[index].praise_num - 1;
          that.setData({
            list: list
          })
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          })
        },
      })
    } else {
      wx.request({
        url: host + 'publish/dianzan',
        data: {
          id: id,
          uid: wx.getStorageSync('uid'),
        },
        dataType: 'json',
        method: "post",
        success: function (res) {
          if (res.data.code == 1) {
            list[index].zan_status = 1;
            list[index].praise_num = list[index].praise_num + 1
            that.setData({
              list: list
            })
            wx.showToast({
              title: res.data.msg,
              icon: 'none',
              duration: 2000
            })
          }
        }
      })
    }

  },
  /**
   * 取消点赞
   */
  closeDianzan: function (e) {
    var that = this;
    var list = that.data.list;
  },

  issueDetails: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/issueDetails/issueDetails?id=' + id,
    })
  }
  
})