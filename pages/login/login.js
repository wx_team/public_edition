
var utilMd5 = require('../../utils/md5.js');
var config = require('../../config.js');
const app = getApp();

Page({
  data: {
    userName: null,
    userPass: null
  },

  onLoad: function (options) {
    if (options.mobile != undefined) {
      this.setData({ userName: options.mobile })
    }
  },
  userNameInput: function (e) {
    this.setData({
      userName: e.detail.value
    })
  },
  userPassInput: function (e) {
    this.setData({
      userPass: e.detail.value
    })
  },
	/**
	 * 登录
	 */
  doLogin: function (e) {
    var userName = this.data.userName;
    var userPass = this.data.userPass;
    if (userName == null || userName.length == 0) {
      app.tip('用户名不能为空', 'loading')
      return false;
    }
    if (userPass == null || userPass.length == 0) {
      app.tip('密码不能为空', 'loading')
      return false;
    }
    //userPass = utilMd5.hexMD5(userPass);
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: config.loginUrl,
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        "username": userName,
        "password": userPass,
        "openid": app.openid
      },
      success: function (result) {
        wx.hideLoading();
        var data = result.data;
        if (data && data.code && data.code == 100) {
          var content = data.content;
          console.info("登陆设置loginUser信息");
          console.info(data.content);
          app.setLoginUser(data.content)
          //wx.switchTab({ url: '../index/index' })
          wx.reLaunch({
            url: '../index/index',
          })
        } else {
          var errMsg;
          if (data && data.msg) {
            errMsg = data.msg
          } else {
            errMsg = result.statusCode + ":" + result.errMsg
          }
          app.tip(errMsg, 'none')
        }
      },
      fail: function ({ errMsg }) {
        wx.hideLoading()
        app.tip(errMsg, 'none')
      }
    })
  },
  doRegedit: function () {
    wx.navigateTo({ url: "../register/register" })
  },
  doForgetPwd: function () {
    var value = this.data.userName;
    if (value)
      wx.navigateTo({ url: "../user/forgetPwd/forgetPwd?mobile=" + value })
    else
      wx.navigateTo({ url: "../user/forgetPwd/forgetPwd" })
  },

})