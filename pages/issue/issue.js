// pages/issue/issue.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: [],
    TabCur: 0,
    tabText:[],
    list: [],
    icon:[]
  },

  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60
    })
    var ids = e.currentTarget.dataset.ids;
    this.setData({
      ids:ids,
      
    })
    var that = this;
    wx.request({
      url: host + 'publish/tabSelect',
      data: {
        id:ids,
        uid: wx.getStorageSync('uid'),
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        that.setData({
          list:res.data.data
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var ids = this.data.ids;
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    wx.request({
      url: host + 'publish',
      data:{
        id: ids,
        uid: wx.getStorageSync('uid'),
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        var list = res.data.data.list;
        that.setData({
          swiperList: res.data.data.banner,
          tabText: res.data.data.cate,
          list: list,
          icon: res.data.data.type
        })
        wx.hideLoading();
      },
    })
  },
  /**
   * 点赞
   */
  dianzan:function(e){
    // console.info("w:" + app.openid);
    var id = e.currentTarget.dataset.id;
    var index = e.currentTarget.dataset.index;
    var that = this;
    var list = that.data.list;
    if (list[index].zan_status == 1) {
      wx.request({
        url: host + 'publish/closeDianzan',
        data: {
          id: id,
          uid: wx.getStorageSync('uid')
          // uid: app.openid,
        },
        method: 'POST',
        dataType: 'json',
        success: function (res) {
          list[index].zan_status = 0;
          list[index].praise_num = list[index].praise_num - 1;
          that.setData({
            list: list
          })
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          })
        },
      })
    }else{
      
      wx.request({
        url: host + 'publish/dianzan',
        data: {
          id: id,
          uid: wx.getStorageSync('uid')
        },
        dataType: 'json',
        method: "post",
        success: function (res) {
          if (res.data.code == 1) {
            list[index].zan_status = 1;
            list[index].praise_num = list[index].praise_num + 1
            that.setData({
              list: list
            })
            wx.showToast({
              title: res.data.msg,
              icon: 'none',
              duration: 2000
            })
          }
        }
      })
    }
    
  },
  /**
   * 取消点赞
   */
  closeDianzan:function(e){
    var that = this;
    var list = that.data.list;
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  toIssueNav: function(){
    wx.navigateTo({
      url: '../issuenav/issuenav',
    })
  },
  issueDetails:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/issueDetails/issueDetails?id='+id,
    })
  },

  seeImgs: function(e){
    var that = this;
    var upindex = e.currentTarget.dataset.pid;
    var index = e.currentTarget.dataset.index;
    var list = that.data.list;
    wx.previewImage({
      current: list[upindex].imgs[index], // 当前显示图片的http链接
      urls: list[upindex].imgs // 需要预览的图片http链接列表
    })
  },
  classifyList:function(e){
    var id = e.currentTarget.dataset.id;
    wx.request({
      url: host + 'publish/typelistt',
      data: {
        uid: wx.getStorageSync('uid')
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        if(res.data.code == 0){
          wx.navigateTo({
            url: '/pages/occupancy/occupancy'
          })
        }else{

          wx.navigateTo({
            url: '/pages/typeList/typeList?id=' + id,
          })

        }
      },

    })
    
  },
  card:function(e){
    var uid = e.currentTarget.dataset.uid;
    wx.navigateTo({
      url: '/pages/myCard/myCard?uid=' + uid,
    })
  },
})