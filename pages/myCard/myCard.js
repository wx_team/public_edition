// pages/myCard/myCard.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk = new QQMapWX({ key: 'MBUBZ-VY2CW-4B5RA-OOIFR-U7L63-4VF6G' });
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: null,
    picker: ['喵喵喵', '汪汪汪', '哼唧哼唧'],
    classIndex: 0,
    classList: [
      // { id: 1, name: '推广' },
    ],
    flag:0,
    user:[],
    uid:0,
    like_status:0,
    attention_status:0,
    showFileUrl: app.globalData.showFileUrl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    console.log(options.uid);
    if(options.uid != undefined){
      this.setData({
        uid: options.uid
      })
    }
  },
  onShow: function () {
    var that = this;
    var uid = that.data.uid;
    if(uid == 0){
      uid = wx.getStorageSync('uid');
    }
    wx.request({
      url: host + 'me/industry',
      data: {
        uid: uid,
        touid: wx.getStorageSync('uid')
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        var picker = [];
        var groupList = res.data.data.list;
        for (var i = 0; i < groupList.length; i++) {
          picker[i] = groupList[i].title
        }
        that.setData({
          groupList: groupList,
          picker: picker,
          user: res.data.data.user,
          flag: res.data.data.flag,
          like_status: res.data.data.like_status,
          attention_status: res.data.data.attention_status
        })
        wx.hideLoading();
      },
    })
    // 调用接口
    qqmapsdk.reverseGeocoder({
      success: function (res) {
        that.setData({
          locationLat: res.result.location.lat,
          locationLng: res.result.location.lng
        });
      },
      fail: function (res) {
        //console.log(res);
      },
      complete: function (res) {
        //console.log(res);
      }
    });
  },
  nolike:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.request({
      url: host + 'me/upUserLike',
      data: {
        id: id,
        uid: wx.getStorageSync('uid')
      },
      method: 'POST',
      dataType: 'json',
      success: function (res) {
        if (res.data.code == 0) {
          // wx.showToast({
          //   title: res.data.msg,
          //   icon:'none'
          // })
        } else {
          that.setData({
            like_status: 0
          })
          wx.showToast({
            title: '取消点赞',
            icon: 'success',
          })
        }
      },
    })
  },
  like:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.request({
      url: host + 'me/UserLike',
      data: {
        id:id,
        uid:wx.getStorageSync('uid')
      },
      method: 'POST',
      dataType: 'json',
      success: function(res) {
        if(res.data.code == 0){
          // wx.showToast({
          //   title: res.data.msg,
          //   icon:'none'
          // })
        }else{
          that.setData({
            like_status: 1
          })
          wx.showToast({
            title: '点赞成功',
            icon:'success',
          })
        }
      },
    })
  },
  attention:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.request({
      url: host + 'me/attention',
      data: {
        id: id,
        uid: wx.getStorageSync('uid')
      },
      method: 'POST',
      dataType: 'json',
      success: function (res) {
        if (res.data.code == 0) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
        } else {
          that.setData({
            attention_status: 1
          })
          wx.showToast({
            title: '关注成功',
            icon: 'success',
          })
        }
      },
    })
  },
  noattention: function (e) {
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.request({
      url: host + 'me/upAttention',
      data: {
        id: id,
        uid: wx.getStorageSync('uid')
      },
      method: 'POST',
      dataType: 'json',
      success: function (res) {
        if (res.data.code == 0) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
        } else {
          that.setData({
            attention_status: 0
          })
          wx.showToast({
            title: '取消关注',
            icon: 'success',
          })
        }
      },
    })
  },
  PickerChange(e) {
    this.setData({
      index: e.detail.value
    })
  },
  clickClass: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    that.setData({
      classIndex: index
    })
  },
  openMap: function () {
    wx.navigateTo({
      url: '../position/position'
    })
  },
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  formSubmit: function (e) {
    var that = this;
    var data = e.detail.value;
    var msg = '';
    if (data.address == '') {
      msg = '请选择具体位置';
    }
    if (data.name == '') {
      msg = '请输入用户名';
    }
    if (data.phone == '' || data.phone.length != 11) {
      msg = '请输入正确的联系方式';
    }
    if (data.company == '') {
      msg = '请输入公司名字';
    }
    if (data.section == '') {
      msg = '请输入部门';
    }
    if (data.position == '') {
      msg = '请输入职位';
    }
    if (msg != '') {
      wx.showToast({
        title: msg,
        icon: 'none'
      })
      return false;
    }
    var tags = that.data.classList.join(',');
    var group_id = that.data.groupList[that.data.index].id;
    wx.request({
      url: host +  'me/addMyCard',
      data: {
        data:data,
        label: tags,
        group_id: group_id,
        uid: wx.getStorageSync('uid'),
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        wx.showToast({
          title: '保存成功',
          icon: 'success',
          duration: 2000,
          success: function () {
            setTimeout(function () {
              wx.navigateBack({
                delta: 1
              })
            }, 2000);
          }
        })
      },
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  clear(e){
    console.log(e)
    var index = e.currentTarget.dataset.index;
    this.data.classList.splice(index,1);
    this.setData({
      classList: this.data.classList
    })
  },
  hideModalQd(e){
    var classList = this.data.classList;
    if (this.data.region == null){
      wx.showModal({
        title: '不可以为空',
      })
    }else{
      classList.push(this.data.region)
    }
    this.setData({
      modalName: null,
      classList: classList
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  bianji:function(){
    wx.navigateTo({
      url: '../editCard/editCard'
    })
  },
  onShareAppMessage() {
    var name = wx.getStorageSync("userInfo");
    var uid = this.data.uid;
    return {
     title: '分享名片',
      desc: name.nickname,
      path: '/pages/myCard/myCard?uid='+uid
   }
  }
})