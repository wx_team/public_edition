// pages/my_friend/my_friend.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    skip_a: true,
    skip_b: false,
    arr1:[],
    arr2:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    wx.request({
      url: host + 'me/likePage',
      data: {
        uid: wx.getStorageSync('uid')
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        that.setData({
          arr1: res.data.data.arr1,
          arr2: res.data.data.arr2
        })
        wx.hideLoading();
      }
    })
  },
  details: function (e) {
    var touid = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/myCard/myCard?uid=' + touid,
    })
  },
  tabFun: function (e) {
    var self = this;
    //精品课程
    //  sessionStorage.setItem('showMask',true); 
    if (e.target.dataset.id && e.target.dataset.id == 0) {
      self.setData({
        skip_a: true,
        skip_b: false
      })
    }
    if (e.target.dataset.id && e.target.dataset.id == 1) {
      self.setData({
        skip_a: false,
        skip_b: true
      })
    }
  }
})