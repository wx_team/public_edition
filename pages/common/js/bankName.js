var bankList = {
	"PSBC": "中国邮政储蓄银行", 
	"ICBC":	"中国工商银行", 
	"ABC":"中国农业银行", 
	"BOC":"中国银行", 
	"CCB":"中国建设银行", 
	"COMM":"中国交通银行", 
	"CMB":"招商银行", 
	"CMBC":"中国民生银行", 
	"CEB":"中国光大银行", 
	"CITIC":"中信银行", 
	"HXBANK":"华夏银行", 
	"SPABANK":"深发/平安银行", 
	"CIB":"兴业银行", 
	"SHBANK":"上海银行", 
	"SPDB":"浦东发展银行", 
	"GDB":"广发银行", 
	"BOHAIB":"渤海银行", 
	"GCB":"广州银行", 
	"HSBANK":"徽商银行", 
	"JSBANK":"江苏银行", 
	"NJCB":"南京银行", 
	"NBBANK":"宁波银行", 
	"BJBANK":"北京银行", 
	"BJRCB":"北京农村商业银行", 
	"JLBANK":"吉林银行", 
	"HKB":"汉口银行", 
	"SJBANK":"盛京银行", 
	"DLB":"大连银行", 
	"BHB":"河北银行", 
	"URMQCCB":"乌鲁木齐市商业银行", 
	"SXCB":"绍兴银行",
	"CDCB":"成都商业银行",
	"FSCB":"抚顺银行", 
	"ZZBANK":"郑州银行", 
	"NXBANK":"宁夏银行", 
	"CQBANK":"重庆银行", 
	"HRBANK":"哈尔滨银行", 
	"LZYH":"兰州银行", 
	"QDCCB":"青岛银行", 
	"QHDCCB":"秦皇岛市商业银行", 
	"BOQH":"青海银行", 
	"TZCB":"台州银行", 
	"CSCB":"长沙银行", 
	"BOQZ":"泉州银行", 
	"BSB":"包商银行", 
	"DAQINGB":"龙江银行", 
	"SHRCB":"上海农商银行", 
	"GLBANK":"桂林银行", 
	"CDRCB":"成都农村商业银行", 
	"FJNX":"福建省农村信用社联合社", 
	"TRCB":"天津农村商业银行", 
	"JSRCU":"江苏省农村信用社联合社", 
	"SLH":"湖南农村信用社联合社", 
	"JXNCX":"江西省农村信用社联合社", 
	"SCBBANK":"商丘市商业银行", 
	"HRXJB":"华融湘江银行", 
	"HSBK":"衡水市商业银行", 
	"CQNCSYCZ":"重庆南川石银村镇银行", 
	"HNRCC":"湖南省农村信用社联合社", 
	"XTB":"邢台银行", 
	"LPRDNCXYS":"临汾市尧都区农村信用合作联社", 
	"DYCCB":"东营银行", 
	"SRBANK":"上饶银行", 
	"DZBANK":"德州银行", 
	"CDB":"承德银行", 
	"YNRCC":"云南省农村信用社", 
	"LZCCB":"柳州银行", 
	"WHSYBANK":"威海市商业银行", 
	"HZBANK":"湖州银行", 
	"BANKWF":"潍坊银行", 
	"GZB":"赣州银行", 
	"RZGWYBANK":"日照银行", 
	"NCB":"南昌银行", 
	"GYCB":"贵阳银行", 
	"BOJZ":"锦州银行", 
	"QSBANK":"齐商银行", 
	"RBOZ":"珠海华润银行", 
	"HLDCCB":"葫芦岛市商业银行", 
	"HBC":"宜昌市商业银行", 
	"HZCB":"杭州商业银行", 
	"JSBANK":"苏州市商业银行", 
	"LYCB":"辽阳银行", 
	"LYB":"洛阳银行", 
	"JZCBANK":"焦作市商业银行", 
	"ZJCCB":"镇江市商业银行", 
	"FGXYBANK":"法国兴业银行", 
	"DYBANK":"大华银行", 
	"DIYEBANK":"企业银行", 
	"HQBANK":"华侨银行", 
	"HSB":"恒生银行", 
	"LSB":"临沂商业银行", 
	"YTCB":"烟台商业银行", 
	"QLB":"齐鲁银行", 
	"BCCC":"BC卡公司", 
	"CYB":"集友银行", 
	"ANTBANK":"蚂蚁银行", 
	"XJRCU":"新疆维吾尔自治区农村信用社", 
	"SXRCU":"山西农村信用社", 
	"HURCB":"湖北省农村信用社"
}

const getName = {
	getName: function(bankCode){
		return bankList[bankCode]
	}
}
module.exports = getName