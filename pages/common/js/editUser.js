var app = getApp()

const edit = {
	/*
	url 完整地址
	param json格式的参数
	*/
	doEdit: function (url, param, jumpToUrl) {
		wx.showLoading({
      title: '加载中',
			mask: true
		})
		wx.request({
			url: url,
			data: { data: JSON.stringify(param) },
			method: 'POST',
			header: {
				'content-type': 'application/x-www-form-urlencoded'
			},
			success: function (res) {
				wx.hideLoading()
				var result = res.data;
				if (result.code == 100) {
					app.tip('操作成功', 'success')
					if (result.content.content)
						app.setLoginUser(result.content.content)
					else
						app.setLoginUser(result.content)
					if (jumpToUrl != null && jumpToUrl != '') {
            			wx.switchTab({ url: jumpToUrl })
					} else {
						wx.navigateBack({
							delta: 1
						})
					}
				} else {
					var errMsg;
					if (result && result.msg) {
						errMsg = result.msg
					} else {
						errMsg = res.statusCode + ":" + res.errMsg
					}
					app.tip(errMsg, 'none')
				}
			},
			fail: function (res) {
				app.tip(res.errMsg, 'none')
			}
		})
	}
};

module.exports = edit