//获取注册、修改密码 验证码
var config = require('../../../config.js');
var app = getApp();
var getCaptcha = {
	//sendType 发送方式:修改密码/注册/掌上取款（findPass/reg/withDraw）
  doRequest: function (that, phoneNum, sendType) {
		var url = config.requestUrl + "user/sendVaildateCodeByMobile"
		wx.request({
			url: url,
			data: {
        mobile: phoneNum,
        sendType: sendType,
        code: "",//app.companyInfo.code
        password:"" //app.companyInfo.tenantPass
			},
			method: 'POST',
			header: {
				'content-type': 'application/x-www-form-urlencoded'
			},
			success: function (res) {
				var result = res.data;
				console.log(res)
				if (result.code == 100) {
					var obj = result.content
					if (typeof (that.captchaCallback) == "function") {
						that.captchaCallback(obj);
					}
				} else {
          that.setData({ showButton: true, able: false});
					wx.showToast({
						title: result.msg,
						icon: 'loading',
						duration: 1000
					})
				}

			},
			fail: function (res) {
				wx.showToast({
					title: "网络连接失败",
					icon: 'loading',
					duration: 1000
				})
			}
		})
	}
}
module.exports = getCaptcha