// pages/myEnterprise/myEnterprise.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  /**
   * 企业详情
   */
  bindTouchStart: function (e) {
    this.startTime = e.timeStamp;
  },
  bindTouchEnd: function (e) {
    this.endTime = e.timeStamp;
  },
  details:function(e){
    var id = e.currentTarget.dataset.id;
    if (this.endTime - this.startTime < 350) {
      wx.navigateTo({
        url: '/pages/occupancy/occupancy?id=' + id+"&edit="+true,
      })
    }
  },
  /**
   * 
   */
  bingLongTap:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确认删除吗？',
      success(res) {
        if (res.confirm) {
          wx.request({
            url: host + 'Enterprise/delEnterprise',
            data: {
              id:id,
              uid: wx.getStorageSync("uid")
            },
            method: 'post',
            dataType: 'json',
            success: function(res) {
              wx.showToast({
                title: '删除成功',
                icon: 'success',
                duration: 2000,
                success:function(){
                  that.setData({
                    list:res.data.data
                  })
                }
              })
            },
          })
        } else if (res.cancel) {
          
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      url: host + 'me/getMyEnterprise',
      data: {
        uid: wx.getStorageSync("uid")
      },
      method: 'post',
      dataType: 'json',
      success: function (res) {
        that.setData({
          list: res.data.data
        })
        wx.hideLoading();
      },
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})