const app = getApp()
var config = require('../../../config.js')
const bankName = require('../../common/js/bankName.js')
const util = require('../../../utils/util.js')
var validate = false

Page({
	data: {
		bank: null,
		userId: null,
		bankName: null,
    bankAccount: null,
    mobile: null,
    idCard: null,
		weismbuhaoshi:'none'
	},
	onLoad: function (options) {
		var info = options.info
		if (info){
			this.setData({ bank: JSON.parse(info), weismbuhaoshi:'block' })
		}else{
      this.setData({ userId: app.loginUser.id, weismbuhaoshi: 'block', 
        bankAccount: app.loginUser.name, 
        mobile: app.loginUser.mobile, 
        idCard: app.loginUser.idNo
      })
		}
		app.checkLogin()
	},
	queryBankName: function(e){
		var no = e.detail.value
		if(no){
			//接口地址没域名，先注释掉
			doQueryBankName(no, this);
		}
	},
  idcardInput: function (e) {
    this.setData({
      idCard: e.detail.value
    })
  },
  bankAccountInput: function (e) {
    this.setData({
      bankAccount: e.detail.value
    })
  },
  mobileInput: function (e) {
    this.setData({
      mobile: e.detail.value
    })
  },
	formSubmit: function (e) {
		var info = e.detail.value;
    var that = this;
    info.bankAccount = that.data.bankAccount;
    info.mobile = that.data.mobile;
    info.idCard = that.data.idCard;
    console.info("身份证号");
    console.info(info.idCard);
    console.info(info);
		if (!info.bankAccount) {
			app.tip('请输入开户人', 'loading')
			return;
		}
		if (!info.mobile) {
			app.tip('请输入手机号', 'loading')
			return;
		}
		if (!util.checkMobile(info.mobile)){
			app.tip("手机号有误", 'loading')
			return;
		}
		if (!info.idCard) {
			app.tip('请输入身份证号', 'loading')
			return;
		}
		if (!util.checkIdNo(info.idCard)) {
			app.tip("身份证号有误", 'loading')
			return;
		}
		if (!info.bankCardNo) {
			app.tip('请输入银行卡号', 'loading')
			return;
		}
    if (!info.bankName) {
      app.tip('请填写正确的银行信息', 'none')
      return;
    }
		if (!validate){
			app.tip('银行卡号有误', 'loading')
			return;
		}
		doSubmit(info)
	}
})
function doSubmit(info) {
	wx.showLoading({
    title: '加载中'
	})
	wx.request({
		url: config.bankAddUrl,
		method: "POST",
		header: {
			"Content-Type": "application/x-www-form-urlencoded"
		},
		data: { data: JSON.stringify(info) },
		success: (res) => {
			wx.hideLoading()
			var data = res.data;
			if (data && data.code && data.code == 100) {
				app.tip('操作成功', 'success')
        wx.navigateTo({ url: "../banks/banks" });
			} else {
				var errMsg;
				if (data && data.msg) {
					errMsg = data.msg
				} else {
					errMsg = res.statusCode + ":" + res.errMsg
				}
				app.tip(errMsg, 'none')
			}
		},
		fail: function ({ errMsg }) {
			wx.hideLoading()
			app.tip(errMsg, 'none')
		}
	})
}
/**
 * 验证银行卡号并获取银行名称
 */
function doQueryBankName(bankCardNumber, _this){
	wx.request({
    url: app.sysInfo.getBankNameUrl + bankCardNumber,
		method: "POST",
		header: {
			"Content-Type": "application/x-www-form-urlencoded"
		},
		success: (res) => {
			console.log(res)
			var data = res.data;
			/*
			bank: "HRBANK", validated: true, cardType: "DC", key: "6217524514000042458", messages[],stat:"ok"
			*/
			var isFail = false
			var failMsg = ''
			if (data && data.bank) {
				var name = bankName.getName(data.bank)
				if (name) {
					_this.setData({ bankName: name })
					validate = data.validated
				} else {
					isFail = true
					failMsg = '获取银行名称失败'
				}
			}else{
				if (data.messages && data.messages.length>0){
					failMsg = data.messages[0].errorCodes
				}else{
					failMsg = res.errMsg
				}
			} 
			if (isFail){
				if (failMsg){
					app.tip(failMsg, 'loading')
				}
				_this.setData({ bankName: "" })
				validate = false
			}
		},
		fail: function ({ errMsg }) {
			validate = false
			_this.setData({ bankName: "" })
			app.tip(errMsg, 'loading')
		}
	})
}