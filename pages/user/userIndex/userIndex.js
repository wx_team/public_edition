const app = getApp()
var config = require('../../../config.js');

Page({
  data: {
    userInfo: null,
    loginUser: null,
    auditStatus: "",
    hide: false,
    show: true,
    showFileUrl: config.showFileUrl
  },
  /**
    * 生命周期函数--监听页面显示
    */
  onShow: function () {
    var that = this
    //重新获取用户信息
    if (app.loginUser != null && app.loginUser.id != null && app.loginUser.mobile != null) {
      
      wx.showLoading({
        title: '加载中',
        mask: true
      })

      wx.request({
        url: config.requestUrl + "/user/getVipInfo",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          "id": app.loginUser.id
        },
        success: function (res) {
          wx.hideLoading()
          //console.info("根据用户ID获取当前登录用户信息");
          //console.info(res);
          if (res.data.code == "100") {
            var vipUser = res.data.content;

            //重新设置登录用户信息
            app.setLoginUser(vipUser);

            var auditStatusName = "";
            var auditStatus = vipUser.auditStatus;
            if (auditStatus == "0") {
              auditStatusName = "待审核";
            } else if (auditStatus == "1") {
              auditStatusName = "已认证";
            } else if (auditStatus == "2") {
              auditStatusName = "已驳回";
            }
            that.setData({
              userInfo: app.userInfo,
              loginUser: vipUser,
              auditStatus: auditStatusName
            })
          }
        },
        fail: function ({ errMsg }) {
          app.tip(errMsg, "none");
        }
      })
    } else {
      //判断是否已经登录过:未登录显示登录和注册，已登录显示退出登录和返回首页
      that.setData({
        hide: true,
        show: false
      })
    }

  },
  onLoad: function (options) {
    //app.checkLogin()
    // wx.setNavigationBarColor({
    //   frontColor: '#ffffff',
    //   backgroundColor: '#107de3'
    // })
  },
  toEdit: function (e) {
    if (checkLoginUser()) {
      let pageType = e.currentTarget.dataset.type;
      wx.navigateTo({ url: pageType })
    }
  },
  toIdentify: function (e) {
    app.tip("即将推出，敬请期待!","none");
    return false;
    if (checkLoginUser()) {
      let auditStatus = e.currentTarget.dataset.type;
      if (auditStatus == "0") {
        app.tip("实名认证已提交，请等待审核", "none");
        return false;
      }
      wx.navigateTo({ url: '../identify/identify' })
    }
  },
  //登录－个人中心(授权获取微信信息)
  showModal(e) {
    if (app.loginUser == null || app.loginUser.id == null || app.loginUser.id == '') {
      wx.navigateTo({
        url: '../../authorizedLogin/authorizedLogin',
      })
    }else{
      var userPhone = wx.getStorageSync("phoneNumber");
      var that = this;
      if (userPhone == "" || userPhone == null || userPhone == undefined) {
        app.tip("授权之后才可以登录", "none");
        return false;
      }

    }
  },
  //登陆物流企业中心
  toLogisticsEnterpriseCenter: function (e) {
    wx.navigateTo({ url: '../../logisticsLogin/logisticsLogin' })
  },

  //返回首页
  // toIndex: function (e) {
  //   wx.switchTab({
  //     url: '../../index/index',
  //   })
  // },
  //退出登录
  toLogout: function (e) {
    var _this = this;
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: config.requestUrl + "/login/logout",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        "openId": app.openid,
        "vipMemberId": _this.data.loginUser.id
      },
      success: function (res) {
        wx.hideLoading()
        console.info("退出登录返回结果");
        console.info(res);
        if (res.data.code == "100") {
          console.info("进入赋值");
          app.loginUser = null;
          app.setLoginUser(null);

        }
        // wx.switchTab({
        //   url: '../../index/index',
        //   success: function (e) {
        //     var page = getCurrentPages().pop();
        //     page.onLoad();
        //   }
        // })
        wx.reLaunch({
          url: '../../index/index',
        })
      },
      fail: function ({ errMsg }) {
        wx.navigateTo({ url: "../../authorizedLogin/authorizedLogin" })
      }
    })
  }
})

function checkLoginUser(){
  if (app.loginUser != null && app.loginUser.id != null && app.loginUser.mobile != null) {
    return true;
  } else {
    wx.showModal({
      title: '提示',
      content: '用户未登录',
      success: function (res) {
        if (res.confirm) {
          wx.navigateTo({
            url: '/pages/authorizedLogin/authorizedLogin'
          })
          return false;
        } else if (res.cancel) {
          return false;
        }
      }
    })
  }
}