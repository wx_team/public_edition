const app = getApp(); 
const config = require('../../../config.js');
Page({
  data: {
    pageSize :10
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    this.data.cardId = options.cardId;
    this.data.kpstart = getDate(-7);
    this.data.kpend = getDate();
    this.data.ffstart = getDate(-7);
    this.data.ffend = getDate();
    this.setData({ kpstart: this.data.kpstart, kpend: this.data.kpend, ffstart: this.data.ffstart, ffend: this.data.ffend, cardId: options.cardId, phone: options.phone});
    getList(this);
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.data.pageSize = 10;
    getList(this);
    wx.stopPullDownRefresh();
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.pageSize = this.data.pageSize + 10;
    getList(this);
  },
  //  点击时间组件确定事件  
  bindTimeChange: function (e) {
    // console.log("谁哦按")
    this.setData({
      times: e.detail.value
    })
  },
  companyChange: function (e) {
    //文本框的值
    var name = app.trim(e.detail.value);
    var that = this;
    that.setData({
      companyInputValue: name,
    })
    that.doSearch();
  },
  doSearch: function (e) {
    var that = this;
    //文本框的值
    console.info("条件公司名称");
    console.info(that.data.companyInputValue);
    var name = (that.data.companyInputValue == undefined || that.data.companyInputValue == null) ? "" : that.data.companyInputValue;
    var arrayObj = new Array();
    var arr = that.data.allCompanyInfoList;
    console.info(arr);
    if (name.length > 0) {
      for (var i = 0; i < arr.length; i++) {
        console.info(arr[i].name);
        if (arr[i].gsChinese.indexOf(name) >= 0) {
          arrayObj.push(arr[i]);
        }
      }
    } else {
      arrayObj = that.data.allCompanyInfoList;
    }

    that.setData({
      wuliu: arrayObj
    })
  }, 
  //  点击日期组件确定事件  
  bindDateChange: function (e) {
    let temptype = e.target.dataset.t;
    let temidate = e.detail.value;
    if ("ks" == temptype){
      this.data.kpstart = temidate;
    } else if ("ke" == temptype){
      this.data.kpend = temidate;
    } else if ("fs" == temptype) {
      this.data.ffstart = temidate;
    } else if ("fe" == temptype) {
      this.data.ffend = temidate;
    }
    this.setData(this.data);
    getList(this)
  }
});

/**
 * 获取数据
 */
function getList(_this) {
  // console.log(_this.data);
  searchList(_this,function(msg){
    //console.log(msg.data.content.list);
    if (Object.prototype.toString.call(msg.data.resultObject) === "[object Array]" && msg.data.resultObject.length > 0){
      for (let i = 0; i < msg.data.resultObject.length ; i++){
        msg.data.resultObject[i].kpDate = msg.data.resultObject[i].kpDate.replace('T', ' ');
        msg.data.resultObject[i].fkDate = msg.data.resultObject[i].fkDate.replace('T', ' ');
      }

      console.info("条件公司名称");
      console.info(_this.data.companyInputValue);
      var name = (_this.data.companyInputValue == undefined || _this.data.companyInputValue == null) ? "" : _this.data.companyInputValue;
      var arrayObj = new Array();
      var arr = msg.data.resultObject;
      console.info(arr);
      if (name.length > 0) {
        for (var i = 0; i < arr.length; i++) {
          console.info(arr[i].name);
          if (arr[i].gsChinese.indexOf(name) >= 0) {
            arrayObj.push(arr[i]);
          }
        }

        _this.setData({
          wuliu: arrayObj,
          allCompanyInfoList: msg.data.resultObject,
          showView: true,
          cardId: _this.data.cardId
        });

      } else {
        _this.setData({
          wuliu: msg.data.resultObject,
          allCompanyInfoList: msg.data.resultObject,
          showView: true,
          cardId: _this.data.cardId
        });
      }

    }else{
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
    }
    
    
  });
}

/**
 * 请求数据
 */

function searchList(_this,callback){
  if (_this.data.kpstart > _this.data.kpend) {
    app.tip("开始时间不能晚于结束时间", "none");
    return false;
  }
  wx.showLoading({ mask: true, title: "加载中" });
  let par = {
    page:1,
    rows: _this.data.pageSize,
    oneCardNo: _this.data.cardId,
    phone: _this.data.phone,
    startDate: _this.data.ffstart,
    endDate: _this.data.ffend,
    //currentPage: 1,
    //pageSize: _this.data.pageSize,
    // vipNo: _this.data.cardId,
    // code: config.tenantCode,
    // orderStartDate: _this.data.kpstart,
    // orderEndDate: _this.data.kpend,
    // exposureStartDate: _this.data.ffstart,
    // exposureEndDate: _this.data.ffend
  };
  // console.log(par);
  wx.request({
    url: config.requestUrl + "vipCard/getVipCardDetails",
    method: "POST",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: par,
    success: function (msg) {
      console.info("一卡通详细返回结果");
      console.info(msg);
      callback(msg);
      wx.hideLoading();
    },
    fail: function (msg) {
      // console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}

/**
 * 获取当前日期
 */
var getDate = function (num) {
  let tempDate = new Date();
  if (num != undefined && "" != num) {
    tempDate.setDate(tempDate.getDate() + num);//获取AddDayCount天后的日期 
  }
  let nowDate = new Date(Date.parse(tempDate));
  //年  
  let Y = nowDate.getFullYear();
  //月  
  let M = (nowDate.getMonth() + 1 < 10 ? '0' + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1);
  //日  
  let D = nowDate.getDate() < 10 ? '0' + nowDate.getDate() : nowDate.getDate();
  return Y + "-" + M + "-" + D;
};
