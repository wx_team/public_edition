const app = getApp();
const config = require('../../../config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.loginUser == null || app.loginUser.id == null && app.loginUser.mobile == null){
      wx.navigateTo({
        url: '/pages/authorizedLogin/authorizedLogin'
      })
      return;
    }
    if (options.fromPage == 'userindex'){
      this.setData({ fromPage: "userindex" });
    }
    getList(this, app.loginUser.mobile);
  },
  /**
   * 进入详情页
   */
  goDetail : function(e){
    // console.log(e.currentTarget.dataset);
    // console.log(this.data.vipCardArr);
    let _this = this;
    let temparr = new Array();
    if (e.currentTarget.dataset.frompage == 'userindex'){
      if (Array.isArray(_this.data.vipCardArr) && _this.data.vipCardArr.length > 0){
        for (let i = 0; i < _this.data.vipCardArr.length ; i++) {
          if (_this.data.vipCardArr[i].vipNo == e.currentTarget.dataset.cardid){
            temparr = _this.data.vipCardArr[i].list;
            break;
          }
        }
      }
      wx.navigateTo({
        url: 'vipCardBankDetail?vipCardArr=' + JSON.stringify(temparr)
      })
    }else{
      wx.navigateTo({
        url: 'vipCardDetail?cardId=' + e.currentTarget.dataset.cardid + '&phone=' + e.currentTarget.dataset.phone
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    getList(this, app.loginUser.mobile);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
  // ,

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // }
});

/**
 * 查询一卡通信息
 */
function getList(_this, mobile) {

  searchList(_this,mobile, function (msg) {
    console.log("一卡通返回结果");
    console.log(msg);
    console.log(msg.data.resultObject);
    let arr = new Array();
    if (Object.prototype.toString.call(msg.data.resultObject) === "[object Array]" && msg.data.resultObject.length > 0) {
      _this.data.vipCardArr = msg.data.resultObject;
      for (let i = 0; i < msg.data.resultObject.length; i++) {
        arr.push({ vipNo: msg.data.resultObject[i].cardCode, phoneNumber: msg.data.resultObject[i].phoneNumber});
      }
      _this.setData({ showView: true, wuliu: arr });
      // _this.setData({ showView: true, wuliu: msg.data.content });
    }else{
      _this.setData({
        showView: false,
        message: "抱歉，您还没有会员卡呢！",
        message1: "快到物流公司办理吧！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/banka.png?sign=395db97191c465864a45990acfd9dabd&t=1570583848"
      });
    }
  });
}

/**
 * 获取一卡通信息
 */
function searchList(_this,mobile, callback) {
  wx.showLoading({ mask: true, title: "加载中" });
  wx.request({
    // url: config.requestUrl + "order/vipCardInfo",
    //url: config.requestUrl + "vipCard/getVipCardList", 
    url: config.requestUrl + "vipCard/getVipCardInfo", 
    method: "POST",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: { "phoneNumber": mobile },
    success: function (msg) {
      callback(msg);
      wx.hideLoading();
    },
    fail: function (msg) {
      // console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}