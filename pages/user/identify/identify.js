const app = getApp()
const config = require('../../../config.js');
const editUser = require('../../common/js/editUser.js')


Page({
  data: {
    loginUser: null,
    //身份证正面照
    idFrontPicSrc: null,
    idFrontPic: null,
    show1: false,
    //身份证反面照
    idBackPicSrc: null,
    idBackPic: null,
    show2: false,
    //手持身份证照
    idHandPicSrc: null,
    idHandPic: null,
    show3: false,
    edit: true,
    defaultHeight: 1298,
    coverHeight: 1298,
    clickNum : 0,
    isCover : "",
    chooseType : ""
  },

  onLoad: function (options) {
    app.checkLogin();
    console.info("实名制表示");
    console.info(app.loginUser);
    if (app.loginUser.auditStatus != null && app.loginUser.auditStatus != 2) {
      this.setData({
        edit: false
      })
    }
    if (app.loginUser.auditStatus == 2){
      this.data.clickNum = 3;
      this.setData({
        loginUser: app.loginUser,
        idFrontPicSrc: app.loginUser.idFrontPic,
        idBackPicSrc: app.loginUser.idBackPic,
        idHandPicSrc: app.loginUser.idHandPic,
        idFrontPic: app.loginUser.idFrontPic,
        idBackPic: app.loginUser.idBackPic,
        idHandPic: app.loginUser.idHandPic,
        show1: true,
        show2: true,
        show3: true
      })
    }else{
      this.setData({
        loginUser: app.loginUser
      })
    }
    
  },
  imgYu: function (event) {
    var src = event.currentTarget.dataset.current;//获取data-src
    console.info("点击图片");
    console.info(src);
    var imgList = [];
    imgList[0] = src
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList
    })
  },
  chooseImage1: function () {
    chooseImage(this, 1);
  },
  chooseImage2: function () {
    chooseImage(this, 2);
  },
  chooseImage3: function () {
    chooseImage(this, 3);
  },
  identifySubmit: function (e) {
    var info = e.detail.value;
    if (!info.name) {
      app.tip('请输入真实姓名', 'none')
      return;
    }
    if (!info.idNo) {
      app.tip('请输入身份证号', 'none')
      return;
    }
    var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    if (!regIdNo.test(info.idNo)) {
      app.tip("身份证号有误", 'none')
      return false;
    }
    if (!this.data.idFrontPicSrc) {
      app.tip('请选择身份证正面照片', 'none')
      return;
    }
    if (!this.data.idBackPicSrc) {
      app.tip('请选择身份证反面照片', 'none')
      return;
    }
    if (!this.data.idHandPicSrc) {
      app.tip('请选择手持身份证照', 'none')
      return;
    }
    this.doSubmit(info)
  },
  doSubmit: function (info) {
    var that = this
    // if (!this.data.idFrontPic) {
    //   setTimeout(function () {
    //     that.doSubmit(info)
    //   }, 1000)
    // }
    // if (!this.data.idBackPic) {
    //   setTimeout(function () {
        
    //     that.doSubmit(info)
    //   }, 1000)
    // }
    // if (!this.data.idHandPic) {
    //   setTimeout(function () {
    //     that.doSubmit(info)
    //   }, 1000)
    // }
    info.id = that.data.loginUser.id
    info.idFrontPic = that.data.idFrontPic
    info.idBackPic = that.data.idBackPic
    info.idHandPic = that.data.idHandPic
    editUser.doEdit(config.modifyUserUrl, info, "../userIndex/userIndex")
  },

  /** 
	 * 预览图片
	 */

  previewImage: function (event) {
    var src = event.currentTarget.dataset.current;//获取data-src
    var imgList = [];
    imgList[0] = src
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList
    })
  }
})

function chooseImage(that, index) {
  wx.chooseImage({
    count: 1,
    success: function (res) {

      if (that.data.chooseType.indexOf("," + index + ",") == -1) {
        that.data.chooseType += "," + index + ",";
        if (that.data.clickNum < 3) {
          that.data.clickNum++;
        }
      }
      if (that.data.clickNum == 1) {
        that.data.coverHeight = that.data.defaultHeight;
      }
      if (that.data.clickNum == 2) {
        that.data.coverHeight = that.data.defaultHeight + 490;
      }
      if (that.data.clickNum == 3){
        that.data.coverHeight = that.data.defaultHeight + 980;
      }
      
      if (index == 1) {
        that.setData({
          idFrontPicSrc: res.tempFilePaths[0],
          idFrontPic: "",
          show1: true
        })
        uploadImage(res.tempFilePaths[0], 1, that)

      } else if (index == 2) {
        that.setData({
          idBackPicSrc: res.tempFilePaths[0],
          idBackPic: "",
          show2: true
        })
        uploadImage(res.tempFilePaths[0], 2, that)

      } else if (index == 3) {
        that.setData({
          idHandPicSrc: res.tempFilePaths[0],
          idHandPic: "",
          show3: true
        })
        uploadImage(res.tempFilePaths[0], 3, that)
      }
    }
  })
}

function uploadImage(filePath, index, that) {
  that.setData({ isCover: "block", coverHeight: that.data.coverHeight + "rpx"});
  wx.showLoading({ mask: true, title: "上传中" });
  wx.uploadFile({
    url: app.sysInfo.uploadFileUrl,
    filePath: filePath,
    name: 'file',
    header: {
      "Content-Type":
      "multipart/form-data"
    },
    success: function (res) {

      if (index == 1) {
        that.setData({
          idFrontPic: "http://upload.feiyang56.cn:40218/fileuploader/" + JSON.parse(res.data).url
        })

      } else if (index == 2) {
        that.setData({
          idBackPic: "http://upload.feiyang56.cn:40218/fileuploader/" + JSON.parse(res.data).url
        })

      } else if (index == 3) {
        that.setData({
          idHandPic: "http://upload.feiyang56.cn:40218/fileuploader/" + JSON.parse(res.data).url
        })
      }
      that.setData({ isCover: "none" });
      wx.hideLoading();
    }
  })
  
  
}