var app = getApp()
const config = require('../../../config.js');
const editUser = require('../../common/js/editUser.js')

Page({

  data: {
    userInfo: null,
    loginUser: null,
    companyList: [],
    index: 0,
    provinceList: [],
    provinceIndex: 0,
    cityList: [],
    cityIndex: 0
  },

  onLoad: function (options) {
    app.checkLogin()
    this.setData({ userInfo: app.userInfo, loginUser: app.loginUser })
    console.info("初始化");
    console.info(app.loginUser);
    getProvinceList(this);
  },
  changeProvince: function (e) {
    //e.detail.value中的值是数组的下标
    this.setData({
      provinceIndex: e.detail.value,
      province: this.data.provinceList[e.detail.value].administrativeDivisionName
    })
    getCityList(this.data.provinceList[e.detail.value].administrativeDivisionId, this);
  },
  changeCity: function (e) {
    //e.detail.value中的值是数组的下标
    this.setData({
      cityIndex: e.detail.value,
      city: this.data.cityList[e.detail.value].administrativeDivisionName
    })
  },
  userInfoSubmit: function (e) {
    var info = e.detail.value;

    if (info.idNo != undefined && info.idNo != null && info.idNo != "") {
      var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
      if (!regIdNo.test(info.idNo)) {
        app.tip("身份证号有误", 'loading')
        return false;
      }
    }

    if (!info.name) {
      app.tip("请输入真实姓名", 'loading')
    }
		/*
		else if (!info.province) {
			app.tip("请输入所在省", 'loading')
		} else if (!info.city) {
			app.tip("请输入所在市", 'loading')
		} 
		
		else if (!info.address) {
			app.tip("请输入详细地址", 'loading')

		}*/
    else {
      info.id = app.loginUser.id
      console.info("省市信息");
      //console.info(this.data.provinceList);
      //console.info(this.data.administrativeDivisionCode);
      //console.info(this.data.areaCode);
      //console.info(this.data.provinceList[this.data.provinceIndex].administrativeDivisionCode);
      info.province = this.data.province
      info.city = this.data.city
      info.areaCode = this.data.areaCode
      editUser.doEdit(config.modifyUserUrl, info, "../userIndex/userIndex")
    }
  }
})

//省选择器的数据加载
function getProvinceList(that) {
  console.info("userid");
  console.info(that.data.loginUser.id);
  console.info(app.loginUser);
  console.info("省信息");
  wx.showLoading({
    title: '加载中',
    mask: true
  })
  wx.request({
    url: config.requestUrl + "administrativeDivision/getProvinceList",
    method: "POST",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.hideLoading()

      var data = res.data;
      if (data && data.code && data.code == 100) {
        var provinceLists = data.content;
        //console.log(provinceLists)
        if (app.loginUser.province != null && app.loginUser.province != "" && that.data.province == undefined) {
          that.setData({ provinceList: provinceLists, province: app.loginUser.province })
        } else {
          that.setData({ provinceList: provinceLists, province: provinceLists[0].administrativeDivisionName })
        }
        getCityList(provinceLists[0].administrativeDivisionId, that);
      } else {
        var errMsg;
        if (data && data.msg) {
          errMsg = data.msg
        } else {
          errMsg = res.statusCode + ":" + res.errMsg
        }
        app.tip(errMsg, 'none')
      }
    },
    fail: function ({ errMsg }) {
      wx.hideLoading()
      app.tip(errMsg, 'none')
    }
  })
}

//市选择器的数据加载
function getCityList(provinceId, that) {
  console.info("市信息");
  wx.showLoading({
    title: '加载中',
    mask: true
  })
  wx.request({
    url: config.requestUrl + "administrativeDivision/getCityList",
    method: "POST",
    data: {
      "provinceId": provinceId
    },
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.hideLoading()

      var data = res.data;
      if (data && data.code && data.code == 100) {
        var cityLists = data.content;
        console.log(that.data.city)
        console.log(app.loginUser)
        if (app.loginUser.city != null && app.loginUser.city != "" && that.data.city == undefined) {
          that.setData({ cityList: cityLists, city: app.loginUser.city, areaCode: app.loginUser.areaCode })
        } else {
          console.info("初始化市的值");
          console.info(cityLists[0].administrativeDivisionCode);
          that.setData({ cityList: cityLists, city: cityLists[0].administrativeDivisionName, areaCode: cityLists[0].administrativeDivisionCode })
        }

      } else {
        var errMsg;
        if (data && data.msg) {
          errMsg = data.msg
        } else {
          errMsg = res.statusCode + ":" + res.errMsg
        }
        app.tip(errMsg, 'none')
      }
    },
    fail: function ({ errMsg }) {
      wx.hideLoading()
      app.tip(errMsg, 'none')
    }
  })
}