//适用于用户未登录点忘记密码
//所以openid一定是空的
var config = require('../../../config.js');
var getCaptcha = require('../../common/js/getCaptcha.js');
var app = getApp()
var maxTime = 120
var currentTime = maxTime //倒计时的事件（单位：s）  
var step_g = 1
var interval = null
var hintMsg = null
var phoneNum = null
var identifyCode = null
var password = null
var rePassword = null
var answer = null

Page({
	data: {
		step: step_g,
		time: currentTime,
		name: null
	},
	onLoad: function (options) {
		this.reset();
		if (options && options.mobile) {
			phoneNum = options.mobile
			this.setData({ name: phoneNum })
		}
	},
	nextStep: function (e) {
		var info = e.detail.value;
		if (step_g == 1) {
			phoneNum = info.input_phoneNum
			firstStep(this)

		} else if (step_g == 2) {
			identifyCode = info.input_identifyCode
			if (secondStep()) {
				step_g = 3
				clearInterval(interval)
			}
		} else {
			password = info.input_password
			rePassword = info.input_rePassword
			thirdStep()
		}
		this.setData({
			step: step_g
		})
		if (hintMsg != null) {
			app.tip(hintMsg, 'loading')
		}
	},
	reSendPhoneNum: function () {
		if (currentTime < 0) {
			var that = this
			currentTime = maxTime
			interval = setInterval(function () {
				currentTime--
				that.setData({
					time: currentTime
				})
				if (currentTime <= 0) {
					currentTime = -1
					clearInterval(interval)
				}
			}, 1000)

			answer = null
			firstStep()

		} else {
			app.tip('短信已发到您的手机，请稍后重试', 'none')
		}
	},
	captchaCallback: function (obj) {
		step_g = 2
		answer = obj.message
		this.setData({
			step: step_g
		})
		var that = this
		interval = setInterval(function () {
			currentTime--;
			that.setData({
				time: currentTime
			})
			if (currentTime <= 0) {
				clearInterval(interval)
				currentTime = -1
			}
		}, 1000)
	},
	reset: function(){
		console.log("调用reset")
		maxTime = 120
		currentTime = maxTime
		step_g = 1
		interval = null
		hintMsg = null
		phoneNum = null
		identifyCode = null
		password = null
		rePassword = null
		answer = null
		this.setData({
			step: step_g,
			time: currentTime,
			name: null
		})
	}
})

// 提交电话号码，获取［验证码］  
function firstStep(that) {
	if (phoneNum == null || phoneNum == '') {
		hintMsg = "请输入电话号码!"
		return false
	} else {
    var regMobile = /^[1][3,4,5,6,7,8][0-9]{9}$/
    if (!regMobile.test(phoneNum)) {
      app.tip("手机号码格式不正确", "none");
      return false;
    }
  }
	/*//测试数据
	var obj = new Object();
	obj.message = '4455'
	that.captchaCallback(obj);
	*/
  getCaptcha.doRequest(that, phoneNum, 'findPass')
}

// 验证［验证码］ 
function secondStep() {
	if (identifyCode == null || identifyCode == '') {
		hintMsg = "请输入验证码!"
		return false
	}
	return submitIdentifyCode(identifyCode)
}

// 提交［密码］和［重新密码］  
function thirdStep() {
	if (password == null || password == '') {
		hintMsg = "请输入密码!"
		return false
	}
	if (rePassword == null || rePassword == '') {
		hintMsg = "请输入确认密码!"
		return false
	}
	if (rePassword != password) {
		hintMsg = "两次密码不一致！"
		return false
	}
	submitPassword(password)
}

//验证［验证码］  
function submitIdentifyCode(identifyCode) {
	console.log("answer:"+answer+" identifyCode:"+identifyCode);
	// 此处调用wx中的网络请求的API，完成短信验证码的提交 
	if (answer != null && answer == identifyCode){
		return true
	}else{
		hintMsg = "验证码错误!"
		return false
	}
}

function submitPassword(password) {
	//此处调用wx中的网络请求的API，完成密码的提交  
	// 修改完成
	var obj = new Object()
	obj.mobile = phoneNum
	obj.password = password
	
	wx.showLoading({
    title: '加载中',
    mask: true
	})
	wx.request({
		url: config.forgetPassUrl,
		method: "POST",
		header: {
			"Content-Type": "application/x-www-form-urlencoded"
		},
		data: {data: JSON.stringify(obj)},
		success: function (res) {
			wx.hideLoading()
			console.log(res.data)
			var data = res.data;
			if (data && data.code && data.code == 100) {
				app.tip("操作成功", 'success')
        wx.navigateTo({
          url: "../../authorizedLogin/authorizedLogin?mobile=" + phoneNum,
        })
			} else {
				var errMsg;
				if (data && data.msg) {
					errMsg = data.msg
				} else {
					errMsg = res.statusCode + ":" + res.errMsg
				}
				app.tip(errMsg, 'none')
			}
		},
		fail: function ({ errMsg }) {
			wx.hideLoading()
			console.log({ errMsg })
			app.tip(errMsg, 'none')
		}
	})
}