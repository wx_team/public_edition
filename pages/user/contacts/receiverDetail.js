const app = getApp();
const config = require('../../../config.js');
const util = require('../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isDefault: 0,
    jjr : "",
    sjh: "",
    dz: ""
  },
  bindPickerChange: function (e) {
    this.setData({
      areaIndex: e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ isDefault_c: "wu", isDefault_d: "sos" });
    let temptitle = "";
    if ("addReceiver" == options.title) {
      temptitle = "新建收货人";
    } else if ("editReceiver" == options.title) {
      temptitle = "编辑收货人";
    }
    wx.setNavigationBarTitle({ title: temptitle });
    if (null != options.receiver && "" != options.receiver) {
      let receiver = JSON.parse(options.receiver);
      if (receiver != null && receiver != "") {
        if (receiver.id != null && receiver.id != "") {
          this.data.id = receiver.id;
          this.data.jjr = receiver.contactName;
          this.data.sjh = receiver.contactMobile;
          this.data.dz = receiver.contactAddress;
          this.data.isDefault = receiver.isDefault;
        }
        // console.log(receiver);
        if (receiver.isDefault == 1) {
          this.setData({ user: receiver, isDefault_c: "wu_h", isDefault_d: "sos_h" });
        } else if (receiver.isDefault == 0) {
          this.setData({ user: receiver, isDefault_c: "wu", isDefault_d: "sos" });
        }
      }
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // },
  /**
   * 提交保存信息
   */
  save: function () {
    let vipMemberId = app.loginUser.id;
    // console.log(this.data);
    if (checkNull(this.data.jjr)) {
      app.tip('请填写收货人', 'none')
      return;
    } else {
      if (!app.inputZhengZe(this.data.jjr)) {
        app.tip("请删除收货人中的特殊符号", "none");
        return false;
      }
    }
    if (checkNull(this.data.sjh)) {
      app.tip('请填写手机号', 'none')
      return;
    }
    if (!util.checkMobile(this.data.sjh)) {
      app.tip('请填写正确手机号', 'none')
      return;
    }
    // if (checkNull(this.data.dz)) {
    //   app.tip('请填写地址', 'none')
    //   return;
    // }
    wx.showLoading({ mask: true, title: "加载中" });
    let par = { "data": '{"vipMemberId":"' + app.loginUser.id + '","contactName":"' + this.data.jjr + '","contactMobile":"' + this.data.sjh + '","contactAddress":"' + this.data.dz + '","isDefault":"' + this.data.isDefault + '"}' };
    // console.log(par);
    let url = config.requestUrl + "receiverContactInfo/add";
    if (this.data.id != null && this.data.id != "") {
      par = { "data": '{"vipMemberId":"' + app.loginUser.id + '","contactName":"' + this.data.jjr + '","contactMobile":"' + this.data.sjh + '","contactAddress":"' + this.data.dz + '","isDefault":"' + this.data.isDefault + '","id":"' + this.data.id + '"}' };
      url = config.requestUrl + "receiverContactInfo/edit";
    }
    // console.log(url);
    wx.request({
      url: url,
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
      },
      data: par,
      success: function (msg) {
        // console.log(msg);
        wx.hideLoading();
        if (msg.data.code == 100) {
          // wx.redirectTo({
          //   url: '../contacts/contacts?title=navigate&type=receiver'
          // })
          wx.navigateBack({
            delta: 1
          })
        } else {
          app.tip(msg.data.msg == null ? "新建收货人失败" : msg.data.msg, 'none');
        }
      },
      fail: function (msg) {
        // console.log(msg);
        app.tip("新建收货人失败", 'none');
        wx.hideLoading();
      }
    });
  },
  /**
   * 取消
   */
  goBack: function () {
    // wx.redirectTo({
    //   url: '../contacts/contacts?title=navigate&type=receiver'
    // })
    wx.navigateBack({
      delta: 1
    })
  },
  /**
   * 默认设置
   */
  changeDefault: function (e) {
    // console.log(e.target.dataset.c);
    // console.log(e.target.dataset.d);
    if (e.target.dataset.c == "wu" || e.target.dataset.d == "sos") {
      this.setData({ isDefault_c: "wu_h", isDefault_d: "sos_h" });
      this.data.isDefault = 1;
    } else if (e.target.dataset.c == "wu_h" || e.target.dataset.d == "sos_h") {
      this.setData({ isDefault_c: "wu", isDefault_d: "sos" });
      this.data.isDefault = 0;
    }
  },
  /**
   * 收货人
   */
  jjr: function (e) {
    if (undefined != e.detail.value){
      this.data.jjr = e.detail.value;
    }else{
      this.data.jjr = "";
    }
    
  },
  /**
   * 手机号
   */
  sjh: function (e) {
    if (undefined != e.detail.value) {
      this.data.sjh = e.detail.value;
    } else {
      this.data.sjh = "";
    }
    // console.log(this.data.sjh);
  },
  /**
   * 地址
   */
  dz: function (e) {
    if (undefined != e.detail.value) {
      this.data.dz = e.detail.value;
    } else {
      this.data.dz = "";
    }
    
  }
});
/**
 * 验证空
 */
function checkNull(str) {
  if (null == str || "" == str || undefined == str) {
    return true;
  } else {
    return false;
  }
}