const app = getApp();
const config = require('../../../config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    senderarr:[],
    type : "sender",
    addtype:"发货人"
  },
  opsnt: function (e) {
    let url = 'senderDetail?title=addSender';
    if(this.data.type == "receiver"){
      url = 'receiverDetail?title=addReceiver';
    }
    wx.navigateTo({
      url: url
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.loginUser == null || app.loginUser.id == null && app.loginUser.mobile == null) {
      wx.navigateTo({
        url: '/pages/authorizedLogin/authorizedLogin'
      })
      return;
    }
    if (options.type == "receiver" || this.data.type == "receiver") {
      this.data.type = "receiver";
      this.setData({ "tab_jjr": "nav_1_1", "tab_sjr": "nav_1_1_h", "addtype": "收货人","type":"receiver" });
    }else{
      this.data.type = "sender";
      this.setData({ "tab_jjr": "nav_1_1_h", "tab_sjr": "nav_1_1", "addtype": "发货人", "type": "sender"});
      
    }
    // getList(this, app.loginUser.id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  deletec: function (e) {
    let _this = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          // 用户点击了确定 可以调用删除方法了
          // console.log(e.currentTarget.dataset.id);
          wx.showLoading({ mask: true, title: "加载中" });
          let url = config.requestUrl + "senderContactInfo/delete";
          if (_this.data.type == "receiver") {
            url = config.requestUrl + "receiverContactInfo/delete";
          }
          wx.request({
            url: url,
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
            },
            data: { "id": e.currentTarget.dataset.id },
            success: function (msg) {
              // console.log(msg);
              wx.hideLoading();
              if (msg.data.code == 100) {
                getList(_this, app.loginUser.id);
                app.tip("删除成功", 'none');
              } else {
                app.tip("删除失败", 'none');
              }
            },
            fail: function (msg) {
              // console.log(msg);
              app.tip("删除失败", 'none');
              wx.hideLoading();
            }
          });
        } else if (sm.cancel) {
          // console.log('用户点击取消')
        }
      }
    })
  },
  editc: function (e) {
    // console.log(this.data.senderarr[e.currentTarget.dataset.index]);
    let url = "senderDetail?title=editSender&sender=";
    let tempobj = this.data.senderarr[e.currentTarget.dataset.index];
    if (this.data.type == "receiver") {
      url = 'receiverDetail?title=editReceiver&receiver=';
      tempobj = this.data.receiverarr[e.currentTarget.dataset.index];
    }
    let obj = {};
    if (undefined != tempobj){
      for (let t in tempobj){
        if (undefined != tempobj[t] && "undefined" != tempobj[t] && "null" != tempobj[t]){
          obj[t] = tempobj[t];
        }else{
          obj[t] = "";
        }
      }
    }
    wx.navigateTo({
      url: url + JSON.stringify(obj) ,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (app.loginUser == null || app.loginUser.id == null && app.loginUser.mobile == null) {
      wx.navigateTo({
        url: '/pages/authorizedLogin/authorizedLogin'
      })
      return;
    }
    if (this.data.type == "receiver") {
      this.setData({ "tab_jjr": "nav_1_1", "tab_sjr": "nav_1_1_h", "addtype": "收货人", "type": "receiver" });
    } else {
      this.setData({ "tab_jjr": "nav_1_1_h", "tab_sjr": "nav_1_1", "addtype": "发货人", "type": "sender" });
    }
    getList(this, app.loginUser.id);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    getList(this, app.loginUser.id);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // },
  changeDefault: function (e) {
    let _this = this;
    // console.log(e.target.dataset.i + "--" + e.target.dataset.i);
    if (e.target.dataset.i == 1 || e.target.dataset.i == 1) {
      app.tip('已是默认', 'none');
    } else {
      wx.showLoading({ mask: true, title: "加载中" });
      let temp = this.data.senderarr[e.target.dataset.index];
      let url = config.requestUrl + "senderContactInfo/edit";
      if (_this.data.type == "receiver") {
        temp = this.data.receiverarr[e.target.dataset.index];
        url = config.requestUrl + "receiverContactInfo/edit";
      }
      let par = { "data": '{"id":"' + temp.id + '","vipMemberId":"' + temp.vipMemberId + '","contactName":"' + temp.contactName + '","contactMobile":"' + temp.contactMobile + '","contactAddress":"' + temp.contactAddress + '","isDefault":"1"}' };

      // console.log(par);
      wx.request({
        url: url,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
        },
        data: par,
        success: function (msg) {
          // console.log(msg);
          wx.hideLoading();
          if (msg.data.code == 100) {
            getList(_this, app.loginUser.id);
            app.tip("设置成功", 'none');
          } else {
            app.tip("设置失败", 'none');
          }
        },
        fail: function (msg) {
          // console.log(msg);
          app.tip("设置失败", 'none');
          wx.hideLoading();
        }
      });
    }

  },
  /**
   * 发货人地址
   */
  jjrdz:function(){
    this.data.type = "sender";
    this.setData({ "tab_jjr": "nav_1_1_h", "tab_sjr": "nav_1_1", "addtype": "发货人", "type" : "sender"});
    getList(this, app.loginUser.id);
    
  },
  /**
   * 收货人地址
   */
  sjrdz: function () {
    this.data.type = "receiver";
    this.setData({ "tab_jjr": "nav_1_1", "tab_sjr": "nav_1_1_h", "addtype": "收货人", "type": "receiver"});
    getList(this, app.loginUser.id);
  }
});
/**
 * 查询数据
 */
function getList(_this, userid) {
  
  searchList(_this, userid, function (msg) {
    // console.log(msg.data.content);
    if (Object.prototype.toString.call(msg.data.content) === "[object Array]" && msg.data.content.length > 0) {
      if (_this.data.type == "receiver") {
        _this.data.receiverarr = msg.data.content;
      }else{
        _this.data.senderarr = msg.data.content;
      }
      for (let i = 0; i < msg.data.content.length ; i++){
        msg.data.content[i].contactAddressTemp = msg.data.content[i].contactAddress.replace('⊙','');
      }
      _this.setData({ wuliu: msg.data.content, showView: true });
    } else {
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
    }

  });
}

/**
 * 请求数据
 */
function searchList(_this, userId, callback) {
  wx.showLoading({ mask: true, title: "加载中" });
  let url = config.requestUrl + "senderContactInfo/search";
  if (_this.data.type == "receiver") {
    url = config.requestUrl + "receiverContactInfo/search";
  }
  wx.request({
    url: url,
    method: "POST",
    header: {
      "Content-Type": "application/x-www-form-urlencoded"//;charset=utf-8
    },
    data: { "vipMemberId": userId },
    success: function (msg) {
      callback(msg);

      //重新设置下单基本信息中的收发货人
      if (app.companyInfo != null && app.companyInfo != undefined && app.companyInfo!=""){
        if (app.companyInfo.businessSetting != null && app.companyInfo.businessSetting != undefined && app.companyInfo.businessSetting != "") {
          if (_this.data.type == "receiver") {
            app.companyInfo.businessSetting.receiverContactInfos = msg.data.content;
          } else {
            app.companyInfo.businessSetting.senderContactInfos = msg.data.content;
          }
        }
      }

      wx.hideLoading();
    },
    fail: function (msg) {
      // console.log(msg);
      _this.setData({
        showView: false,
        message: "抱歉，没有找到相关数据！",
        imagePath: "https://7075-public-bb1cff-1257643776.tcb.qcloud.la/noData.png?sign=ce0f461a731ab901f7f26c5ad354c749&t=1570586800"
      });
      wx.hideLoading();
    }
  });
}
