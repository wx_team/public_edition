const app = getApp()
const config = require('../../../config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  onLoad: function (options) {
    var that = this;
    //获取银行卡信息
    wx.request({
      url: config.requestUrl + "bankInfo/search",
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: {
        vipMemberId: app.loginUser.id
      },
      success: function (res) {
        console.info("银行卡信息返回结果");
        console.info(res);
        var bankInfoList = [];
        var bankReturnResult = res.data.content;
        if (bankReturnResult != undefined && bankReturnResult != null && bankReturnResult != "") {
          for (var i = 0; i < bankReturnResult.length; i++) {
            bankInfoList[i] = bankReturnResult[i];
          }
        }
        that.setData({
          bankArray: bankInfoList
        })

        //重新设置下单基本信息中的收发货人
        app.companyInfo.businessSetting.vipBankList = bankInfoList;
      },
      fail: function (res) {
        console.log("error res=")
        console.log(res.data)
      }
    });

  },

  //返回首页
  toIndex: function (e) {
    wx.switchTab({
      url: '../../index/index',
    })
  },
  
  /**
   * 跳转到添加银行卡页面
   */
  toAddBank: function () {
    wx.navigateTo({ url: "../addbank/addbank" });
  },

  /**
   * 删除银行卡
   */
  delBank: function (e) {
    var id = e.currentTarget.dataset.id;
    var that = this;

    wx.showModal({
      title: '提示',
      content: "是否确定删除",
      showCancel: true,
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '加载中'
          })
          wx.request({
            url: config.bankDelUrl,
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            data: { id: id },
            success: (res) => {
              wx.hideLoading()
              var data = res.data;
              if (data && data.code && data.code == 100) {
                app.tip('操作成功', 'success')
                that.onLoad();
              } else {
                var errMsg;
                if (data && data.msg) {
                  errMsg = data.msg
                } else {
                  errMsg = res.statusCode + ":" + res.errMsg
                }
                app.tip(errMsg, 'none')
              }
            },
            fail: function ({ errMsg }) {
              wx.hideLoading()
              app.tip(errMsg, 'none')
            }
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  }
  // ,

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {
    
  // }
})