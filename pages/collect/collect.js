// pages/classifyList/classifyList.js
const app = getApp()
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentActive: 0,
    recommendList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      url: host + 'me/myCollect',
      data: {
        uid : wx.getStorageSync('uid'),
      },
      method: 'post',
      dataType: 'json',
      success: function(res) {
        that.setData({
          recommendList:res.data.data
        })
        wx.hideLoading()
      },
    })
  },
  details:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/information/information?id='+id,
    })
  }
})