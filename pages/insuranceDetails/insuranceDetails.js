// pages/insurance/insurance.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: [
      {
        id: 0,
        type: 'image',
        url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big84000.jpg'
      },
      {
        id: 1,
        type: 'image',
        url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big84001.jpg',
      },
      {
        id: 2,
        type: 'image',
        url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big39000.jpg'
      }
    ],
    recommend: ["产品介绍", "详细说明"],
    currentActive: 0,
    recommendList: [
      {
        id: 1,
        img: "http://new.feiyang56.cn:8087/uploads/20190718/170af65ab97bc782b9abd7c6ae458efb.png",
      },
      {
        id: 2,
        img: "http://new.feiyang56.cn:8087/uploads/20190718/170af65ab97bc782b9abd7c6ae458efb.png",
      },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  NavTab: function (e) {
    var that = this;
    that.setData({
      currentActive: e.currentTarget.dataset.index
    })
  },
  fast:function(){
    wx.navigateTo({
      url: '/pages/insuranceInfo/insuranceInfo',
    })
  }
})