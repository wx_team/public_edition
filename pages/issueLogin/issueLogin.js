// pages/issueLogin/issueLogin.js
const app = getApp();
var host = app.globalData.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  onGetUserInfo:function(e){
    console.log("host:"+host);
   
    console.log("userinfo:"+e.detail.userInfo);
    var a= e.detail.userInfo;
    if (!this.logged && e.detail.userInfo) {
      wx.login({
        success(res) {
          if (res.code) {
            console.log("res.code:"+res.code);
            // 发起网络请求
            wx.request({
              url: host + 'Issuelogin',
              data: {
                code: res.code,
                userinfo: e.detail.userInfo,
              },
              success: function (res) {
                console.log(res);
                wx.setStorageSync('userInfo', res.data.data.userinfo);
                wx.setStorageSync('uid', res.data.data.uid);
                wx.setStorageSync('openid', res.data.data.openid);
                // wx.navigateTo({
                //   url: '../index/index'
                // })
                wx.navigateBack({
                  delta: 1
                })
              }
            })
          } else {
            console.log('登录失败！' + res.errMsg)
          }
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})