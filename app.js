//app.js
var loginComplete = false
var getInfoComplete = false
const config = require('config.js');
var QQMapWX = require('utils/qqmap-wx-jssdk.js');

var latitude = '';
var longitude = '';
App({
  //小程序应用名称
  appName: "",
  appId: "",
  loadComplete: false,
  //微信用户信息
  userInfo: null,
  //服务端用户信息，格式：
  loginUser: null,
  openid: null,
  //定位信息
  currentPosition: null,
  //公司信息
  companyInfo: null,
  //系统信息
  sysInfo: null,
  //投保公司信息
  insureCompany: null,
  //投保用户名
  insureUserName: null,
  //投保密码
  insurePassword: null,

  onLaunch: function () {
    if (!wx.cloud) {
      console.error(" 2.2.3 或以上的基础库以使用云能力")
    } else {
      console.log(" cloud 正常加载");
      wx.cloud.init({
        traceUser: true,
        env:'public-bb1cff'
      })
    }

    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    host: "https://b56.feiyang56.cn/api/",//index/index
    insurance: 'http://114.115.128.95:60008/insurancecenter-restful-api/',
    showFileUrl: config.showFileUrl
  },
  onShow: function () {
    var pages = getCurrentPages();
    //加载当前位置信息   加判断解决选择图片时加载这个方法
    if(pages.length==0){
      this.getLocalInfo();
      this.wxLogin();
      this.forceUpdate();
    }
    
  },
  getLocalInfo: function(){
    var _this = this
    // 实例化API核心类
    var qqmapsdk = new QQMapWX({
      key: 'RPTBZ-NX6LJ-ETVF5-KRBHK-GAFM6-SIFKA'
    });
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        latitude = res.latitude,
        longitude = res.longitude;
        _this.setLatitude(latitude);
        _this.setLongitude(longitude);
        //2、根据坐标获取当前位置名称，显示在顶部:腾讯地图逆地址解析
        qqmapsdk.reverseGeocoder({
          location: {
            latitude: res.latitude,
            longitude: res.longitude
          },
          success: function (addressRes) {
            _this.setCurrentPosition(addressRes.result);
          }
        })
      }
    })
  },
	/*
		提示未登录，点击确定跳到登录页面
		点击取消回到首页
	*/
  checkLogin: function () {
    if (this.loginUser == null || this.loginUser.id == null || this.loginUser.id == '') {
      wx.showModal({
        title: '提示',
        content: '用户未登录',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/authorizedLogin/authorizedLogin'
            })
          } else if (res.cancel) {
            wx.switchTab({
              url: '/pages/index/index'
            })
          }
        }
      })
      return false

    } else {
      return true
    }
  },
  	/*
		提示未登录，点击确定跳到登录页面
		点击取消回到首页
	*/
  checkOrderLogin: function (shareId) {
    console.log("app shareId"+shareId);
    if (this.loginUser == null || this.loginUser.id == null || this.loginUser.id == '') {
      wx.showModal({
        title: '提示',
        content: '用户未登录',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/authorizedLogin/authorizedLogin?shareId='+shareId
            })
          } else if (res.cancel) {
            wx.switchTab({
              url: '/pages/index/index'
            })
          }
        }
      })
      return false

    } else {
      return true
    }
  },
  setAppName: function (appName) {
    this.appName = appName
  },
  setAppId: function (appId) {
    this.appId = appId
  },
  setCompanyInfo: function (company) {
    this.companyInfo = company
  },
  setSysInfo: function (sysInfo) {
    this.sysInfo = sysInfo
  },
  setLoginUser: function (user) {
    this.loginUser = user
  },
  setLatitude: function (latitude) {
    this.latitude = latitude
  },
  setLongitude: function (longitude) {
    this.longitude = longitude
  },
  setCurrentPosition: function (currentPosition) {
    this.currentPosition = currentPosition
  },
  setInsureCompany: function (insureCompany) {
    this.insureCompany = insureCompany
  },
  setInsureUserName: function (insureUserName) {
    this.insureUserName = insureUserName
  },
  setInsurePassword: function (insurePassword) {
    this.insurePassword = insurePassword
  },
	/*
		文本输入框正则
		只可以输入汉字，英文，数字  有效符号：（），。/*+   。
	*/
  inputZhengZe: function (msg) {
    //正则表达式
    var reg = new RegExp("^[A-Za-z0-9,.，。()*+/\u4e00-\u9fa5]+$");
    var res = reg.test(msg);
    return res;
  },
  /*
  文本输入框正则
  只可以输入汉字，英文，数字   
  */
  inputZimuHanziShuziZhengZe: function (msg) {
    //正则表达式
    var reg = new RegExp("^[A-Za-z0-9/\u4e00-\u9fa5]+$");
    var res = reg.test(msg);
    return res;
  },
  /*
  显示提示
  msg 提示内容
  icon 图标 success、loading、none 默认是none
*/
  tip: function (msg, icon) {
    if (msg) {
      wx.showToast({
        title: msg,
        icon: icon ? icon : 'none',
        duration: 2000
      })
    }
  },
  //获取用户授权信息
  //这个方法暂时没用了，wx.getUserInfo会自动弹出授权窗口
  getUserInfo: function () {
    var _this = this;
    wx.getSetting({
      success: res => {
        if (!res.authSetting['scope.userInfo']) {
          wx.authorize({
            scope: 'scope.userInfo',
            success() {
              _this.wxGetUserInfo()
            },
            fail: function (e) {
              getInfoComplete = true
              _this.userInfo = null
              _this.doComplete()
              //_this.tip("用户拒绝授权", 'loading')
            }
          })
        } else {
          _this.wxGetUserInfo()
        }
      }
    })
  },
  //获取用头像昵称等
  wxGetUserInfo: function () {
    var _this = this;
    wx.getUserInfo({
      withCredentials: false,
      success: function (res) {
        _this.userInfo = res.userInfo
      },
      fail: function (e) {
        _this.userInfo = null
        //_this.tip("获取用户信息失败", 'loading')
      },
      complete: function (e) {
        getInfoComplete = true
        _this.doComplete()
      }
    })
  },
  //微信登录
  wxLogin: function () {
    var _this = this;
    wx.login({
      success: res => {
        if (res.code) {
          _this.getOpenId(res.code);

        } else {
          loginComplete = true
          _this.doComplete()
          //_this.tip("微信登录失败", 'loading')
        }
      }
    })
  },
	/**
	 * 根据微信登录获取的code去服务端获取openId及绑定的用户信息
	 */

  getOpenId: function (code) {
    wx.showLoading({
      title: '加载中',
      mask:true
    })
    wx.hideTabBar();
    var _this = this
    wx.request({
      url: config.openIdUrl,
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        "code": code,
        "tenantCode": "",
        "latitude": latitude,
        "longitude": longitude
      },
      success: function (result) {
        console.info(result);
        var data = result.data;
        if (data.success) {
          if (data.openid) {
            _this.openid = data.openid
          }
          _this.setAppName(data.appName)
          _this.setAppId(data.appId)
          _this.setCompanyInfo(data.company)
          _this.setSysInfo(data.sysInfo)
          if (data.user && data.user.id) {
            _this.setLoginUser(data.user)
          } else {
            //用户未绑定
            _this.setLoginUser(null)
          }
          wx.redirectTo({ url: "../index/index" })

        } else {
          _this.tip(data.msg, 'none')
        }
      },
      fail: function ({ errMsg }) {
      },
      complete: function () {
        loginComplete = true
        _this.doComplete()
      }
    })
  },
  doComplete: function () {
    if (loginComplete) {//&& getInfoComplete
      this.loadComplete = true
      wx.hideLoading()
      wx.showTabBar();
    }
  },
  forceUpdate: function () {
    if (!wx.canIUse("getUpdateManager")) return
    const updateManager = wx.getUpdateManager()
    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
    })
    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function (res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed(function () {
      // 新的版本下载失败
    })
  },

  // 保存用户FormId信息
  updateUserFormId: function (mobile, formId, openId) {
    console.info("updateUserFormId---appid:" + this.appId);
    // 判断手机号码不等于null是，保存信息
    if (mobile != null && mobile != undefined) {
      wx.request({
        url: config.requestUrl + "user/editFormIds",
        method: "GET",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          mobile: mobile,
          formId: formId,
          openId: openId,
          appId: this.appId
        },
        success: function (msg) {
          console.log("SUCCESS 更新用户FormIds结果信息：" + JSON.stringify(msg));
        },
        fail: function (msg) {
        }
      })
    }
  },
  
  trim: function (str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
  },
})