// 公共参数
// var host = "https://xcxtest.feiyang56.cn/xcxtest-restful-api/"//测试环境
var host = "https://b114.feiyang56.cn/logistics-restful-api/"//正式环境
// var host = "http://b8w7xa.natappfree.cc/logistics_restful_api/"//test
// 商城支付公共参数
// var mallHost = "http://tengyun-test.feiyang56.cn:60012/mall-restful-api/"//测试环境
var mallHost = "https://mall.feiyang56.cn/mall-restful-api/"//正式环境
// var mallHost = "http://tengyun-test.feiyang56.cn:60012/mall-restful-api/"//test
const config = {
  appid: "wx881c5f23c98d2b34",
  host,
  // 用code换取openId
  openIdUrl: `${host}wxUtil/getOpenId`,
  // 获取选择公司后的基本信息
  publicBasicinfoUrl: `${host}wxUtil/getPublicBasicinfo`,
  // 登录地址，用于建立会话
  loginUrl: `${host}login`,
  // 注册保存接口
  registerUrl: `${host}login/register`,
  // 修改用户信息接口
  modifyUserUrl: host + 'user/editUserInfo',
  // 新增银行信息== null ? '暂无地址' : item.address== null ? '暂无地址' : item.address== null ? '暂无地址' : item.address   
  bankAddUrl: host + 'bankInfo/add',
  // 删除银行信息
  bankDelUrl: host + 'bankInfo/delete',
  // 手机号，修改密码
  forgetPassUrl: host + 'user/editPass',
  // 用code换取openId
  openUserIdUrl: `${host}wxUtil/getUserId`,
  // 自动登录
  loginAutoUrl: `${host}login/loginNotPassWord`,
  // 请求地
  requestUrl: host,
  // 商城支付请求地址
  mallHost : mallHost,
  tenantCode:"",
  // 显示图片
  showFileUrl: "https://b178.feiyang56.cn/fileuploader/"
};

module.exports = config
