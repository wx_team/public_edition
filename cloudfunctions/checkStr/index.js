// 云函数入口文件
const cloud = require('wx-server-sdk')

//cloud.init()
cloud.init({
  env:cloud.DYNAMIC_CURRENT_ENV
})
// 云函数入口函数 文字内容是否合法
exports.main = async (event, context) => {
  return await cloud.openapi.security.msgSecCheck({
    content:event.txt
  })

}